package com.tnl.headoffer.restcontroller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.biz.hunter.dao.CompanyDAO;
import com.biz.hunter.db.entity.Company;
import com.biz.hunter.db.entity.Employee;

@RestController
public class CompanyController {
	private static final Logger logger = LoggerFactory
			.getLogger(CompanyController.class);
	@Autowired
	private CompanyDAO companyDAO;
	
	@RequestMapping(value =RestURIConstants.GET_COMPANY_ID ,method = RequestMethod.GET)
	public Company getCompanyDetail(@RequestParam(value = "id",required = false,
	                                                    defaultValue = "0") Integer id) {
		Company company = companyDAO.getById(id);
		return company;
	}
	
	@RequestMapping(value =RestURIConstants.GET_COMPANY_ALL,method = RequestMethod.GET)
	public List<Company> getCompanies() {
		List<Company> list =  companyDAO.getALL();
		return list;
	}
	
	@RequestMapping(value =RestURIConstants.GET_COMPANY_DOMAIN ,method = RequestMethod.GET)
	public List<Company> getDomainCompanies(@RequestParam(value = "id",required = true,
	                                                    defaultValue = "0") Integer id) {
		List<Company> list =  companyDAO.getDomainCompanies(id);
		return list;
	}
	
	@RequestMapping(value = RestURIConstants.CREATE_COMPANY, method = RequestMethod.POST)
	public @ResponseBody Company createCompany(@RequestBody Company company) {
		logger.info("Start create Company.");

		Date now = new Date();
		company.setCreateDate(now.getTime());
		int result = companyDAO.save(company);
		if (result ==-1)
		{
			logger.error("create company failed" +company);
			return null;
		}
		
			 company.setId(result);
		return company;
	}

	
	@RequestMapping(value = RestURIConstants.DELETE_COMPANY, method = RequestMethod.DELETE)
	public @ResponseBody Company deleteCompany(@PathVariable("id") int Id) {
		logger.info("Start delete Company.");
		Company company = companyDAO.getById(Id);
		companyDAO.deleteById(Id);
		return company;
	}

	
	@RequestMapping(value = RestURIConstants.UPDATE_COMPANY, method = RequestMethod.POST)
	public @ResponseBody Company updateCompany(@RequestBody Company company) {
		logger.info("Start UPDATE Company.");

		companyDAO.update(company);
		return company;
	}
	
	
}