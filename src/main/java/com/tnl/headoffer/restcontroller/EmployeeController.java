package com.tnl.headoffer.restcontroller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.biz.hunter.dao.CompanyDAO;
import com.biz.hunter.dao.EmployeeDAO;
import com.biz.hunter.db.entity.Company;
import com.biz.hunter.db.entity.Employee;

/**
 * Handles requests for the Employee service.
 */
@RestController
public class EmployeeController {

	private static final Logger logger = LoggerFactory
			.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeDAO empDAO;
	
	@Autowired
	private CompanyDAO companyDAO;
	
//	@Autowired
//	private ICompanyService companyService;

	@RequestMapping(value = RestURIConstants.GET_EMP, method = RequestMethod.GET)
	public @ResponseBody Employee getEmployee(@PathVariable("id") int empId) {
		logger.info("Start getEmployee. ID=" + empId);
		Employee emp = empDAO.getById(empId);
		Company comp = companyDAO.getById(emp.getCompanyId());
		emp.setCompany(comp);
		return emp;
	}

	@RequestMapping(value = RestURIConstants.GET_ALL_EMP, method = RequestMethod.GET)
	public @ResponseBody List<Employee> getAllEmployees() {
		logger.info("Start getAllEmployees.");
		List<Employee> emps = empDAO.getALLEmps();
		return emps;
	}

	@RequestMapping(value = RestURIConstants.CREATE_EMP, method = RequestMethod.POST)
	public @ResponseBody Employee createEmployee(@RequestBody Employee emp) {
		logger.info("Start create Employee.");

		Date now = new Date();
		emp.setCreateDate(now.getTime());
		empDAO.save(emp);
		return emp;
	}

	@RequestMapping(value = RestURIConstants.DELETE_EMP, method = RequestMethod.DELETE)
	public @ResponseBody Employee deleteEmployee(@PathVariable("id") int empId) {
		logger.info("Start deleteEmployee.");
		Employee emp = empDAO.getById(empId);
		empDAO.delete(""+empId);
		return emp;
	}

	
	@RequestMapping(value = RestURIConstants.UPDATE_EMP, method = RequestMethod.POST)
	public @ResponseBody Employee updateEmployee(@RequestBody Employee emp) {
		logger.info("Start UPDATE Employee.");

		empDAO.update(emp);
		return emp;
	}
}
