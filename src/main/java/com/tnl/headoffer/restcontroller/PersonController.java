package com.tnl.headoffer.restcontroller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.biz.hunter.dao.PersonDAO;
import com.biz.hunter.db.entity.Person;

@RestController
public class PersonController {
	@Autowired
	private PersonDAO personDAO;
	
	
private static final Logger logger = LoggerFactory.getLogger(PersonController.class);
	
	
	@RequestMapping(value = RestURIConstants.GET_PERSON, method = RequestMethod.GET)
	public @ResponseBody Person getPerson(@PathVariable("id") int personId) {
		logger.info("Start getPerson. ID="+personId);
		return personDAO.getById(personId);
	}
	
	@RequestMapping(value = RestURIConstants.GET_ALL_PERSONS, method = RequestMethod.GET)
	public @ResponseBody List<Person> getAllPersons() {
		logger.info("Start getAllPersons.");
		return  personDAO.getALL();
		
	}
	
	@RequestMapping(value = RestURIConstants.CREATE_PERSON, method = RequestMethod.POST)
	public @ResponseBody Person createPerson(@RequestBody Person person) {
		logger.info("Start createPerson.");
		Date current = new Date();
		
		person.setCreateDate(current.getTime());
		personDAO.save( person);
		return person;
	}
	
	@RequestMapping(value = RestURIConstants.UPDATE_PERSON, method = RequestMethod.POST)
	public @ResponseBody Person updatePerson(@RequestBody Person person) {
		logger.info("Start UPDATEPerson.");
		Date current = new Date();
		person.setCreateDate(current.getTime());
		personDAO.update( person);
		return person;
	}
	
	@RequestMapping(value = RestURIConstants.DELETE_PERSON, method = RequestMethod.DELETE)
	public @ResponseBody Person deletePerson(@PathVariable("id") int empId) {
		logger.info("Start deletePerson.");
		Person person = personDAO.getById(empId);
		personDAO.delete( empId);
		return person;
	}
}