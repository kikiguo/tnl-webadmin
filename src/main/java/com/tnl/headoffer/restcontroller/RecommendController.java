package com.tnl.headoffer.restcontroller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.biz.hunter.dao.PersonDAO;
import com.biz.hunter.dao.RecommendDAO;
import com.biz.hunter.db.entity.Person;
import com.biz.hunter.db.entity.Recommendation;

/**
 * Handles requests for the Recommendation service.
 */
@Controller
public class RecommendController {

	private static final Logger logger = LoggerFactory
			.getLogger(RecommendController.class);

	@Autowired
	private RecommendDAO recommendService;
	@Autowired
	
	PersonDAO personDAO;
	
	@RequestMapping(value = RestURIConstants.GET_RECOMMEND_ID, method = RequestMethod.GET)
	public @ResponseBody Recommendation getRecommendation(@PathVariable("id") String Id) {
		logger.info("Start getRecommendation. ID=" + Id);
		Recommendation emp = recommendService.getById(Id);
		int id = emp.getPersonId();
	Person candidate = 	personDAO.getById(id);
		emp.setPerson(candidate);
		return emp;
	}

	@RequestMapping(value = RestURIConstants.GET_RECOMMEND_ALL, method = RequestMethod.GET)
	public @ResponseBody List<Recommendation> getAllRecommendations() {
		logger.info("Start getAllRecommendations.");
		List<Recommendation> emps = recommendService.getALL();
		return emps;
	}

	@RequestMapping(value = RestURIConstants.CREATE_RECOMMEND, method = RequestMethod.POST)
	public @ResponseBody Recommendation createRecommendation(@RequestBody Recommendation recommend) {
		logger.info("Start create Recommendation.");

		Date now = new Date();
		recommend.setCreateDate(now.getTime());
		recommendService.save(recommend);
		return recommend;
	}

	@RequestMapping(value = RestURIConstants.DELETE_RECOMMEND, method = RequestMethod.DELETE)
	public @ResponseBody Recommendation deleteRecommendation(@PathVariable("id") String empId) {
		logger.info("Start deleteRecommendation.");
		Recommendation emp = recommendService.getById(empId);
		recommendService.delete(empId);
		return emp;
	}

	
	@RequestMapping(value = RestURIConstants.UPDATE_RECOMMEND, method = RequestMethod.POST)
	public @ResponseBody Recommendation updateRecommendation(@RequestBody Recommendation emp) {
		logger.info("Start UPDATE Recommendation.");

		recommendService.update(emp);
		return emp;
	}
}
