package com.tnl.headoffer.restcontroller;

public class RestURIConstants {

	public static final String GET_EMP = "/rest/emp/{id}";
	public static final String GET_ALL_EMP = "/rest/emps";
	public static final String CREATE_EMP = "/rest/emp/create";
	public static final String UPDATE_EMP = "/rest/emp/update";
	public static final String DELETE_EMP = "/rest/emp/delete/{id}";
	
	public static final String GET_PERSON = "/rest/person/{id}";
	public static final String GET_PERSON_OPENID = "/rest/person/{openid}";
	public static final String GET_ALL_PERSONS = "/rest/persons";
	public static final String CREATE_PERSON = "/rest/person/create";
	public static final String UPDATE_PERSON = "/rest/person/update";
	public static final String DELETE_PERSON = "/rest/person/delete/{id}";
	
	public static final String GET_RECOMMEND_ID ="/rest/recommendation/{id}";
	public static final String GET_RECOMMEND_ALL = "/rest/recommendations";
	public static final String CREATE_RECOMMEND = "/rest/recommendation/create";
	public static final String UPDATE_RECOMMEND = "/rest/recommendation/update";
	public static final String DELETE_RECOMMEND = "/rest/recommendation/delete/{id}";
	
	
	public static final String GET_COMPANY_ID ="/rest/company/{id}";
	public static final String CREATE_COMPANY = "/rest/company/create";
	public static final String UPDATE_COMPANY = "/rest/company/update";
	public static final String DELETE_COMPANY = "/rest/company/delete/{id}";
	public static final String GET_COMPANY_ALL = "/rest/companies";
	public static final String GET_COMPANY_DOMAIN = "/rest/companies/{id}";
	
	public static final String GET_JOB_ID ="/rest/job/id/{id}";
	public static final String CREATE_JOB = "/rest/job/create";
	public static final String UPDATE_JOB = "/rest/job/update";
	public static final String DELETE_JOB = "/rest/job/delete/{id}";
	public static final String GET_JOB_ALL = "/rest/jobs";
	public static final String GET_JOBS_DOMAIN = "/rest/jobs/{id}";
	
	public static final String GET_JOB_NAME ="/rest/job/{name}";
	public static final String GET_JOBS_CODE = "/rest/job/jobcode/{code}";
	

}
