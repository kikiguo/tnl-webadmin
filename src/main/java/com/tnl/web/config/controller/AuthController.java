/**
 * 
 */
package com.tnl.web.config.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tnl.domain.enums.UserStatus;
import com.tnl.web.domain.AdminUser;
import com.tnl.web.entity.ErrorMsg;
import com.tnl.web.service.UserService;

/**
 * @author dongjie.shi
 * 
 */
@Controller("authController")
public class AuthController {
	private static Logger log = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private Authentication authentication;

	@Autowired
	private UserService userService;

	private AdminUser user = null;

	public AdminUser getCurrentUser() {
		return authentication.getCurrentUser();
	}

	@RequestMapping(value = "login.do", method = RequestMethod.POST)
	public @ResponseBody ErrorMsg login(HttpServletRequest request, AdminUser user1	) {
   String username = user1.getUsername();
   String password = user1.getPassword();
		log.info("USER: " + username + " is trying to login.");
		ErrorMsg msg = new ErrorMsg();
		msg.setData("");
		AdminUser user = userService.retrieveUserByName(username);
		if (user == null) {
			msg.setErrorcode("40001");
			msg.setMsg("user name is not correct");
			return msg;
		}
		if (!password.equals(user.getPassword())) {
			
			msg.setErrorcode("40002");
			msg.setMsg("password is NOT correct!");
			
			log.debug("password is NOT correct!");
			return msg;
		}

		
		if (user.getStatus() == UserStatus.DELETED.getStatus()) {
			log.debug("Warning: User account is not active, please contact"
					+ " VDP Platform Support Center (vdpservicedesk@hp.com) for help.");
			msg.setErrorcode("40003");
			msg.setMsg("password is NOT correct!");
			return msg;
		}
		msg.setErrorcode("0");
		msg.setMsg("login successfully");
		authentication.setCurrentUser(user);
		HttpSession session = request.getSession();
		session.setAttribute("UserId", ""+user.getId());
		return msg;
	}

	@RequestMapping(value = "logout.do", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> logout(HttpServletRequest request) {
		Map<String, Object> msg = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		session.removeAttribute("UserId");
		if (getCurrentUser() != null) {
			log.info("USER: " + getCurrentUser().getUsername()
					+ " is trying to logout.");
			authentication.setCurrentUser(null);
		}
		return msg;
	}

	
		@RequestMapping(value = "admin/login.page", method = RequestMethod.GET)
		public String loginPage(HttpServletRequest request, Model model) {
			return "admin/platformadminlogin";
		}
		
		@RequestMapping(value = "admin/logout.page", method = RequestMethod.GET)
		public String logoutPage(HttpServletRequest request, Model model) {
			 logout(request);
				return "admin/platformadminlogin";
		}

}
