package com.tnl.web.config.controller;

import org.springframework.stereotype.Service;

import com.tnl.web.domain.AdminUser;


/**
 * 
 * 
 */

@Service
public class Authentication  {


	private AdminUser currentUser;

	public AdminUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(AdminUser user) {
		currentUser = user;
	}
	

}
