package com.tnl.web.config.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DomainController {
	private static final Logger logger = LoggerFactory.getLogger(DomainController.class);
	
	@RequestMapping(value = "admin/config/domain.page", method = RequestMethod.GET)
	public String jobTitleManagement(HttpServletRequest request, ModelMap model) {
		return "admin/config/domainlist";
	}
}
