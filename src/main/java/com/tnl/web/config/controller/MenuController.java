package com.tnl.web.config.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tnl.web.dao.PermissionDAO;
import com.tnl.web.domain.AdminUser;
import com.tnl.web.domain.Permission;
import com.tnl.web.dto.MenuDto;

@Controller
public class MenuController {
	private static Logger logger = LoggerFactory.getLogger(MenuController.class);

	@Autowired
	private Authentication authentication;
	@Autowired
	private PermissionDAO permDAO;
	
	@RequestMapping(value = "admin/main.page", method = RequestMethod.GET)
	public String main(HttpServletRequest request,Model model) {
		//candidate 
		HttpSession session = request.getSession();
		session.getAttribute("UserId");
		MenuDto candidateMenu = new MenuDto();
		AdminUser user = authentication.getCurrentUser();
		if (user ==null)
		{
			logger.error("user is null, session is expired.");
		}
//		List<Permission> permissions =  permDAO.getRolesPermission(user.getRoleId());
//		for(Permission perm:permissions)
//		{
//			perm.getPermissionName();
//		}
//		model.addAttribute("candidateMenu", "");
		
	    return "admin/platformadmin";
	}
	/*
	 * { menus:[{menuItem},{}
	 * }]
	 * }
	 */
	@RequestMapping(value = "web/getMenus.do", method = RequestMethod.GET)
	public @ResponseBody List<MenuDto> getMenus(HttpServletRequest request,
			 Model model) {
		return null;
	}
	
	@RequestMapping(value = "web/user/getPermissions.do", method = RequestMethod.POST)
	public @ResponseBody List<Permission> getPermission(HttpServletRequest request,
			String username, Model model) {
		AdminUser user = authentication.getCurrentUser();
		if (user ==null)
		{
			logger.error("user is null, session is expired.");
		}
		List<Permission> permissions =  permDAO.getRolesPermission(user.getRoleId());
		
		return permissions;
	}
	
}
