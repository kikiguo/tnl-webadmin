package com.tnl.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.biz.hunter.dao.CompanyDAO;
import com.biz.hunter.dao.DomainDAO;
import com.biz.hunter.dao.EmployeeDAO;
import com.biz.hunter.dao.PersonDAO;
import com.biz.hunter.dao.WXUserDAO;
import com.biz.hunter.db.entity.Employee;
import com.biz.hunter.db.entity.Person;
import com.tnl.web.config.controller.Authentication;
import com.tnl.web.domain.AdminUser;
import com.tnl.web.entity.ErrorMsg;

@Controller
public class CandidateController {

	private static final Logger logger = LoggerFactory
			.getLogger(CandidateController.class);

	@Autowired
	private WXUserDAO userDAO;
	@Autowired
	private DomainDAO domainDAO;
	@Autowired
	private CompanyDAO companyDAO;
	@Autowired
	private PersonDAO personDAO;
	@Autowired
	private EmployeeDAO empDAO;

	@Autowired
	private Authentication authentication;

	@RequestMapping(value = "admin/candidate/manager.page", method = RequestMethod.GET)
	public String toList(HttpServletRequest request, int platformId, Model model) {

		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("UserId");

		logger.debug("userid is " + userId);
		if (!StringUtils.hasText(userId)) {

			logger.info("empty openid ,wexin browser is needed.");
			return "admin/errorpage";
		}

		AdminUser user = authentication.getCurrentUser();
		if (user == null) {
			logger.error("can not get the user info" + userId);
			return "admin/platformadminlogin";
		}
		if (platformId == 0) {
			List<Person> candisates = personDAO.getALL();
			model.addAttribute("candisates", candisates);
		} else {
			List<Person> candisates = personDAO.getALL();// TODO: 猎头平台
			model.addAttribute("candisates", candisates);
		}
		return "admin/candidate/list";
	}

	@RequestMapping(value = "admin/candidate/toView.page", method = RequestMethod.GET)
	public String toView(HttpServletRequest request, int id, Model model) {
		Person person = personDAO.getById(id);
		Employee emp = empDAO.getById(id);
		if (emp == null) {
			model.addAttribute("candisate", person);
		} else {
			model.addAttribute("employee", emp);
		}
		return "admin/candidate/detail";
	}

	@RequestMapping(value = "admin/candidate/toEdit.page", method = RequestMethod.GET)
	public String toEdit(HttpServletRequest request, int id, Model model) {
		Person person = personDAO.getById(id);
		Employee emp = empDAO.getById(id);
		if (emp == null) {
			model.addAttribute("candisate", person);
		} else {
			model.addAttribute("employee", emp);
		}
		return "admin/candidate/edit";
	}

	@RequestMapping(value = "admin/candidate/create.do", method = RequestMethod.POST)
	public @ResponseBody ErrorMsg createCandidate( Person person) {
		ErrorMsg msg = new ErrorMsg();
		if (person == null)
		{
			msg.setErrorcode("40001");
			msg.setMsg("input parameters is not correct");
			return msg;
		}
		
	boolean exist = isExist(person);
	if(exist){
		msg.setErrorcode("30001");
		msg.setMsg("there is a person in system");
		return msg;
	}
		person.setActive(1);
		Date date = new Date();
		person.setCreateDate(date.getTime());
		
	    int result = personDAO.save(person);
	    if(result == -1 )
	    {
	    	msg.setErrorcode("50001");
			msg.setMsg("can not save the person" + person.getName());
			return msg;
	    }
		msg.setErrorcode("0");
		msg.setMsg("success");
		
		return msg;
	}
	
	//@RequestMapping(value = "admin/candidate/checkExist.do", method = RequestMethod.POST)
	private boolean isExist(Person person) {
		if (person == null) {
			return true;
		}

		if (StringUtils.hasText(person.getPhone())) {
			Person persondb = personDAO.getByPhone(person.getPhone());
			if (persondb == null) {
				return false;
			}
		}
		return true;
	}

	@RequestMapping(value = "admin/candidate/update.do", method = RequestMethod.POST)
	public @ResponseBody ErrorMsg updateCandidate(Person person) {
		ErrorMsg msg = new ErrorMsg();
		int result = personDAO.update(person);
		if (result == 1) {
			msg.setErrorcode("0");
			msg.setMsg("success");
			return msg;
		}
		msg.setErrorcode("50001");
		msg.setMsg("update failed");
		return msg;
	}

	@RequestMapping(value = "admin/candidate/delete.do", method = RequestMethod.GET)
	public @ResponseBody ErrorMsg  deleteCandidate(HttpServletRequest request, int personId,
			Model model) {
		ErrorMsg msg = new ErrorMsg();
		msg.setErrorcode("0");
		msg.setMsg("success");
		int empId = personId;
		try {
			Employee emp = this.empDAO.getById(empId);
			int result = -1;
			if (emp != null) {
				personId = emp.getPersonId();
				empDAO.delete("" + empId);
			}
			Person person = personDAO.getById(personId);
			if (person != null) {
				result = personDAO.delete(personId);
			}
		} catch(Exception ex){
		
			msg.setErrorcode("50001");
			msg.setMsg("update failed");
			return msg;
		}
		msg.setErrorcode("0");
		msg.setMsg("success");
		return msg;
	}

	@RequestMapping(value = "admin/candidate/activate.do", method = RequestMethod.POST)
	public String activateCandidate( Person person,
			Model model) {
		person.setActive(1);
		int result = personDAO.update(person);
		return "admin/candidate/list";
	}

	@RequestMapping(value = "admin/candidate/inactivate.do", method = RequestMethod.POST)
	public String inactivateCandidate(Person person,
			int employeeId, Model model) {
		person.setActive(0);
		int result = personDAO.update(person);
		return "admin/candidate/list";
	}

}
