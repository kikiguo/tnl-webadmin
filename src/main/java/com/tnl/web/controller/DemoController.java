package com.tnl.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.biz.hunter.dao.CityDAO;
import com.biz.hunter.dao.CompanyDAO;
import com.biz.hunter.dao.JobDAO;
import com.biz.hunter.dao.SurveyPersonDAO;
import com.biz.hunter.db.entity.City;
import com.biz.hunter.db.entity.Company;
import com.biz.hunter.db.entity.JobDesc;
import com.biz.hunter.db.entity.SurveyPerson;
import com.biz.wechat.util.DateUtil;
import com.tnl.web.entity.ErrorMsg;

@Controller
@RequestMapping("demo")
public class DemoController {

  private static final Logger logger = LoggerFactory.getLogger(DemoController.class);

  @Autowired
  SurveyPersonDAO surveyPersonDAO;

  @RequestMapping(value = "survey/add", method = RequestMethod.POST)
  @ResponseBody
  public ErrorMsg addSurvey(HttpServletRequest request, SurveyPerson dto, Model model) {
    ErrorMsg msg = new ErrorMsg();
    logger.debug("add survey candidate");
    int result = surveyPersonDAO.save(dto);
    msg.setErrorcode(""+result);
    logger.debug("persistent survey candidate,return" + result);
    msg.setData( dto);
    msg.setMsg(dto.getName());
    return msg;
  }

  @RequestMapping(value = "survey.page", method = RequestMethod.GET)
  public String viewAssistantList(HttpServletRequest request, ModelMap model) {
    return "demo/surveyo";
  }

  @RequestMapping(value = "survey/view", method = RequestMethod.GET)
  public String viewSurvey(HttpServletRequest request,String user, Model model) {

    logger.debug("add survey candidate");
//    model.addAttribute("user", user);
    return "demo/surveyOK";
  }

  @RequestMapping(value = "job/list", method = RequestMethod.GET)
  public String viewJobList(HttpServletRequest request, int companyId, ModelMap model) {

    List<JobDesc> jds = jobDAO.getAllperPage(companyId, 0, 10);
    convertJobDTO(jds);
    model.addAttribute("jobs", jds);
    return "demo/jobList";
  }

  private void convertJobDTO(List<JobDesc> jds) {
    for (JobDesc job : jds) {
      int companyId = job.getCompanyId();
      Company company = companyDAO.getById(companyId);

      int cityId = job.getCityId();
      City city = cityDAO.getById(cityId);

      job.setCity(city);
      job.setCompanyName(company.getName());
      long data = job.getExpiredDate();
      job.setExpiredDateStr(DateUtil.getDateStr(data));
    }
  }

  @RequestMapping(value = "job/more", method = RequestMethod.GET)
  @ResponseBody
  public Map<String, Object> moreHunters(HttpServletRequest request, int companyId, ModelMap model) {

    Map<String, Object> returnMap = new HashMap<String, Object>();

    int offset = 0;
    String startStr = request.getParameter("offset");
    if (StringUtils.isEmpty(startStr)) {
      offset = 0;
    } else {
      offset = Integer.valueOf(startStr);
    }
    // String companyId = request.getParameter("companyId");

    List<JobDesc> list = jobDAO.getAllperPage(companyId, offset, 10);

    returnMap.put("errorcode", 0);
    returnMap.put("msg", "成功获取数据");
    returnMap.put("jobs", list);

    return returnMap;
  }

  @RequestMapping(value = "job/view", method = RequestMethod.GET)
  public String viewJobDetail(HttpServletRequest request, int jobId, ModelMap model) {

    JobDesc job = jobDAO.getById(jobId);
    int companyId = job.getCompanyId();
    Company company = companyDAO.getById(job.getCompanyId());

    int cityId = job.getCityId();
    City city = cityDAO.getById(cityId);
    model.addAttribute("job", job);
    model.addAttribute("company", company);

    job.setCity(city);
    job.setCompanyName(company.getName());
    long data = job.getExpiredDate();
    job.setExpiredDateStr(DateUtil.getDateStr(data));
    model.addAttribute("job", job);

    return "demo/zhiweixinxi";
  }

  @Autowired
  private JobDAO jobDAO;

  @Autowired
  private CompanyDAO companyDAO;
  @Autowired
  CityDAO cityDAO;
}
