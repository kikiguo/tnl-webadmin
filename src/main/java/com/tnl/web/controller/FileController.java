package com.tnl.web.controller;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biz.wechat.annotation.OAuthRequired;

// 服务文件，上载的简历、音频
@Controller
public class FileController {
	
	public static final String BASE_PATH = "/usr/share/tomcat7/uploads/";
	//public static final String BASE_PATH = "c:/";
	
	@OAuthRequired
	@RequestMapping(value = "/rs/{fileName}.{ext}" , method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(@PathVariable("fileName") String fileName, @PathVariable("ext") String fileExt) throws IOException { 

		File downloadFile = new File(BASE_PATH + fileName + "." + fileExt); 
		
		HttpHeaders headers = new HttpHeaders(); 
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM); //APPLICATION_OCTET_STREAM是以流的形式下载文件，这样可以实现任意格式的文件下载。 
		headers.setContentDispositionFormData("attachment", fileName + "." + fileExt); 
		
		// return new ResponseEntity<byte[]>( 
		// FileUtils.readFileToByteArray(downloadFile), headers, 
		// HttpStatus.CREATED); 
		//网上有些人把HttpStatus.OK改成了HttpStatus.CREATED，这样在IE下会有问题，无法下载文件。 
		return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(downloadFile), headers, HttpStatus.OK);
	}

}
