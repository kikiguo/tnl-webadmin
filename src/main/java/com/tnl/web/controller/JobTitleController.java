package com.tnl.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.biz.hunter.dao.JobTitleDAO;
import com.biz.hunter.db.entity.JobTitle;
import com.tnl.web.dto.jqGridRequestObject;
import com.tnl.web.dto.jqGridResponseObject;
import com.tnl.web.entity.ErrorMsg;


@Controller
public class JobTitleController {
	private static final Logger logger = LoggerFactory.getLogger(JobTitleController.class);
	
	@Autowired
	private JobTitleDAO jobtitleDAO;

	// 职位名称管理首页
	@RequestMapping(value = "admin/jobtitle/manage.page", method = RequestMethod.GET)
	public String jobTitleManagement(HttpServletRequest request, ModelMap model) {
		return "admin/job/jobtitlelist";
	}
	
	// 显示添加/编辑页面
	@RequestMapping(value = "admin/jobtitle/form.page", method = RequestMethod.GET)
	public String jobTitleForm(HttpServletRequest request, ModelMap model, int coe) {
		JobTitle jobTitleItem = null;
		
		if(coe <= 0) {
			jobTitleItem = new JobTitle();
		} else {
			jobTitleItem = jobtitleDAO.getById(coe);
			if(jobTitleItem == null) {
				model.addAttribute("msg", "没找到你要的职位。");
				return "admin/msgpage";
			}
		}
		
		model.addAttribute("jobtitle", jobTitleItem);
		
		return "admin/job/jobtitleform";
	}
	
	// 创建/编辑jobtitle
	@RequestMapping(value = "admin/jobtitle/update.do", method = RequestMethod.POST)
	public @ResponseBody ErrorMsg updateJobTitle(HttpServletRequest request, ModelMap model, JobTitle jobtitle) {

		ErrorMsg msg = new ErrorMsg();

		if (jobtitle == null) {
			logger.debug("jobtitle is null");
			msg.setErrorcode("-1");
			msg.setMsg("未提交数据");	
			return msg;
		}
		
		JobTitle jb = null;
		if(jobtitle.getId() <=0) {
			if(jobtitle.getName() == null || StringUtils.isEmpty(jobtitle.getName())) {
				msg.setErrorcode("-2");
				msg.setMsg("名字不能为空");	
				return msg;
			}
			jb = jobtitleDAO.getByName(jobtitle.getName());
			if(jb != null) {
				msg.setErrorcode("-3");
				msg.setMsg("该职位名称已经存在");	
				return msg;
			}
			long id = jobtitleDAO.save(jobtitle);
			if (id == -1) {
				msg.setErrorcode("-1");
				msg.setMsg("创建失败");
				return msg;
			}
			jobtitle.setId(id);;
			msg.setErrorcode("0");
			msg.setMsg("成功创建职位");
			msg.setData(jobtitle);
			return msg;
		} else {
			jb = jobtitleDAO.getByNameAndNotId(jobtitle.getName(), jobtitle.getId());
			if(jb != null) {
				msg.setErrorcode("-3");
				msg.setMsg("该职位名称已经存在");	
				return msg;
			}
			int result = jobtitleDAO.update(jobtitle);
			if(result == -1) {
				msg.setErrorcode("-4");
				msg.setMsg("更新失败");	
			} else {
				msg.setErrorcode("0");
				msg.setMsg("更新成功");
			}
			return msg;
		}
	}
	
	// 删除
	@RequestMapping(value = "admin/jobtitle/delete.do", method = RequestMethod.GET)
	public @ResponseBody ErrorMsg deleteJobTitle(HttpServletRequest request, Integer jobtitleid) {

		ErrorMsg msg = new ErrorMsg();

		if (jobtitleid == null) {
			logger.debug("jobtitle is null");
			msg.setErrorcode("-1");
			msg.setMsg("必须指定要删除的id");	
			return msg;
		}
		
		int result = jobtitleDAO.delete(jobtitleid);
		if(result == -1) {
			msg.setErrorcode("-2");
			msg.setMsg("删除失败");	
		} else {
			msg.setErrorcode("0");
			msg.setMsg("删除成功");
		}
		return msg;
	}

	// 搜索数据
	@RequestMapping(value = "admin/jobtitle/search.do", method = RequestMethod.POST)
	public @ResponseBody jqGridResponseObject<JobTitle> searchJobTitle(jqGridRequestObject request, String sname, Integer spostype) {
		
		jqGridResponseObject<JobTitle> msg = new jqGridResponseObject<JobTitle>();
		
		long page = request.getPage();
		long pagesize = request.getRows();
		
		long totalrows = request.getTotalrows();
		long totalpages = 0;
		
		// 需要分页和计算总数
		if(totalrows == 0) {
			totalrows = jobtitleDAO.getCountOfAll(sname,spostype);
		}
		totalpages = jqGridResponseObject.calTotalPage(totalrows, pagesize);
		
		// 起始位置
		long start = 0;
		start = (page-1) * pagesize;
		
		// 获取数据
		List<JobTitle> list = jobtitleDAO.getAllWithLimit(sname,spostype,start, pagesize);
		if(list != null) {
			for (JobTitle jd : list) {
				int postype = jd.getPostype();
				String postypeStr = null;
				switch (postype) {
	            	case 1:  postypeStr = "技术";
	                     break;
	            	case 2:  postypeStr = "市场";
	                     break;
	            	case 3:  postypeStr = "营销";
	                     break;
	            	case 4:  postypeStr = "管理";
	                     break;
	            	default: postypeStr = "技术";
	                     break;
				}
				jd.setPostypeStr(postypeStr);
			}
		}
		
		// 设置返回的数据
		msg.setPage(page);
		msg.setTotal(totalpages);
		msg.setRecords(totalrows);
		msg.setRows(list);
		
		return msg;
	}
	
	// 后台管理的错误页面
	@RequestMapping(value = "admin/error.page", method = RequestMethod.GET)
	public String errorPage(HttpServletRequest request, ModelMap model) {
		return "admin/errorpage";
	}
	
}
