package com.tnl.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.biz.hunter.dao.CompanyDAO;
import com.biz.hunter.dao.DomainDAO;
import com.biz.hunter.dao.JobTitleDAO;
import com.biz.hunter.dao.StatisticDAO;
import com.biz.hunter.db.entity.Company;
import com.biz.hunter.db.entity.Domain;
import com.biz.hunter.db.entity.Employee;
import com.biz.hunter.db.entity.JobTitle;
import com.biz.hunter.db.entity.RecommendPosition;
import com.biz.hunter.db.entity.RecommendedPerson;
import com.tnl.web.util.qualifiers.AuditIt;

@Controller
public class ReportController {
	private static final Logger logger = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	private DomainDAO domainDAO;
	@Autowired
	private CompanyDAO companyDAO;
	@Autowired
	private StatisticDAO statisticDAO;
	@Autowired
	private JobTitleDAO jobtitleDAO;
	
	// 报告首页
	@AuditIt(module = "Report", action = "report_view", description = "Show the report index page")
	@RequestMapping(value = "admin/report/manage.page", method = RequestMethod.GET)
	public String jobManagement(HttpServletRequest request, ModelMap model, Integer page) {
		if(page == null) {
			return "admin/report/mainhyfl";
		}
		
		List<Domain> domains = domainDAO.getAllSubDomain();
		List<Company> companies = companyDAO.getALL();

		switch(page) {
			case 1:
				return "admin/report/mainhyfl";
			case 2:
				model.addAttribute("domains", domains);
				return "admin/report/mainxrgs";
			case 3:
				model.addAttribute("domains", domains);
				return "admin/report/mainxrzw";
			case 4:
				model.addAttribute("domains", domains);
				return "admin/report/mainszcs";
			case 5:
				model.addAttribute("domains", domains);
				model.addAttribute("companies", companies);
				return "admin/report/mainqwjrgs";
			case 6:
				model.addAttribute("companies", companies);
				return "admin/report/mainqwdrzw";
			case 7:
				model.addAttribute("domains", domains);
				model.addAttribute("companies", companies);
				return "admin/report/mainhslx";
			default:
				return "admin/report/mainhyfl";	
		}	
	}
	
	// 行业分类
	@RequestMapping(value = "admin/report/hyfl.do", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Object> reportHyfl(HttpServletRequest request, ModelMap model) {
		
		Map<String, Object> returnMap = new HashMap<String,Object>();
		
		Long medical = statisticDAO.getCountOfDomain(1);
		Long drug    = statisticDAO.getCountOfDomain(2);
		
		returnMap.put("errorcode", 0);
		returnMap.put("medical", medical);
		returnMap.put("drug", drug);
		
		return returnMap;
	}
	
	// 何时联系1
	@RequestMapping(value = "admin/report/hslx1.do", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Object> reportHslx1(HttpServletRequest request, ModelMap model) {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		String subdomain = request.getParameter("subdomain");
		if (!StringUtils.hasText(subdomain)) {
			returnMap.put("errorcode", -1);
			returnMap.put("msg", "必须选择细分领域");
		}
		
		Domain dm = domainDAO.getById(Integer.parseInt(subdomain));
		if(dm == null) {
			returnMap.put("errorcode", -2);
			returnMap.put("msg", "不存在该细分领域");
		}
		
		/*
		List<Domain> dmlist = domainDAO.getAllSubDomain();
		if(dmlist == null) {
			returnMap.put("errorcode", -3);
			returnMap.put("msg", "没有数据");
		}
		*/
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("subdomain", dm.getName());
		
		ArrayList<Long> list = new ArrayList<Long>();
		Long count0 = statisticDAO.getCountOfIntentTimeOfDomain(Integer.parseInt(subdomain), 0);
		Long count1 = statisticDAO.getCountOfIntentTimeOfDomain(Integer.parseInt(subdomain), 1);
		Long count6 = statisticDAO.getCountOfIntentTimeOfDomain(Integer.parseInt(subdomain), 6);
		Long count12 = statisticDAO.getCountOfIntentTimeOfDomain(Integer.parseInt(subdomain), 12);
		
		list.add(count0);
		list.add(count1);
		list.add(count6);
		list.add(count12);
		
		data.put("monthdata", list);
		
		returnMap.put("data", data);
		returnMap.put("errorcode", 0);
		returnMap.put("msg", "成功获取数据");

		return returnMap;
	}

	// 何时联系2
	@RequestMapping(value = "admin/report/hslx2.do", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Object> reportHslx2(HttpServletRequest request, ModelMap model) {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		String company = request.getParameter("company");
		if (!StringUtils.hasText(company)) {
			returnMap.put("errorcode", -1);
			returnMap.put("msg", "必须选择公司");
		}
		
		Company cp = companyDAO.getById(Integer.parseInt(company));
		if(cp == null) {
			returnMap.put("errorcode", -2);
			returnMap.put("msg", "不存在该公司");
		}
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("companyname", cp.getName());
		
		// 外循环 - 职位类型， 内循环 - intentTime
		ArrayList<Long> list1 = new ArrayList<Long>();
		Long count10 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(0, 1, Integer.parseInt(company));
		Long count11 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(1, 1, Integer.parseInt(company));
		Long count16 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(6, 1, Integer.parseInt(company));
		Long count112 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(12, 1, Integer.parseInt(company));
		list1.add(count10);
		list1.add(count11);
		list1.add(count16);
		list1.add(count112);
		
		ArrayList<Long> list2 = new ArrayList<Long>();
		Long count20 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(0, 2, Integer.parseInt(company));
		Long count21 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(1, 2, Integer.parseInt(company));
		Long count26 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(6, 2, Integer.parseInt(company));
		Long count212 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(12, 2, Integer.parseInt(company));
		list2.add(count20);
		list2.add(count21);
		list2.add(count26);
		list2.add(count212);

		ArrayList<Long> list3 = new ArrayList<Long>();
		Long count30 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(0, 3, Integer.parseInt(company));
		Long count31 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(1, 3, Integer.parseInt(company));
		Long count36 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(6, 3, Integer.parseInt(company));
		Long count312 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(12, 3, Integer.parseInt(company));
		list3.add(count30);
		list3.add(count31);
		list3.add(count36);
		list3.add(count312);

		ArrayList<Long> list4 = new ArrayList<Long>();
		Long count40 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(0, 4, Integer.parseInt(company));
		Long count41 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(1, 4, Integer.parseInt(company));
		Long count46 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(6, 4, Integer.parseInt(company));
		Long count412 = statisticDAO.getCountOfIntentTimeOfJobTypeOfCompany(12, 4, Integer.parseInt(company));
		list4.add(count40);
		list4.add(count41);
		list4.add(count46);
		list4.add(count412);
		
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(list1);
		list.add(list2);
		list.add(list3);
		list.add(list4);

		data.put("positiondata", list);
		
		returnMap.put("data", data);
		returnMap.put("errorcode", 0);
		returnMap.put("msg", "成功获取数据");
	
		return returnMap;
	}

	// 最感兴趣职位
	@RequestMapping(value = "admin/report/qwdrzw.do", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Object> reportQwdrzw(HttpServletRequest request, ModelMap model) {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		String company = request.getParameter("company");
		if (!StringUtils.hasText(company)) {
			returnMap.put("errorcode", -1);
			returnMap.put("msg", "必须选择公司");
		}
		
		Company cp = companyDAO.getById(Integer.parseInt(company));
		if(cp == null) {
			returnMap.put("errorcode", -2);
			returnMap.put("msg", "不存在该公司");
		}
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("companyname", cp.getName());
		
		// 外循环 职位名称
		List<JobTitle> jobs = jobtitleDAO.getALL();
		Map<String, Long> countMap = new HashMap<String, Long>();
		
		for (JobTitle jobtitle : jobs) {
			long jobtitleid = jobtitle.getId();
			String jobname  = jobtitle.getName();
			
			Long count = statisticDAO.getCountOfJobOfCompany(jobtitleid, Integer.parseInt(company));
			countMap.put(jobname, count);
		}
		// 排序
		// 这里将map.entrySet()转换成list
        List<Map.Entry<String,Long>> list = new ArrayList<Map.Entry<String,Long>>(countMap.entrySet());
        // 然后比较器来实现
        Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
            //降序
            public int compare(Entry<String, Long> o1,
                    Entry<String, Long> o2) {
                return -1 * o1.getValue().compareTo(o2.getValue());
            }
            
        });
        
        ArrayList<Long>   listvalue = new ArrayList<Long>();
        ArrayList<String> listkey =   new ArrayList<String>();
        
        for(int i=0; i<5; i++) {
        	Entry<String, Long> item = list.get(i);
        	listvalue.add(item.getValue());
        	listkey.add(item.getKey());
        }
        data.put("positiondata", listvalue);
        data.put("positionticks", listkey);
        
        returnMap.put("data", data);
		returnMap.put("errorcode", 0);
		returnMap.put("msg", "成功获取数据");
	
		return returnMap;
	}

	// 现任公司1
	@RequestMapping(value = "admin/report/xrgs1.do", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Object> reportCrgs1(HttpServletRequest request, ModelMap model) {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		String subdomain = request.getParameter("subdomain");
		if (!StringUtils.hasText(subdomain)) {
			returnMap.put("errorcode", -1);
			returnMap.put("msg", "必须细分领域");
		}
		
		Domain dm = domainDAO.getById(Integer.parseInt(subdomain));
		if(dm == null) {
			returnMap.put("errorcode", -2);
			returnMap.put("msg", "不存在该细分领域");
		}
		
		// 得到该细分领域的所有公司
		List<Company> companies = companyDAO.getDomainCompanies(Integer.parseInt(subdomain));
		Map<String, Long> countMap = new HashMap<String, Long>();
		
		for (Company company : companies) {
			long companyid = company.getId();
			String companyname  = company.getName();
			
			Long count = statisticDAO.getCountOfCompany(companyid);
			countMap.put(companyname, count);
		}
		
		// 排序
		// 这里将map.entrySet()转换成list
		List<Map.Entry<String,Long>> list = new ArrayList<Map.Entry<String,Long>>(countMap.entrySet());
		// 然后比较器来实现
		Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
			//降序
			public int compare(Entry<String, Long> o1, Entry<String, Long> o2) {
				return -1 * o1.getValue().compareTo(o2.getValue());
			}
		});
		
		ArrayList<Long>   listvalue = new ArrayList<Long>();
        ArrayList<String> listkey =   new ArrayList<String>();
        
        for(int i=0; i<Math.min(10, countMap.size()); i++) {
        	Entry<String, Long> item = list.get(i);
        	listvalue.add(item.getValue());
        	listkey.add(item.getKey());
        }
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("subdomainname", dm.getName());
        data.put("data", listvalue);
        data.put("subdomainticks", listkey);
        
        returnMap.put("data", data);
		returnMap.put("errorcode", 0);
		returnMap.put("msg", "成功获取数据");
	
		return returnMap;
	}


	// 现任公司2
	@RequestMapping(value = "admin/report/xrgs2.do", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Object> reportCrgs2(HttpServletRequest request, ModelMap model) {
			
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		List<Domain> domains = domainDAO.getAllSubDomain();
		//Map<String, Long> countMap = new HashMap<String, Long>();
		ArrayList<Long>   listvalue = new ArrayList<Long>();
        ArrayList<String> listkey =   new ArrayList<String>();
		
		for (Domain domain : domains) {
			int domainid = domain.getId();
			String domainname  = domain.getName();
			
			Long count = statisticDAO.getCountOfSubDomain(domainid);
			//countMap.put(domainname, count);
			listvalue.add(count);
			listkey.add(domainname);
		}
		
		Map<String, Object> valueMap1 = new HashMap<String, Object>();
		valueMap1.put("subdomainticks", listkey);
		valueMap1.put("data", listvalue);
		
		Map<String, Object> valueMap2 = new HashMap<String, Object>();
		valueMap2.put("subdomainticks", new ArrayList<String>());
		valueMap2.put("data", new ArrayList<Long>());
		
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(valueMap1);
		list.add(valueMap2);

		returnMap.put("data", list);
		returnMap.put("errorcode", 0);
		returnMap.put("msg", "成功获取数据");
		
		return returnMap;
	}

	// 现任职位
	@RequestMapping(value = "admin/report/xrzw.do", method = RequestMethod.GET)
	public @ResponseBody  Map<String, Object> reportXrzw(HttpServletRequest request, ModelMap model) {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		String subdomain = request.getParameter("subdomain");
		if (!StringUtils.hasText(subdomain)) {
			returnMap.put("errorcode", -1);
			returnMap.put("msg", "必须细分领域");
		}
		
		Domain dm = domainDAO.getById(Integer.parseInt(subdomain));
		if(dm == null) {
			returnMap.put("errorcode", -2);
			returnMap.put("msg", "不存在该细分领域");
		}
		
		Long count1 = statisticDAO.getCountOfJobtype(1);
		Long count2 = statisticDAO.getCountOfJobtype(2);
		Long count3 = statisticDAO.getCountOfJobtype(3);
		Long count4 = statisticDAO.getCountOfJobtype(4);
		
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("subdomainname", dm.getName());
		
		ArrayList<Object>   listvalue1 = new ArrayList<Object>();
		ArrayList<Object>   listvalue2 = new ArrayList<Object>();
		ArrayList<Object>   listvalue3 = new ArrayList<Object>();
		ArrayList<Object>   listvalue4 = new ArrayList<Object>();
		
		listvalue1.add("技术");
		listvalue1.add(count1);
		
		listvalue2.add("市场");
		listvalue2.add(count2);
		
		listvalue3.add("营销");
		listvalue3.add(count3);
		
		listvalue4.add("管理");
		listvalue4.add(count4);
		
		ArrayList<Object>   listvalue = new ArrayList<Object>();
		listvalue.add(listvalue1);
		listvalue.add(listvalue2);
		listvalue.add(listvalue3);
		listvalue.add(listvalue4);
		
        data.put("data", listvalue);
        
        returnMap.put("data", data);
		returnMap.put("errorcode", 0);
		returnMap.put("msg", "成功获取数据");
	
		return returnMap;
	}

}
