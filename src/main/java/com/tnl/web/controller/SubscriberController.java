package com.tnl.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.biz.hunter.dao.CompanyDAO;
import com.biz.hunter.dao.EmployeeDAO;
import com.biz.hunter.dao.JobDAO;
import com.biz.hunter.dao.JobSubscriberDAO;
import com.biz.hunter.dao.PersonDAO;
import com.biz.hunter.db.entity.Company;
import com.biz.hunter.db.entity.JobSubscriber;
import com.tnl.web.dto.jqGridRequestObject;
import com.tnl.web.dto.jqGridResponseObject;

@Controller()
public class SubscriberController {

	private final Logger logger = LoggerFactory.getLogger(RecommendController.class);

	@Autowired
	private CompanyDAO companyDAO;
	@Autowired
	private JobDAO jobDAO;
	@Autowired
	private PersonDAO personDAO;
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired 
	JobSubscriberDAO subscriberDAO;
	

	// 推荐管理首页
	@RequestMapping(value = "demo/subscriber/manage.page", method = RequestMethod.GET)
	public String jobManagement(HttpServletRequest request, ModelMap model) {
		return "demo/subscriberlist";
	}

	

	// 搜索数据
	@RequestMapping(value = "demo/subscriber/search.do", method = RequestMethod.POST)
	public @ResponseBody jqGridResponseObject<JobSubscriber> search(jqGridRequestObject request, 
			String sname) {

		jqGridResponseObject<JobSubscriber> msg = new jqGridResponseObject<JobSubscriber>();

		long page = request.getPage();
		long pagesize = request.getRows();

		long totalrows = request.getTotalrows();
		long totalpages = 0;

		// 需要分页和计算总数
		if (totalrows == 0) {
			totalrows = subscriberDAO.getCountOfAll(sname,null);
		}

		totalpages = jqGridResponseObject.calTotalPage(totalrows, pagesize);

		// 起始位置
		long start = 0;
		start = (page - 1) * pagesize;

		// 获取数据
		List<Company> companies = companyDAO.getALL();
		List<JobSubscriber> list = subscriberDAO.getAllWithLimit(sname, null, start, pagesize);
		if (list != null) {
			for (JobSubscriber rd : list) {
				//TODO employee name replace the nick name
			}
		}

		// 设置返回的数据
		msg.setPage(page);
		msg.setTotal(totalpages);
		msg.setRecords(totalrows);
		msg.setRows(list);

		return msg;
	}
}
