package com.tnl.web.dto;

public class CompanyDto {
	private String companycontact;
private int companyid;
private String companyname;
private String  companyphone;
private String companyaddress;
private String companyemail;
private int hunterid;
private int hrid;
private String orgcode;
private String bizcode;
public String getCompanycontact() {
	return companycontact;
}
public void setCompanycontact(String companycontact) {
	this.companycontact = companycontact;
}
public int getCompanyid() {
	return companyid;
}
public void setCompanyid(int companyid) {
	this.companyid = companyid;
}
public String getCompanyname() {
	return companyname;
}
public void setCompanyname(String companyname) {
	this.companyname = companyname;
}
public String getCompanyphone() {
	return companyphone;
}
public void setCompanyphone(String companyphone) {
	this.companyphone = companyphone;
}
public String getCompanyaddress() {
	return companyaddress;
}
public void setCompanyaddress(String companyaddress) {
	this.companyaddress = companyaddress;
}
public int getHunterid() {
	return hunterid;
}
public void setHunterid(int hunterid) {
	this.hunterid = hunterid;
}
public String getOrgcode() {
	return orgcode;
}
public void setOrgcode(String orgcode) {
	this.orgcode = orgcode;
}
public String getBizcode() {
	return bizcode;
}
public void setBizcode(String bizcode) {
	this.bizcode = bizcode;
}
public int getHrid() {
	return hrid;
}
public void setHrid(int hrid) {
	this.hrid = hrid;
}
public String getCompanyemail() {
	return companyemail;
}
public void setCompanyemail(String companyemail) {
	this.companyemail = companyemail;
}
}
