package com.tnl.web.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.*;

/**
 * JqGird表格分页对象
 */
@SuppressWarnings("rawtypes")
public class JqGridPage
	{
		/**
		 * how many rows we want to have into the grid
		 */
		private Integer rows;

		/**
		 * the requested page
		 */
		private Integer page;

		/**
		 * index row - i.e. user click to sort
		 */
		private String sidx;

		/**
		 * the direction
		 */
		private String sord;

		/**
		 * total pages
		 */
		private Integer total;

		/**
		 * records count
		 */
		private Integer records;

		/**
		 * data rows
		 */
		private ArrayList data;

		/**
		 * 首条记录的偏移值
		 */
		private int firstResult;

		/**
	     * 记录集的总数
	     */
		private int maxResults;

		/**
		 * 执行结果
		 * 
		 * 0 代表正确
		 * 非0 代表错误
		 */
		private int result;
		
		/**
		 * 获取执行结果
		 * 
		 * @return 执行结果
		 */
		public int getResult() {
			return result;
		}

		/**
		 * 设置执行结果
		 * 
		 * @param result 执行结果
		 */
		public void setResult(int result) {
			this.result = result;
		}

		/**
		 * 计算当前分页数值, 根据总数量计算得出总页数(total)，首条记录偏移(firstResult)，
		 * 当前记录数量
		 * 
		 * @param count 总数量
		 */
		public void execute(int count) {
		    if (rows == null || rows == 0)
		        rows = 10;
		    
			records = count;

			total = records / rows;
			if (records % rows != 0)
				total += 1;

			firstResult = page * rows - rows;
			maxResults = rows;
		}

		/**
		 * 获取每页记录数量
		 *
		 * @return 每页记录数量
		 */
		public Integer getRows() {
			return rows;
		}

		public void setRows(Integer rows) {
			this.rows = rows;
		}

		public Integer getPage() {
			return page;
		}

		public void setPage(Integer page) {
			this.page = page;
		}

		public String getSidx() {
			return sidx;
		}

		public void setSidx(String sidx) {
			this.sidx = sidx;
		}

		public String getSord() {
			return sord;
		}

		public void setSord(String sord) {
			this.sord = sord;
		}

		public Integer getTotal() {
			return total;
		}

		public void setTotal(Integer total) {
			this.total = total;
		}

		public Integer getRecords() {
			return records;
		}

		public void setRecords(Integer records) {
			this.records = records;
		}

		public List getData() {
			return data;
		}

		public void setData(List data) {
			if (data instanceof ArrayList)
			{
				this.data =(ArrayList) data;
				return;
			}
			
			this.data = new ArrayList(data);
		}

		public int getFirstResult() {
			return firstResult;
		}

		public void setFirstResult(int firstResult) {
			this.firstResult = firstResult;
		}

		public int getMaxResults() {
			return maxResults;
		}

		public void setMaxResults(int maxResults) {
			this.maxResults = maxResults;
		}

		public static JqGridPage valueOf(Page page) {
		    JqGridPage jqgrid = new JqGridPage();

		    jqgrid.setPage(page.getNumber() + 1);
		    jqgrid.setRows(page.getSize());

		    jqgrid.execute((int) page.getTotalElements());

		    jqgrid.setData(page.getContent());

		    return jqgrid;
		}

		public PageRequest toPageable() {
		    if (this.sord == null)
		    {
	    	    PageRequest pageable =
	    	        new PageRequest(this.page - 1, this.rows);
	    	    
	    	    return pageable;
		    }

		    PageRequest pageable =
	            new PageRequest(this.page - 1, this.rows,
	                Sort.Direction.valueOf(this.sord.toUpperCase()),
	                this.sidx);

		    return pageable;
		}
	}
