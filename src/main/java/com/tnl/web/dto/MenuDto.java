package com.tnl.web.dto;

import java.util.ArrayList;
import java.util.List;

public class MenuDto {
	
	private String name;
	private List<MenuItem> menus = new ArrayList<MenuItem>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<MenuItem> getMenus() {
		return menus;
	}
	public void setMenus(List<MenuItem> menus) {
		this.menus = menus;
	}
	

}
