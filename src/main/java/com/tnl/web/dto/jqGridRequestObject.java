package com.tnl.web.dto;

public class jqGridRequestObject {
	
	private long page;            // 请求的页
	private long rows;            // 每页的行数
	private String sidx;          // 排序的
	private String sord;
	private String _search;
	private long nd;
	private String id;
	private String oper;
	private String edit;
	private String add;
	private String del;
	private long totalrows;
	private String subgridid;
	
	public long getPage() {
		return page;
	}
	public void setPage(long page) {
		this.page = page;
	}
	public long getRows() {
		return rows;
	}
	public void setRows(long rows) {
		this.rows = rows;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String get_search() {
		return _search;
	}
	public void set_search(String _search) {
		this._search = _search;
	}
	public long getNd() {
		return nd;
	}
	public void setNd(long nd) {
		this.nd = nd;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public String getEdit() {
		return edit;
	}
	public void setEdit(String edit) {
		this.edit = edit;
	}
	public String getAdd() {
		return add;
	}
	public void setAdd(String add) {
		this.add = add;
	}
	public String getDel() {
		return del;
	}
	public void setDel(String del) {
		this.del = del;
	}
	public long getTotalrows() {
		return totalrows;
	}
	public void setTotalrows(long totalrows) {
		this.totalrows = totalrows;
	}
	public String getSubgridid() {
		return subgridid;
	}
	public void setSubgridid(String subgridid) {
		this.subgridid = subgridid;
	}
	
	

}
