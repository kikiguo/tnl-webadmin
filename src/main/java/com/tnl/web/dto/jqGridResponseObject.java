package com.tnl.web.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class jqGridResponseObject <T> {
	private long page;  // 当前页
	private long total; // 总共多少页
	private long records;  // 总共多少行
	private  List<T> rows; // 数据
	
	public long getPage() {
		return page;
	}
	
	public void setPage(long page) {
		this.page = page;
	}
	
	public long getTotal() {
		return total;
	}
	
	public void setTotal(long total) {
		this.total = total;
	}
	
	public long getRecords() {
		return records;
	}
	
	public void setRecords(long records) {
		this.records = records;
	}
	
	public List<T> getRows() {
		return rows;
	}
	
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	
	public static long calTotalPage(long records, long pagesize) {
		if(records > 0 && pagesize > 0) {
			long total = records / pagesize;
			if (records % pagesize != 0) {
				total += 1;
			}
			return total;
		}
		return 1;
	}

}
