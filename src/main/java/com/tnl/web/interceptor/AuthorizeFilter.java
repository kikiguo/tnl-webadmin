package com.tnl.web.interceptor;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
  
  

public class AuthorizeFilter implements Filter {  
  private Logger logger = LoggerFactory.getLogger(AuthorizeFilter.class);
    /**  
     * Default constructor.   
     */  
    public AuthorizeFilter() {  
    }  
    /**  
     * @see Filter#init(FilterConfig)  
     */  
    public void init(FilterConfig fConfig) throws ServletException {  
    }  
  
    /**  
     * @see Filter#destroy()  
     */  
    public void destroy() {  
    }  
  
    /**  
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)  
     */  
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {  
    	HttpServletRequest req  = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String url    = req.getRequestURL().toString();
        logger.info("request url is "+ url);
   	 
   	    HttpSession session = req.getSession();
		Object objUid = session.getAttribute("UserId");
		if (objUid == null) {
			
			// 请求的路径
			String contextPath = req.getContextPath();
			logger.debug("contextPath = " + contextPath);
			res.sendRedirect(contextPath
					+ "/views/admin/platformadminlogin.jsp");
			
		
        } else {
        	
        	// pass the request along the filter chain  
    	logger.debug("do filter start");
        chain.doFilter(request, response);
        }
	}

	
    
}  