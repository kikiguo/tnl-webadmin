package com.tnl.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class SecurityInterceptor implements HandlerInterceptor {

	private static Logger logger = LoggerFactory
			.getLogger(SecurityInterceptor.class);

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		logger.debug("**afterCompletion**");
	}

	/** * 在业务处理器处理请求执行完成后,生成视图之前执行的动作 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object arg2, ModelAndView modelAndView)
			throws Exception {
		logger.debug("**postHandle**");
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.debug("**preHandle**");
		String url = request.getRequestURL().toString();
		
		HttpSession session = request.getSession();
		Object objUid = session.getAttribute("UserId");
		if (objUid == null) {
			
			// 请求的路径
			String contextPath = request.getContextPath();
			logger.debug("contextPath = " + contextPath);
			response.sendRedirect(contextPath + "/views/admin/platformadminlogin.jsp");
			return false;
		}
		return true;
	}
}