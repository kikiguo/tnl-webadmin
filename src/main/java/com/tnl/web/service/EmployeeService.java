package com.tnl.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.biz.hunter.dao.EmployeeDAO;
import com.biz.hunter.db.entity.Employee;

@Service
public class EmployeeService {

	@Autowired
	EmployeeDAO empDAO;
	public Page<Employee> find(String name, Pageable page) {
		// TODO Auto-generated method stub
		List<Employee> list = empDAO.getALLEmps();
		Page<Employee> emps = new PageImpl<Employee>(empDAO.getALLEmps(), page,list.size() );
		return emps;
	}

}
