package com.tnl.web.service;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
@Service
public class MailService {
	private static final Logger logger = LoggerFactory.getLogger(MailService.class);
	
	public String sendMail(String to,String content) {
		JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
		// 设定mail server
		senderImpl.setHost("smtp.163.com");

		// 建立邮件消息
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		// 设置收件人，寄件人 用数组发送多个邮件
		// String[] array = new String[] {"sun111@163.com","sun222@sohu.com"};
		// mailMessage.setTo(array);
		mailMessage.setTo(to);
		mailMessage.setFrom("m17721218360@163.com");
		mailMessage.setSubject("您的邮箱验证码");
		mailMessage.setText("您的邮箱验证码:"+ content);//content

		senderImpl.setUsername("m17721218360@163.com"); 
		senderImpl.setPassword("q072216"); 

		Properties prop = new Properties();
		prop.put(" mail.smtp.auth ", " true "); 
		prop.put(" mail.smtp.timeout ", " 25000 ");
		senderImpl.setJavaMailProperties(prop);
		// 发送邮件
		senderImpl.send(mailMessage);

		logger.info(" 邮件发送成功.. ");
		return "Success";
	}
}