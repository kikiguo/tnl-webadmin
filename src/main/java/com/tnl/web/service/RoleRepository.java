/**
 * 
 */
package com.tnl.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tnl.web.dao.PermissionDAO;
import com.tnl.web.dao.RoleDAO;
import com.tnl.web.domain.Permission;
import com.tnl.web.domain.Role;

/**
 * @author haigang.li
 *
 */
@Repository
public class RoleRepository {

	@Autowired
	private RoleDAO roleDAO;

	@Autowired
	private PermissionDAO permDAO;

	public Role retrieveRoleByName(String rolename) {
		Role role = roleDAO.findByRolename(rolename);
		List<Permission> permissions = permDAO.getRolesPermission(role
				.getIdrole());
		if (permissions != null) {
			role.setPermissions(permissions);
		}
		return role;
	}

}
