package com.tnl.web.service;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SMSService {
	private static final Logger logger = LoggerFactory.getLogger(SMSService.class);
	
	public String sendSMS(String code) throws Exception {
		
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod("http://sms.webchinese.cn/web_api/");
		post.addRequestHeader("Content-Type",
				"application/x-www-form-urlencoded;charset=gbk");// 在头文件中设置转码
		NameValuePair[] data = { new NameValuePair("Uid", "hunterF"), // 注册的用户名
				new NameValuePair("Key", "780ee6facad3c8c4b3ce"), // 注册成功后,登录网站使用的密钥
				new NameValuePair("smsMob", "17721218360"), // 手机号码
				new NameValuePair("smsText", "您的验证码是"+code) };// 设置短信内容
		post.setRequestBody(data);

		client.executeMethod(post);
		Header[] headers = post.getResponseHeaders();
		int statusCode = post.getStatusCode();
		logger.debug("statusCode:" + statusCode);
		for (Header h : headers) {
			logger.debug(h.toString());
		}
		String result = new String(post.getResponseBodyAsString().getBytes(
				"gbk"));
		logger.debug("result" + result);
		post.releaseConnection();
		return result;
	}
}