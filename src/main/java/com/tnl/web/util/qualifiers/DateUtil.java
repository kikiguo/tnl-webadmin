package com.tnl.web.util.qualifiers;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static String getDateTimeStr(long time){
		java.sql.Date date = new java.sql.Date(time*1000);
		SimpleDateFormat bartDateFormat =
	        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str = bartDateFormat.format(date);
		return str;
	}

	public static String getDateStr(long time){
		java.sql.Date date = new java.sql.Date(time);
		SimpleDateFormat bartDateFormat =
	        new SimpleDateFormat("yyyy-MM-dd");
		String str = bartDateFormat.format(date);
		return str;
	}

	public static String getDateTimeStr(Date date)
	{
		SimpleDateFormat bartDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String str = bartDateFormat.format(date);
		return str;
	}
	public static Date toDate(String dateStr) {

		if (dateStr == null || "".equals(dateStr))
			return null;
		Date date = null;
		SimpleDateFormat formater = new SimpleDateFormat();
		formater.applyPattern("yyyy-MM-dd");
		try {
			date = formater.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static long toTimeStamp(String tsStr) {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		
		try {
			ts = Timestamp.valueOf(tsStr);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ts.getTime();
	}

	public static String timestamp2String(Timestamp ts) {
		String tsStr = "";
		DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			tsStr = sdf.format(ts);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tsStr;
	}
	
}
