<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
    <title>HR公司信息</title>  
</head>
<body>
<div class="ui segment" id="n-page">
        <div class="ui segment" id="n-content">
            <form class="ui form">
             <c:if test="${company != null}">
                        <!-- disable the editable component -->   
             </c:if> 
                <div class="field">
                    <label>公司名称</label>
                    <select class="ui search dropdown" name="companyid" id="companyid">
                    <c:choose>
                        <c:when test="${(tempcompany.id == 0) and (tempcompany.name != '')}">
                            <option value="">${tempcompany.name}</option>
                        </c:when> 
                        <c:otherwise>
                            <option value="">请输入公司名称</option>
                        </c:otherwise>
                    </c:choose>
                    <c:forEach items="${companies}" var="comp">
                        <c:choose>
                            <c:when test="${comp.id == tempcompany.id }">
                                <option value="${comp.id}" selected="selected">${comp.name}</option>
                            </c:when> 
                            <c:otherwise> 
                                <option value="${comp.id}">${comp.name}</option>
                            </c:otherwise> 
                        </c:choose>  
                    </c:forEach>
                    </select>
                </div>
                
                <div class="field">
                    <label>组织机构代码</label>
                    <div class="ui input">
                        <input type="text" name="orgcode" id="orgcode" value="${tempcompany.orgnizationCode}" placeholder="请输入组织机构代码">
                    </div>
                </div>
                <div class="field">
                    <label>营业执照编码</label>
                    <div class="ui input">
                        <input type="text" name="bizcode" id="bizcode" value="${tempcompany.licenseCode}" placeholder="请输入营业执照编码">
                    </div>
                </div>                      
                <input type="hidden" name="employerId" id="employerId" value="${employer.id}"/>
            </form>        
        </div>
        <div class="ui segment" id="btn-next2">
            <div class="fluid ui button" id="next">下一步</div>
        </div>

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this)
                .addClass('active') 
                .siblings() 
                .removeClass('active'); 

            if($(this).attr('id') == 'next') {

                var companyid = $("#companyid").val();
                var hrid = $("#employerId").val();
                var companycontact = $("#companycontact").val();
                var companyphone   = $("#companyphone").val();
                var companyname    = "";
                var companyaddress = $("#companyaddress").val();
                var orgcode  = $("#orgcode").val();
                var bizcode  = $("#bizcode").val();
                var companyname2 = "${hunter.companyname}";
                var companyemail = $("#companyemail").val();
              //  var employerId =$("#employerId").val()
                if(companyid == "") {
                    //alert($("#companyId").dropdown('get search')[0]);
                    if(!$("#companyid").dropdown('get search')[0] || $("#companyid").dropdown('get search')[0]=="" ) {
                        if(companyname2!=$("#companyid").dropdown('get text')[0]) {
                            alert("您必须输入公司名称");
                            return false;
                        } 
                        companyname = companyname2;
                        companyid = 0;
                        
                    } else {
                        companyname = $("#companyid").dropdown('get search')[0];
                        companyid   = 0;
                    }
                }

                if(!companycontact) {
                    alert("您必须输入公司联系人");
                    return false;
                }
                if(!companyphone) {
                    alert("您必须输入公司电话");
                    return false;
                }
                if(!companyaddress) {
                    alert("您必须输入公司地址");
                    return false;
                }
                if(!orgcode) {
                    alert("您必须输入公司组织代码");
                    return false;
                }
                if(!bizcode) {
                    alert("您必须输入营业执照编码");
                    return false;
                }
                if(!companyemail) {
                    alert("您必须输入公司邮箱后缀");
                    return false;
                }

                var postUrl = '${ctx}/wechat/company/create.do';
                var param = {};
                param['companycontact']  = companycontact;
                param['id']   = companyid;
                param['companyname'] = companyname;
                param['companyphone'] = companyphone;
                param['companyaddress'] = companyaddress;
                param['companyemail'] = companyemail;
                param['hrid']    = hrid;
                param['orgcode'] = orgcode;
                param['bizcode'] = bizcode;

                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data: param,
                    success: function(response){ 
                    	if(response.errorcode=="200"){
                    		var companyId = response.data;
                            location.href = '${ctx}/wechat/hr/company/toUploadFile.page?hrId='+hrid+"&companyId="+companyId;
                        }else{
                        	alert(response.msg); 
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                
            }
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        
        $('.dropdown').dropdown({
			fullTextSearch:true,
		});
    });

</script>
</body>
</html>