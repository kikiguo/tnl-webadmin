<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>

<head>

<link href="${ctx}/resources/semanticui/semantic.min.css"
	rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
</head>

<body>


	<script type="text/javascript">
var validateTest = function() {
	var name = $("#name").val();
	
	return true;
};

var checkExistName = function(name) {
	var exists = false;
	var datas={};
	datas['name'] = name;
	$.ajax({
		url : "${ctx}/admin/employee/checkExistName.do",
		type : 'POST',
		async : false,
		data : {name:name},
		dataType : 'json',
		success : function(data, textStatus) {
			exists = data.exist;
			return exists;
		},
		error : function() {
			exists = true;
		}
	});
	return exists;
};

</script>

	<div class="content">
		<div class="content-guide">
			<span> <strong> 候选人管理 </strong>
			</span>
		</div>
		<div class="content-body">
			<form class="ui form">
				<div class="field">
					<label>请确认您所在的行业</label>

					<div class="ui center aligned segment" id="step1-promotion">
						<div class="ui buttons">
							<c:choose>
								<c:when test="${employee.domainId == 1 }">
									<div class="step1-choice ui button" id="drug">医药</div>
									<div class="step1-choice ui button active" id="medical">医疗</div>
								</c:when>
								<c:when test="${employee.domainId == 2 }">
									<div class="step1-choice ui button active" id="drug">医药</div>
									<div class="step1-choice ui button" id="medical">医疗</div>
								</c:when>
								<c:otherwise>
									<div class="step1-choice ui button" id="drug">医药</div>
									<div class="step1-choice ui button" id="medical">医疗</div>
								</c:otherwise>
							</c:choose>

						</div>
					</div>
				</div>
				<!-- 获取候选人对象 -->
				<c:if test="${employee != null}">
					<input type="hidden" name="empId" id="empId" value="${employee.id}" />
				</c:if>

				<div class="field">
					<label>姓名</label>
					<div class="ui input">
						<input type="text" id="name" name="name" value="${employee.name}"
							placeholder="请输入候选人的姓名">
					</div>
				</div>
				<div class="field">
					<label>年龄</label>
					<div class="ui input">
						<input type="text" id="age" name="age" value="${employee.age}"
							placeholder="请输入候选人的年龄">
					</div>
				</div>


				<div class="field">
					<label>工作年限</label>
					<div class="ui input">
						<input id="workAge" type="text" name="duration"
							value="${employee.workage}" placeholder="请输入现任工作年限">
					</div>
				</div>
				<div class="field">
					<label>联系方式</label>
					<div class="ui input">
						<input type="text" id="phone" name="phone"
							value="${employee.phone}" placeholder="请输入您的手机号码">
					</div>
				</div>
				<div class="field">
					<label>现任公司</label> <select name="companyId" id="companyId"
						class="ui search dropdown">
						<c:choose>
							<c:when
								test="${(employee.companyId == 0) and (employee.companyName != '')}">
								<option value="">${employee.companyName}</option>
							</c:when>
							<c:otherwise>
								<option value="">请输入现任公司名称</option>
							</c:otherwise>
						</c:choose>
						<c:forEach items="${companies}" var="company">
							<c:choose>
								<c:when test="${employee.companyId == company.id }">
									<option value="${company.id}" selected="selected">${company.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${company.id}">${company.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>现任职位</label> <select name="roleId" id="roleId"
						class="ui search dropdown">
						<option value="">请输入现任职位名称</option>
						<c:forEach items="${roles}" var="role">
							<c:choose>
								<c:when test="${employee.roleId == role.id }">
									<option value="${role.id}" selected="selected">${role.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${role.id}">${role.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>所在城市</label> <select name="cityId" id="cityId"
						class="ui search dropdown">
						<option value="">请输入所在城市名称</option>
						<c:forEach items="${cities}" var="city">
							<c:choose>
								<c:when test="${employee.cityId == city.id }">
									<option value="${city.id}" selected="selected">${city.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${city.id}">${city.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>管理区域</label>
					<div class="ui input">
						<input type="text" id="bestdomain" name="bestdomain"
							value="${employee.bestdomain}" placeholder="请输入管理区域名称">
					</div>
				</div>

				<div class="field">
					<label>期望加入公司(至少填入一个公司名)</label> <select name="expectCompany1"
						id="expectCompany1" class="ui search dropdown">
						<option value="">第一期望加入公司</option>
						<c:forEach items="${companies}" var="company">
							<c:choose>
								<c:when test="${employee.expectCompany1 == company.id }">
									<option value="${company.id}" selected="selected">${company.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${company.id}">${company.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<select name="expectCompany2" id="expectCompany2"
						class="ui search dropdown">
						<option value="">第二期望加入公司</option>
						<c:forEach items="${companies}" var="company">
							<c:choose>
								<c:when test="${employee.expectCompany2 == company.id }">
									<option value="${company.id}" selected="selected">${company.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${company.id}">${company.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<select name="expectCompany3" id="expectCompany3"
						class="ui search dropdown">
						<option value="">第三期望加入公司</option>
						<c:forEach items="${companies}" var="company">
							<c:choose>
								<c:when test="${employee.expectCompany3 == company.id }">
									<option value="${company.id}" selected="selected">${company.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${company.id}">${company.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>期望担任职位</label> <select name="expectJob" id="expectJob"
						class="ui search dropdown">
						<option value="">请输入期望担任职位名称</option>
						<c:forEach items="${roles}" var="role">
							<c:choose>
								<c:when test="${employee.expectJob == role.id }">
									<option value="${role.id}" selected="selected">${role.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${role.id}">${role.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				
				<div class="field">
					<label>何时联系我？</label> <select id="intentTime" name="intentTime"
						class="ui selection dropdown">
						<c:choose>
							<c:when test="${employee.intentTime == 1 }">
								<option value="1" selected="selected">一月后</option>
								<option value="6">半年后</option>
								<option value="12">一年后</option>
							</c:when>
							<c:when test="${employee.intentTime == 6 }">
								<option value="1">一月后</option>
								<option value="6" selected="selected">半年后</option>
								<option value="12">一年后</option>
							</c:when>
							<c:otherwise>
								<option value="1">一月后</option>
								<option value="6">半年后</option>
								<option value="12" selected="selected">一年后</option>
							</c:otherwise>
						</c:choose>
					</select>
				</div>

				
				<div class="field">
					<label>工作状态</label> <select id="inService"
						class="ui selection dropdown">
						<option value="0">离职</option>
						<option value="1">在职</option>
					</select>
				</div>
			</form>
		</div>
	</div>

	<div class="ui segment" id="cont-next">
		<div class="fluid ui button" id="next">保存</div>
	</div>
	<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
	<script src="${ctx}/resources/semanticui/semantic.js"
		type="text/javascript"></script>

	<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this)
                .addClass('active') 
                .siblings() 
                .removeClass('active'); 

            if($(this).attr('id') == 'next') {
                var expectCompany1 = $("#expectCompany1").val();
                var expectCompany2 = $("#expectCompany2").val();
                var expectCompany3 = $("#expectCompany3").val();
                var expectJob = $("#expectJob").val();
                var comment = $("#comment").val();
                var intentTime = $("#intentTime").val();
                var phone = $("#phone").val();
				var companyId = $("#companyId").val();
                var roleId = $("#roleId").val();
                var name = $("#name").val();
                var age = $("#age").val();
                var workAge = $("#workAge").val();
                var inService = $("#inService").val();
                var empId = $("#empId").val();
                
                if(!expectCompany1) {
                    alert("您必须至少输入第一个公司名称");
                    return false;
                }
                if(!expectCompany2) {
                    expectCompany2 = 0;
                }
                if(!expectCompany3) {
                    expectCompany3 = 0;
                }
                //if(!expectJob) {
                  //  alert("您必须输入期望担任职位名称");
                 //   return false;
                //}
               
                if(!phone) {
                    alert("您必须输入您的联系方式");
                    return false;
                }
                if(!roleId) {
                    alert("您必须选择一个职位名称");
                    return false;
                }
                if(!cityId) {
                    alert("您必须选择一个城市");
                    return false;
                }
                if(bestdomain == "") {
                    alert("您必须输入你的管理区域");
                    return false;
                }
               
            
                var param = {};
                param['name']=name;        
                param['phone']=phone;
                param['age']=age;
                param['workage']=workAge;
                param['inservice']=inService;
                param['companyId']   = companyId;
                param['roleId']=roleId;
            
                var postUrl = '${ctx}/admin/employee/create.do';
                if (empId && empId >0)
                {
                	param['id']=empId;       
                	postUrl = '${ctx}/admin/employee/update.do'
                }
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
            		data : param,
            		dataType : 'json',
                    success: function(data){ 
                    	if (data.errorcode=="0")
                    		{alert("保存成功");}
                        location.href = '${ctx}/admin/employee/manage.page';
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });

            }
           
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
			fullTextSearch:true,
		});
    });

</script>
</body>