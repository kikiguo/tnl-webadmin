<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>公司名归一化</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
    公司名归一化
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    返回求职人管理
    </button>
    <br><br>
    <input type="hidden" name="empId" id="empId" value="${employee.empid}"/>  
    <div class="ui segment" id="n-content">
        1. 使用现有公司替代<br><br>
        <form class="ui form">
            <div class="field">
                <label>选择公司名</label>
                <div class="ui search" id="existcompanyname" name="existcompanyname">
                    <div class="ui input">
                        <input class="prompt" type="text" placeholder="请输入公司名称" value="${employee.companyName}">
                    </div>
                    <div class="results"></div>
                </div>    
            </div>
            <div class="ui segment" id="cont-next">
                <div class="ui primary button" id="subsitute">替换归一化</div>
            </div>
        </form>
        <div class="ui horizontal divider">
        或者
        </div>
        2. 创建新的公司名并替代<br><br>
        <form class="ui form">
            <div class="field">
                <label>新的公司名称</label>
                <div class="ui search" id="newcompanyname" name="newcompanyname">
                    <div class="ui input">
                        <input class="prompt" type="text" placeholder="请输入公司名称">
                    </div>
                    <div class="results"></div>
                </div> 
            </div>
            <div class="field">
                <label>地址</label>
                <div class="ui input">
                    <input type="text" name="companyaddress" id="companyaddress" placeholder="请输入公司地址">
                </div>
            </div>
            <div class="field">
                <label>细分领域</label>
                <select class="ui selection dropdown" name="domainid" id="domainid">
                <option value="">请输入细分领域</option>
                <c:forEach items="${domains}" var="dm">
                    <c:choose>
                        <c:when test="${dm.id == tempcompany.domainid }">
                            <option value="${dm.id}" selected="selected">${dm.name}</option>
                        </c:when> 
                        <c:otherwise> 
                            <option value="${dm.id}">${dm.name}</option>
                        </c:otherwise> 
                    </c:choose>  
                </c:forEach>
                </select>
            </div>
            <div class="field">
                <label>联系人</label>
                <div class="ui input">
                    <input type="text" name="companycontact" id="companycontact" placeholder="请输入公司联系人">
                </div>
            </div>
            <div class="field">
                <label>联系电话</label>
                <div class="ui input">
                    <input type="text" name="companyphone" id="companyphone" placeholder="请输入公司联系电话">
                </div>
            </div>
            <div class="field">
                <label>公司邮箱</label>
                <div class="ui input">
                    <input type="text" name="companyemail" id="companyemail" placeholder="请输入公司邮箱">
                </div>
            </div>
            <div class="field">
                <label>组织机构代码</label>
                <div class="ui input">
                    <input type="text" name="orgcode" id="orgcode" placeholder="请输入组织机构代码">
                </div>
            </div>
            <div class="field">
                <label>营业执照编码</label>
                <div class="ui input">
                    <input type="text" name="bizcode" id="bizcode" placeholder="请输入营业执照编码">
                </div>
            </div>

            <div class="ui segment" id="cont-next">
                <div class="ui primary button" id="createcompany">创建并替换</div>
            </div>
        </form>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'subsitute') {
                var empId = $("#empId").val();
                var existcompanyname = $('#existcompanyname').search('get value');
                
                if(!existcompanyname) {
                    alert("必须输入公司名称");
                    return false;
                }
            
                var param = {};     
                param['name'] = existcompanyname;
  
                var postUrl = '${ctx}/admin/employee/normalise.do?op=subsitute&empid=' + empId;
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                            location.href = '${ctx}/admin/employee/manage.page';
                        } else {
                            alert(data.msg);
                        }
                        return;
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }

            if($(this).attr('id') == 'createcompany') {

                var empId = $("#empId").val();
                var newcompanyname = $('#newcompanyname').search('get value');


                var companyaddress = $("#companyaddress").val();
                var domainid = $("#domainid").val();
                var companycontact = $("#companycontact").val();

                var companyphone = $("#companyphone").val();
                var companyemail = $("#companyemail").val();
                var orgcode = $("#orgcode").val();
                var bizcode = $("#bizcode").val();
                
                if(!newcompanyname) {
                    alert("您必须输入公司名称");
                    return false;
                }
                if(!domainid) {
                    alert("您必须输入细分领域");
                    return false;
                }
                if(!companycontact) {
                    alert("您必须输入联系人");
                    return false;
                }
                if(!companyphone) {
                    alert("您必须输入公司联系电话");
                    return false;
                }
                if(companyemail == "") {
                    alert("您必须输入公司邮箱");
                    return false;
                }
                if(!orgcode) {
                    alert("您必须输入公司组织机构代码");
                    return false;
                }
                if(bizcode == "") {
                    alert("您必须输入公司营业执照编码");
                    return false;
                }
               
                var param = {};
                param['name'] = newcompanyname;        
                param['orgnizationCode'] = orgcode;
                param['licenseCode'] = bizcode;
                param['contact'] = companycontact;
                param['phone'] = companyphone;
                param['email'] = companyemail;
                param['address'] = companyaddress;
                param['domain']  = domainid; 
  
                var postUrl = '${ctx}/admin/employee/normalise.do?op=create&empid=' + empId;
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                            location.href = '${ctx}/admin/employee/manage.page';
                        } else {
                            alert(data.msg);
                            return;
                        } 
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });

            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/employee/manage.page';
                location.href = goUrl;
                return;
            }
           
        } 
    };

    var companydata = [];
    
    <c:forEach items="${companies}" var="company">
        var tmp${company.id} = {};
        tmp${company.id}.title = "${company.name}";
        companydata.push(tmp${company.id});
    </c:forEach>

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
            fullTextSearch:true,
        });
        $('.ui.search').search({
            source: companydata,
            searchFullText: true
        });
    });

</script>
</body>
<html>