<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>候选人详细信息</title>  
</head>
<body>

<div class="ui segment">

	<h2 class="ui sub header">
  	求职人详细信息
	</h2>

	<br>
	<button class="ui primary button" id="backward">
    返回求职人管理
	</button>
	<button class="ui primary button" id="edit">
  	编辑
	</button>
	<button class="ui primary button" id="del">
  	删除
	</button>
<c:if test="${employee.companyId == 0}">
	<button class="ui primary button" id="normalise">
  	公司名归一化
	</button>
</c:if>	

	<br><br>

    求职人所在的行业：<br>

    <div class="ui segment no-shadow">
<c:choose>
    <c:when test="${employee.domainId == 1 }">
        医疗 
        <br />
    </c:when>    
    <c:otherwise>
        医药
        <br />
    </c:otherwise>
</c:choose>        
    </div>

	<br><br>
    求职人的现状：
	<br>

    <div class="ui segment no-shadow">
        现任公司：
        ${employee.companyName}
        <br>
        现任职位：
<c:forEach items="${roles}" var="role">
    <c:if test="${role.id == employee.roleId}">
        ${role.name}
    </c:if>
</c:forEach>
        <br>
        所在城市：
<c:forEach items="${cities}" var="city">
    <c:if test="${city.id == employee.cityId}">
        ${city.name}
    </c:if>
</c:forEach>
        <br>
        管理区域：${employee.bestdomain}
    </div>

    <br><br>
    求职人的期望
    <br>

    <div class="ui segment no-shadow">
        期望加入公司：<br>
<c:forEach items="${companies}" var="company">
    <c:if test="${company.id == employee.expectCompany1}">
        &nbsp;&nbsp;&nbsp;&nbsp;${company.name}<br>
    </c:if>
</c:forEach>
<c:forEach items="${companies}" var="company">
    <c:if test="${company.id == employee.expectCompany2}">
        &nbsp;&nbsp;&nbsp;&nbsp;${company.name}<br>
    </c:if>
</c:forEach>
<c:forEach items="${companies}" var="company">
    <c:if test="${company.id == employee.expectCompany3}">
        &nbsp;&nbsp;&nbsp;&nbsp;${company.name}<br>
    </c:if>
</c:forEach>

        期望担任职位：
<c:forEach items="${roles}" var="role">
    <c:if test="${role.id == employee.expectJob}">
        ${role.name}
    </c:if>
</c:forEach>
        <br>
        特别备注：${employee.comment}<br>
        何时联系：
<c:choose>
    <c:when test="${employee.intentTime == '0' }">
        现在
    </c:when>  
    <c:when test="${employee.intentTime == '1' }">
        一月后
    </c:when>  
    <c:when test="${employee.intentTime == '6' }">
        半年后
    </c:when>   
    <c:otherwise>
        一年后
    </c:otherwise>
</c:choose> 
        <br>
        联系方式：${employee.phone}<br>
        &nbsp;<br>
    </div>


</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'backward') {
				var goUrl = '${ctx}/admin/employee/manage.page';
                location.href = goUrl;
                return;
            }
			if($(this).attr('id') == 'normalise') {
                var goUrl = '${ctx}/admin/employee/normalise.page?empid=${employee.empid}';
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'edit') {
                var companyid=0;
                <c:if test="${employee.companyId != 0}">
                companyid = ${employee.companyId};
                </c:if>
                if(companyid == 0) {
                    alert("该求职人的公司名需要归一化后才能编辑");
                    return;
                } 
                var goUrl = '${ctx}/admin/employee/form.page?empid=${employee.empid}';
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
            	var goUrl = '${ctx}/admin/employee/delete.do?empid=${employee.empid}';
				$.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        location.href = '${ctx}/admin/employee/manage.page';
                    	} else {
                    		alert(data.msg);
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
		} 
	};

	$(document).ready(function() { 
		$('.button').on('click', handler.activate)
	});

</script>
</body>
</html>