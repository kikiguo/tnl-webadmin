<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>HR公司详细信息</title>  
</head>
<body>

<div class="ui segment">

    <h2 class="ui sub header">
      HR公司详细信息
    </h2>

    <br>
    <button class="ui primary button" id="backward">
    返回HR公司管理
    </button>
    <button class="ui primary button" id="approve">
    认证
    </button>
    <button class="ui primary button" id="del">
    删除
    </button>

    <br><br><br>

    <b>公司信息：</b>&nbsp;&nbsp;&nbsp;&nbsp;
    <br><br>

    公司名称：${company.name}&nbsp;<br>&nbsp;<br>
    细分领域：
    <c:forEach items="${domains}" var="domain">
        <c:choose>
            <c:when test="${company.domain == domain.id }">
                ${domain.name}
            </c:when> 
        </c:choose>  
    </c:forEach>
    &nbsp;<br>&nbsp;<br>
    联系人：${company.contact}&nbsp;<br>&nbsp;<br>
    联系电话：${company.phone}&nbsp;<br>&nbsp;<br>
    公司地址：${company.address}&nbsp;<br>&nbsp;<br>
    组织机构代码：${company.orgnizationCode}&nbsp;<br>&nbsp;<br>
    公司营业执照编码：${company.licenseCode}&nbsp;<br>&nbsp;<br> 

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/hrcompany/manage.page';
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'approve') {
                var goUrl = '${ctx}/admin/hrcompany/form.page?companyid=${company.id}';
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
                var goUrl = '${ctx}/admin/hrcompany/delete.do?companyid=${company.id}';
                $.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            alert(data.msg);
                        location.href = '${ctx}/admin/hrcompany/manage.page';
                        } else {
                            alert(data.msg);
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate)
    });

</script>
</body>
</html>