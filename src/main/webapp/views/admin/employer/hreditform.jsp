<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>HR编辑</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
      HR编辑
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    返回HR管理
    </button>
    <br><br>
    <div class="ui segment" id="n-content">
        <form class="ui form">
            <div class="field">
                <label>姓名</label>
                <div class="ui input">
                    <input type="text" name="name" id="name" placeholder="请输入姓名" value="${hr.name}">
                </div>
            </div>
            <div class="field">
                <label>手机</label>
                <div class="ui input">
                    <input type="text" name="phone" id="phone" placeholder="请输入您的手机号码" value="${hr.phone}">
                </div>
            </div>
            <div class="field">
                <label>座机</label>
                <div class="ui input">
                    <input type="text" name="telephone" id="telephone" placeholder="请输入您的座机号码" value="${hr.telephone}">
                </div>
            </div>
            <div class="field">
                <label>邮箱</label>
                <div class="ui input">
                    <input type="text" name="email" id="email" placeholder="请输入您的邮箱" value="${hr.email}">
                </div>
            </div>
            <input type="hidden" name="hrid" id="hrid" value="${hr.id}"/>   
        </form>

        <div class="ui segment" id="cont-next">
            <div class="ui primary button" id="next">提交</div>
        </div>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'next') {
                var name = $("#name").val();
                var telephone = $("#telephone").val();
                var phone = $("#phone").val();
                var email = $("#email").val();
                var hrid  = $("#hrid").val();
                if(!name) {
                    alert("您必须输入您的名字");
                    return false;
                }
                if(!telephone) {
                    alert("您必须输入座机号码");
                    return false;
                }
                if(!phone) {
                    alert("您必须输入手机号码");
                    return false;
                }
                if(!email) {
                    alert("您必须输入电子邮箱");
                    return false;
                }
               
                var param = {};
                param['name']  = name;
                param['telephone'] = telephone;
                param['email']  = email;
                param['phone']  = phone;
                param['id'] = hrid;

                var postUrl = '${ctx}/admin/hr/update.do'
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data) { 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                        }
                        location.href = '${ctx}/admin/hr/manage.page';
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/hr/manage.page';
                location.href = goUrl;
                return;
            }
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
    });

</script>
</body>
<html>