<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
    <title>猎头信息</title>  
</head>
<body>
<div class="ui segment" id="n-page">
<a class="ui red right ribbon label">已认证</a>
&nbsp;<br>
姓名：${hunter.name}&nbsp;<br>&nbsp;<br>
年龄：${hunter.age}&nbsp;<br>&nbsp;<br>
擅长领域：${hunter.domainname}&nbsp;<br>&nbsp;<br>
从业时间：${hunter.workage}年&nbsp;<br>&nbsp;<br>
手机：${hunter.mobile}&nbsp;<br>&nbsp;<br>
座机：${hunter.phone}&nbsp;<br>&nbsp;<br>
邮箱：${hunter.email}&nbsp;<br>&nbsp;<br>

公司名称：${company.companyname}&nbsp;<br>&nbsp;<br>
联系人：${company.companycontact}&nbsp;<br>&nbsp;<br>
联系电话：${company.companyphone}&nbsp;<br>&nbsp;<br>
公司地址：${company.companyaddress}&nbsp;<br>&nbsp;<br>
组织机构代码：${company.orgcode}&nbsp;<br>&nbsp;<br>
公司营业执照编码：${company.bizcode}&nbsp;<br>&nbsp;<br>

<input type="hidden" name="hunterid" id="hunterid" value="${hunter.id}"/>

</div>
<div class="ui segment" id="btn-next">
    <div class="fluid ui button" id="next">修改信息</div>
</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this)
                .addClass('active') 
                .siblings() 
                .removeClass('active'); 
                
            var hunterid = $('#hunterid').val;
            var getUrl = '${ctx}/wechat/headhunter/hunter/toEdit.do';
            getUrl += '?hunterid=' + hunterid;

            if($(this).attr('id') == 'next') {
                location.href = getUrl;
            }
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
    });

</script>
</body>
</html>