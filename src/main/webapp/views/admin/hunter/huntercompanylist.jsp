<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>猎头公司管理</title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  猎头公司管理
</h2>
<br><br>
<button class="ui primary button" id="detail">
  详细信息
</button>
<c:if test="${system:checkalias(sessionScope.UserId,'editCompany')}">
<button class="ui primary button" id="edit">
  编辑
</button>
</c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'deleteCompany')}">
<button class="ui primary button" id="del">
  删除
</button>
</c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'reviewCompany')}">
<button class="ui primary button" id="check">
  公司认证
</button>
</c:if>
<br><br>

<div class="cont">
	<div class="ui input cell-div">
		<input type="text" name="search_name" id="search_name" placeholder="公司名称">
  	</div>
    <div class="ui input cell-div">
         <input type="text" name="search_phone" id="search_phone" placeholder="联系电话">
    </div>
  	<div class="cell-div2">
  		<select class="ui dropdown" name="search_status" id="search_status">
            <option value="">请选择公司状态</option>
            <option value="0">未认证公司</option>
            <option value="1">已认证公司</option>
        </select>
  	</div>
  	<div class="cell-div">
  		<button class="ui primary button" id="search">
  				搜索
		</button>
  	</div>
</div>

<br><br>
	<table id="grid"></table>
	<div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
            if($(this).attr('id') == 'detail') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要查看的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/huntercompany/detail.page?companyid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'check') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要认证的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var approved = rowdata['approved'];
                if(approved != "未认证") {
                    alert("该猎头公司已经认证了");
                    return;
                }
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/huntercompany/approve.do?companyid=' + coe;
                $.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            alert(data.msg);
                        location.href = '${ctx}/admin/huntercompany/manage.page';
                        } else {
                            alert(data.msg);
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }
            if($(this).attr('id') == 'edit') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要编辑的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/huntercompany/form.page?companyid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要删除的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/huntercompany/delete.do?companyid=' + coe;
				$.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        location.href = '${ctx}/admin/huntercompany/manage.page';
                    	} else {
                    		alert(data.msg);
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
		} 
	};

	$(document).ready(function() { 

		$("#grid").jqGrid({
			postData: {
				sname: function() { return $("#search_name").val(); },
				sphone: function() { return $("#search_phone").val(); },
                sstatus: function() { return $("#search_status").val(); }
			},
			mtype: 'POST',
			datatype : 'json',
			url : '${ctx}/admin/huntercompany/search.do',
			width: 980, 
            height:420,
			rowNum : 10,
			colNames:['编号','公司名', '联系人','联系电话','组织机构代码','认证状态'], 
			colModel:[ 
				{name:'id', index:'id', key: true, width:50},
				{name:'name', index:'name', width:100},
				{name:'contact', index:'contact', width:150},
                {name:'phone', index:'phone', width:100},
                {name:'orgnizationCode', index:'orgnizationCode', width:80},
                {name:'approved', index:'approved', width:100, formatter:approveFmatter}  
			],  
			pager: '#pager',  
			sortname: 'id',  
			viewrecords: true,  
			sortorder: 'asc', 
			loadComplete : function(data) {
				//console.log(data)
			} 
		});

		$("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
		//$("#grid").jqGrid('filterToolbar',{autosearch:true});
		$('.button').on('click', handler.activate);

	});

    function approveFmatter (cellvalue, options, rowObject) {
        if(cellvalue == 0) {
            return "未认证";
        } else {
            return "已认证";
        }
    }


</script>
</body>
</html>