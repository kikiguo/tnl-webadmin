<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<link href="${ctx}/resources/semanticui/semantic.min.css"
	rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
<title>职位表单</title>
</head>
<body>

	<div class="ui segment" id="content">

		<h2 class="ui sub header">职位编辑</h2>
		<br>
		<button class="ui primary button" id="backward">返回职位管理</button>
		<br>
		<br>
		<div class="ui segment" id="n-content">
			<form class="ui form">
				<div class="field">
					<label>职位名称</label>
					<div class="ui search" id="name" name="name">
						<div class="ui input">
							<input class="prompt" type="text" placeholder="请输入职位名称"
								value="${position.name}">
						</div>
						<div class="results"></div>
					</div>
				</div>
<div class="field">
                    <label>公司名称</label>
                    <select class="ui search dropdown" name="companyid" id="companyid">
                    <option value="">请输入公司名</option>
                    <c:forEach items="${companies}" var="company">  
                        <c:choose>
                            <c:when test="${position.companyId == company.id }">
                                <option value="${company.id}" selected="selected">${company.name}</option>
                            </c:when> 
                            <c:otherwise> 
                                <option value="${company.id}">${company.name}</option>
                            </c:otherwise> 
                        </c:choose>  
                    </c:forEach>              
                    </select>
                </div>
				<div class="field">
					<label>所在城市</label> <select name="cityId" id="cityId"
						class="ui search dropdown">
						<option value="">请输入所在城市名称</option>
						<c:forEach items="${cities}" var="city">
							<c:choose>
								<c:when test="${position.cityId == city.id }">
									<option value="${city.id}" selected="selected">${city.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${city.id}">${city.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>负责区域</label>
					<div class="ui input">
						<input type="text" name="location" id="location"
							value="${position.location}" placeholder="请输入区域">
					</div>
				</div>
				<div class="field">
					<label>下属人数</label>
					<div class="ui input">
						<input type="text" name="employeeNum" id="employeeNum"
							value="${position.employeeNum}" placeholder="请输入下属人数">
					</div>
				</div>
				<div class="field">
					<label>主要职责</label>
					<div class="ui input">
						<input type="text" name="majorDuty" id="majorDuty"
							value="${position.majorDuty}" placeholder="请输入主要职责">
					</div>
				</div>
				<div class="field">
					<label>推荐奖金</label>
					<div class="ui input">
						<input type="text" name="reward" id="reward"
							value="${position.reward}" placeholder="请输入推荐奖金">
					</div>
				</div>
				 <div class="field">
                    <label>期望候选人职位</label>
                    <div class="ui input">
                        <input type="text" name="expectJob" id="expectJob" value="${position.expectJob}" placeholder="请输入期望候选人职位">
                    </div>
                </div> 
                <div class="field">
                        <label>细分领域</label> <select class="ui search selection dropdown"
                            name="majordomain" id="majordomain" multiple="">
                            <option value="">请输入擅长领域</option>
                            <c:forEach items="${domains}" var="dm">

                                <option value="${dm.id}">${dm.name}</option>

                            </c:forEach>
                        </select>

                    </div>
				<div class="field">
					<label>期望公司来源1</label> <select class="ui search dropdown"
						name="expectcompanyid" id="expectcompanyid">
						<option value="">请输入期望来源公司的名称</option>
						<c:forEach items="${companies}" var="company">
							<c:choose>
								<c:when test="${position.expectcompanyid == company.id }">
									<option value="${company.id}" selected="selected">${company.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${company.id}">${company.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>期望公司来源2</label> <select class="ui search dropdown"
						name="expectcompanyid2" id="expectcompanyid2">
						<option value="">请输入期望来源公司的名称</option>
						<c:forEach items="${companies}" var="company">
							<c:choose>
								<c:when test="${position.expectcompanyid2 == company.id }">
									<option value="${company.id}" selected="selected">${company.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${company.id}">${company.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>期望公司来源3</label> <select class="ui search dropdown"
						name="expectcompanyid3" id="expectcompanyid3">
						<option value="">请输入期望来源公司的名称</option>
						<c:forEach items="${companies}" var="company">
							<c:choose>
								<c:when test="${position.expectcompanyid3 == company.id }">
									<option value="${company.id}" selected="selected">${company.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${company.id}">${company.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<div class="field">
					<label>有效期</label>
					<div class="ui input">
					   <c:choose>
                                <c:when test="${position.expiredDateStr != null }">
                                   <input type="date" name="expiredDateStr" id="expiredDateStr"
                            value="${position.expiredDateStr}">
                                </c:when>
                                <c:otherwise>
                                    <input type="date" name="expiredDateStr" id="expiredDateStr"
                             value="2016-12-31">
                                </c:otherwise>
                            </c:choose>
						
					</div>
				</div>
				<div class="field">
					<label>选择要发布的平台</label>
					<div class="ui center aligned segment" id="choice-buttons">
						<div class="two ui buttons">
							<c:choose>
								<c:when test="${position.platformID == 2 }">
									<div class="ui button active dog" id="hunter">猎头平台</div>
									<div class="ui button dog" id="recom">推荐平台</div>
								</c:when>
								<c:otherwise>
									<div class="ui button dog" id="hunter">猎头平台</div>
									<div class="ui button active dog" id="recom">推荐平台</div>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
				<input type="hidden" name="positionid" id="positionid"
					value="${position.id}" />

			</form>
			<br>
			<div class="ui segment" id="cont-next">
				<div class="ui primary button" id="next">提交</div>
			</div>

		</div>

	</div>

	<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
	<script src="${ctx}/resources/semanticui/semantic.js"
		type="text/javascript"></script>
	<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'next') {
                
                var param = {};

                var positionid  = $("#positionid").val();
                
                if(positionid && positionid > 0){
                  param['id']  = positionid;
                  var postUrl = '${ctx}/admin/job/update.do'
                }
                else{
                  var postUrl = '${ctx}/admin/job/create.do'
                }
                
                var companyId = $("#companyid").val();
                var cityid      = $("#cityId").val();
                var location    = $("#location").val();
                var employeeNum = $("#employeeNum").val()
                var majorDuty   = $("#majorDuty").val();
                var reward      = $("#reward").val();
                var expectcompanyid = $("#expectcompanyid").val();
                var expectcompanyid2 = $("#expectcompanyid2").val();
                var expectcompanyid3 = $("#expectcompanyid3").val();
                var expiredDateStr  = $("#expiredDateStr").val();
                var platformid  = 2;
                var name = $('#name').search('get value');;
                var expectJob     = $("#expectJob").val();
                if(!name) {
                    alert("您必须输入职位名称");
                    return false;
                }
                if($('#hunter').hasClass('active')) {
                    platformid = 2;
                } else {
                    platformid = 1;
                }

                if(!location) {
                    alert("您必须负责区域");
                    return false;
                }
                if(!employeeNum) {
                    alert("您必须输入下属人数");
                    return false;
                }
                if(!majorDuty) {
                    alert("您必须主要职责");
                    return false;
                }
                if(!reward) {
                    alert("您必须输入推荐奖金");
                    return false;
                }

             
                param['name'] = name;
                param['cityId']   = cityid;
                param['location'] = location;
                param['employeeNum'] = employeeNum;
                param['majorDuty']   = majorDuty;
                param['reward'] = reward;
                param['companyId'] =companyId;
                param['expectcompanyid'] = expectcompanyid;
                param['expectcompanyid2'] = expectcompanyid2;
                param['expectcompanyid3'] = expectcompanyid3;
                param['expiredDateStr']  = expiredDateStr;
                param['platformID']      = platformid;
                param['expectJob']      = expectJob;
                var arrayLength = majordomain.length;
                if (arrayLength>0)
                  {
                  param['majorDomainId1'] = majordomain[0];
                  }
                if (arrayLength>1)
                {
                param['majorDomainId2'] = majordomain[1];
                }if (arrayLength>2)
                  {
                  param['majorDomainId3'] = majordomain[2];
                  }
                
                var majordomain  = $("#majordomain").val();


                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data: param,
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            alert(data.msg);
                            //window.location.href = '${ctx}/admin/job/manage.page';
                        } else {
                            alert(data.msg);
                            //location.href = '${ctx}/admin/error.page';
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/job/manage.page';
                location.href = goUrl;
                return;
            }
        } 
    };

    var jobtitledata = [];
    
    <c:forEach items="${roles}" var="jobtitle">
        var tmp${jobtitle.id} = {};
        tmp${jobtitle.id}.title = "${jobtitle.name}";
        jobtitledata.push(tmp${jobtitle.id});
    </c:forEach>

    $(document).ready(function() { 
        $('.button').on('click', handler.activate)
        $('.dropdown').dropdown({
            fullTextSearch:true,
        });
        $('.ui.search').search({
            source: jobtitledata,
            searchFullText: true
        });

    });

</script>
</body>
</html>