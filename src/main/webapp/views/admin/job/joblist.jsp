<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>职位管理</title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  职位管理
</h2>
<br><br>
<button class="ui primary button" id="create">
  创建
</button>
<button class="ui primary button" id="publish">
  发布
</button>
<c:if test="${system:checkalias(sessionScope.UserId,'editJob')}">
<button class="ui primary button" id="edit">
  编辑
</button>
</c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'deleteJob')}">
<button class="ui primary button" id="del">
  删除
</button>
</c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'normaliseJob')}">
<button class="ui primary button" id="normalise">
  职位名归一化
</button>
</c:if>
<br><br>

<div class="cont">
    <div class="ui input cell-div">
        <input type="text" name="search_name" id="search_name" placeholder="职位名称">
    </div>
    <div class="cell-div">
        <select class="ui dropdown" name="search_status" id="search_status">
            <option value="">请选择职位状态</option>
            <option value="0">未发布</option>
            <option value="1">已发布</option>
        </select>
    </div>
    <div class="cell-div2">
        <select class="ui dropdown" name="search_company" id="search_company">
            <option value="">请选择所在公司</option>
            <c:forEach items="${companies}" var="company">  
                <option value="${company.id}">${company.name}</option>
            </c:forEach>
        </select>
    </div>
    <div class="cell-div">
        <button class="ui primary button" id="search">
            搜索
        </button>
    </div>
</div>

<br><br>
    <table id="grid"></table>
    <div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
            if($(this).attr('id') == 'publish') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要发布的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/job/publish.do?jobid=' + coe;
                $.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            alert(data.msg);
                        location.href = '${ctx}/admin/job/manage.page';
                        } else {
                            alert(data.msg);
                            //location.href = '${ctx}/admin/error.page';
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
            if($(this).attr('id') == 'edit') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要编辑的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var roleid = rowdata['roleid'];
                if(roleid) {
                    alert("该职位的名称需要归一化处理");
                    return;
                }
                var goUrl = '${ctx}/admin/job/form.page?coe=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'create') {
             
             
              var goUrl = '${ctx}/admin/job/toCreate.page';
              location.href = goUrl;
              return;
          }
            if($(this).attr('id') == 'normalise') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要归一化的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var roleid = rowdata['roleid'];
                if(!roleid) {
                    alert("该职位的名称不需要归一化处理");
                    return;
                }
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/job/normalise.page?jobid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要编辑的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/job/delete.do?jobid=' + coe;
                $.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            alert(data.msg);
                        location.href = '${ctx}/admin/job/manage.page';
                        } else {
                            alert(data.msg);
                            //location.href = '${ctx}/admin/error.page';
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
        } 
    };

    $(document).ready(function() { 

        $("#grid").jqGrid({
            postData: {
                sname: function() { return $("#search_name").val(); },
                scompanyid: function() { return $("#search_company").val(); },
        sstatus: function() { return $("#search_status").val(); }
            },
            mtype: 'POST',
            datatype : 'json',
            url : '${ctx}/admin/job/search.do',
            width: 910, 
            height:420,
            rowNum : 10,
            colNames:['编号', '归一化', '职位名称','发布公司','期待来源公司','平台','状态','奖金','领域','有效期'], 
            colModel:[ 
                {name:'id', index:'id', key: true, width:50}, 
                {name:'roleid', index:'roleid', width:50, formatter:roleFmatter},
                {name:'name', index:'name', width:150},
                {name:'companyName', index:'companyName', width:150},
                {name:'expCompanyName', index:'expCompanyName', width:150},
                {name:'platformName', index:'platformName', width:100},
                {name:'statusStr', index:'statusStr', width:100},
                {name:'reward', index:'reward', width:60} ,
                {name:'domainid', index:'domainid', width:60} ,
                {name:'expiredDate', index:'expireddate', width:60} 
            ],  
            pager: '#pager',  
            sortname: 'id',  
            viewrecords: true,  
            sortorder: 'asc', 
            loadComplete : function(data) {
                //console.log(data)
            } 
        });

        $("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
        //$("#grid").jqGrid('filterToolbar',{autosearch:true});
        $('.button').on('click', handler.activate);

    });

    function roleFmatter (cellvalue, options, rowObject) {
        var roledata = "";
        
        if(cellvalue == 0) {
            roledata = "<img src='${ctx}/resources/images/normalise.png' alt='需要归一化职位名' />";
        }
        return roledata; 
    }


</script>
</body>
</html>