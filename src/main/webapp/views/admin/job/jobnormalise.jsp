<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>职位名归一化</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
    职位名归一化
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    返回职位管理
    </button>
    <br><br>
    <input type="hidden" name="jobid" id="jobid" value="${job.id}"/>  
    <div class="ui segment" id="n-content">
        1. 使用现有职位名替代<br><br>
        <form class="ui form">
            <div class="field">
                <label>选择职位名</label>
                <div class="ui search" id="existjobtitle" name="existjobtitle">
                    <div class="ui input">
                        <input class="prompt" type="text" placeholder="请输入职位名称" value="${job.name}">
                    </div>
                    <div class="results"></div>
                </div>    
            </div>
            <div class="ui segment" id="cont-next">
                <div class="ui primary button" id="subsitute">替换归一化</div>
            </div>
        </form>
        <div class="ui horizontal divider">
        或者
        </div>
        2. 创建新的职位名并替代<br><br>
        <form class="ui form">
            <div class="field">
                <label>新的职位名称</label>
                <div class="ui search" id="newjobtitle" name="newjobtitle">
                    <div class="ui input">
                        <input class="prompt" type="text" placeholder="请输入职位名称">
                    </div>
                    <div class="results"></div>
                </div> 
            </div>
            <div class="field">
                <label>职位分类</label>
                <select class="ui dropdown" id="postype" name="postype">
                    <option value="1">技术</option>
                    <option value="2">市场</option>
                    <option value="3">营销</option>
                    <option value="4">管理</option>
                </select>
            </div>

            <div class="ui segment" id="cont-next">
                <div class="ui primary button" id="createjobtitle">创建并替换</div>
            </div>
        </form>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'subsitute') {
                var jobid = $("#jobid").val();
                var existjobtitle = $('#existjobtitle').search('get value');
                
                if(!existjobtitle) {
                    alert("必须输入公司名称");
                    return false;
                }
            
                var param = {};     
                param['name'] = existjobtitle;
  
                var postUrl = '${ctx}/admin/job/normalise.do?op=subsitute&jobid=' + jobid;
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                            location.href = '${ctx}/admin/job/manage.page';
                        } else {
                            alert(data.msg);
                        }
                        return;
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }

            if($(this).attr('id') == 'createjobtitle') {

                var jobid = $("#jobid").val();
                var newjobtitle = $('#newjobtitle').search('get value');
                var postype = $("#postype").val();

                if(!postype) {
                    alert("您必须输入职位类型");
                    return false;
                }
                if(!newjobtitle) {
                    alert("您必须输入职位名称");
                    return false;
                }
               
                var param = {};
                param['name'] = newjobtitle;        
                param['postype'] = postype;
  
                var postUrl = '${ctx}/admin/job/normalise.do?op=create&jobid=' + jobid;
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                            location.href = '${ctx}/admin/job/manage.page';
                        } else {
                            alert(data.msg);
                            return;
                        } 
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });

            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/job/manage.page';
                location.href = goUrl;
                return;
            }
           
        } 
    };

    var jobtitledata = [];
    
    <c:forEach items="${jobtitles}" var="jobtitle">
        var tmp${jobtitle.id} = {};
        tmp${jobtitle.id}.title = "${jobtitle.name}";
        jobtitledata.push(tmp${jobtitle.id});
    </c:forEach>

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
            fullTextSearch:true,
        });
        $('.ui.search').search({
            source: jobtitledata,
            searchFullText: true
        });
    });

</script>
</body>
<html>