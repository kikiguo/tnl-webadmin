<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>职位名称表单</title>  
</head>
<body>

<div class="ui segment" id="content">

	<h2 class="ui sub header">
  	职位名称创建/编辑
	</h2>
	<div class="ui segment" id="n-content">
		<form class="ui form">
			<div class="field">
                <label>职位分类</label>
                <select class="ui dropdown" id="postype" name="postype">
                <c:choose>
                    <c:when test="${jobtitle.postype == 1 }">
                        <option value="1" selected="selected">技术</option>
                    </c:when> 
                    <c:otherwise> 
                        <option value="1">技术</option>
                    </c:otherwise> 
                </c:choose>
                <c:choose>
                    <c:when test="${jobtitle.postype == 2 }">
                        <option value="2" selected="selected">市场</option>
                    </c:when> 
                    <c:otherwise> 
                        <option value="2">市场</option>
                    </c:otherwise> 
                </c:choose>
                <c:choose>
                    <c:when test="${jobtitle.postype == 3 }">
                        <option value="3" selected="selected">营销</option>
                    </c:when> 
                    <c:otherwise> 
                        <option value="3">营销</option>
                    </c:otherwise> 
                </c:choose>
                <c:choose>
                    <c:when test="${jobtitle.postype == 4 }">
                        <option value="4" selected="selected">管理</option>
                    </c:when> 
                    <c:otherwise> 
                        <option value="4">管理</option>
                    </c:otherwise> 
                </c:choose>
                </select>
            </div>
            <div class="field">
                <label>职位名称</label>
                <div class="ui input">
                    <input type="text" name="name" id="name" value="${jobtitle.name}" placeholder="输入职位的名称">
                </div>
            </div>
		</form>
		<br>
		<input type="hidden" name="jobtitleid" id="jobtitleid" value="${jobtitle.id}"/> 
		<div class="ui segment" id="cont-next">
        	<div class="ui primary button" id="next">提交</div>
    	</div>

	</div>

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'next') {
				var postUrl = '${ctx}/admin/jobtitle/update.do'
                var param = {};

                var jobtitleid = $("#jobtitleid").val();
                var postype = $("#postype").val();
                var name = $("#name").val();

                if(!name) {
                    alert("您必须输入职位名称");
                    return false;
                }

                param['postype'] = postype;
                param['id']   = jobtitleid;
                param['name'] = name;

                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data: param,
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        	location.href = '${ctx}/admin/jobtitle/manage.page';
                    	} else {
                    		alert(data.msg);
                    		//location.href = '${ctx}/admin/error.page';
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }
		} 
	};

	$(document).ready(function() { 
		$('.button').on('click', handler.activate)
		$('.dropdown').dropdown({
			fullTextSearch:true,
		});
	});

</script>
</body>
</html>