<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>职位名称管理</title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  职位名称管理
</h2>
<br><br>

<c:if test="${system:checkalias(sessionScope.UserId,'createJob')}">
<button class="ui primary button" id="add">
  创建
</button>
</c:if>

<c:if test="${system:checkalias(sessionScope.UserId,'editJob')}">
<button class="ui primary button" id="edit">
  编辑
</button>
</c:if>

<c:if test="${system:checkalias(sessionScope.UserId,'deleteJob')}">
<button class="ui primary button" id="del">
  删除
</button>
</c:if>
<br><br>

<div class="cont">
	<div class="ui input cell-div">
		<input type="text" name="search_name" id="search_name" placeholder="职位名称">
  	</div>
  	<div class="cell-div">
  		<select class="ui dropdown" name="search_postype" id="search_postype">
            <option value="">请选择职位类型</option>
            <option value="1">技术</option>
            <option value="2">市场</option>
            <option value="3">营销</option>
            <option value="4">管理</option>
        </select>
  	</div>
  	<div class="cell-div">
  		<button class="ui primary button" id="search">
  				搜索
		</button>
  	</div>
</div>

<br><br>
	<table id="grid"></table>
	<div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
			if($(this).attr('id') == 'add') {
                var goUrl = '${ctx}/admin/jobtitle/form.page?coe=0';
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'edit') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要编辑的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/jobtitle/form.page?coe=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要编辑的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/jobtitle/delete.do?jobtitleid=' + coe;
				$.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        	location.href = '${ctx}/admin/jobtitle/manage.page';
                    	} else {
                    		alert(data.msg);
                    		//location.href = '${ctx}/admin/error.page';
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
		} 
	};

	$(document).ready(function() { 

		$("#grid").jqGrid({
			postData: {
				sname: function() { return $("#search_name").val(); },
				spostype: function() { return $("#search_postype").val(); },
			},
			mtype: 'POST',
			datatype : "json",
			url : '${ctx}/admin/jobtitle/search.do',
			width: 800, 
			rowNum : 10,
			colNames:['编号','职位名称','职位分类'], 
			colModel:[ 
				{name:'id', index:'id', key: true, width:50}, 
				{name:'name', index:'name', width:200},
				{name:'postypeStr', index:'postypeStr', width:300} 
			],  
			pager: '#pager',  
			sortname: 'id',  
			viewrecords: true,  
			sortorder: "asc", 
			loadComplete : function(data) {
				//console.log(data)
			} 
		});

		$("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
		//$("#grid").jqGrid('filterToolbar',{autosearch:true});
		$('.button').on('click', handler.activate);

	});

</script>
</body>
</html>