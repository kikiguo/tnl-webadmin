<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<%@ taglib uri="http://www.tnl.com/system" prefix="system"%> 
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/leftmenu.css" rel="stylesheet" type="text/css" />
    <title>左边菜单</title>  
</head>
<body>

<div class="ui tiny header"><br>&nbsp;&nbsp;菜单<br></div>
<div class="ui vertical menu">
  <c:if test="${system:checkalias(sessionScope.UserId,'employee_view')}">
  <div class="item">
    <div class="header">求职人管理</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/employee/manage.page' target='funwindow'>求职人列表</a>
    </div>
  </div>
  </c:if> 
   <c:if test="${system:checkalias(sessionScope.UserId,'hunter_view')}">
 <div class="item">
    <div class="header">猎头管理</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/hunter/manage.page' target='funwindow'>猎头列表</a>
      <a class="item" href='${ctx}/admin/huntercompany/manage.page' target='funwindow'>猎头公司管理</a>
    </div>
   
  </div>
  </c:if>
  <c:if test="${system:checkalias(sessionScope.UserId,'hr_view')}">
    <div class="item">
    <div class="header">HR管理</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/hr/manage.page' target='funwindow'>HR列表</a>
      <a class="item" href='${ctx}/admin/hrcompany/manage.page' target='funwindow'>HR公司管理</a>
    </div>
     
  </div>
   </c:if> 
 <c:if test="${system:checkalias(sessionScope.UserId,'company_view')}">
    <div class="item">
   <div class="header">公司管理</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/company/manage.page' target='funwindow'>公司列表</a>
    </div>
    
  </div>
   </c:if> 
  <c:if test="${system:checkalias(sessionScope.UserId,'job_view')}">
 <div class="item">
     <div class="header">职位管理</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/job/manage.page' target='funwindow'>职位管理</a>
      <a class="item" href='${ctx}/admin/jobtitle/manage.page' target='funwindow'>职位名称管理</a>
    </div>
     
  </div>
 </c:if> 
 
  <c:if test="${system:checkalias(sessionScope.UserId,'recommend_view')}">
  <div class="item">
    <div class="header">面试管理</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/recommend/manage.page' target='funwindow'>推荐列表</a>
    </div>
     
  </div>
</c:if> 
  <c:if test="${system:checkalias(sessionScope.UserId,'report_view')}">
  <div class="item">
    <div class="header">直通车报告</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/report/manage.page?page=1' target='funwindow'>行业分类</a>
      <a class="item" href='${ctx}/admin/report/manage.page?page=2' target='funwindow'>现任公司</a>
      <a class="item" href='${ctx}/admin/report/manage.page?page=3' target='funwindow'>现任职位</a>
      <a class="item" href='${ctx}/admin/report/manage.page?page=4' target='funwindow'>所在城市</a>
      <a class="item" href='${ctx}/admin/report/manage.page?page=5' target='funwindow'>期望加入公司</a>
      <a class="item" href='${ctx}/admin/report/manage.page?page=6' target='funwindow'>期望担任职位</a>
      <a class="item" href='${ctx}/admin/report/manage.page?page=7' target='funwindow'>何时联系</a>
    </div>
       
  </div>
  </c:if>
   <c:if test="${system:checkalias(sessionScope.UserId,'conf_view')}">
 
  <div class="item">
    <div class="header">设置</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/config/domain.page' target='funwindow'>细分领域管理</a>
      <a class="item" href='${ctx}/admin/config/jobtype.page' target='funwindow'>职位分类管理</a>
    </div>
    
  </div>
    </c:if> 
   <!--  c:if test="${system:checkalias(sessionScope.UserId,'admin_view')}">
   -->
  <div class="item">
  <div class="header">用户管理</div>
    <div class="menu">
      <a class="item" href='${ctx}/admin/logout.page' >登出</a>
    </div>
  </div>

</div>
  
<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function() { 

	});

</script>
</body>
</html>