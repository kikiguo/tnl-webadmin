<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>左边菜单</title>  
</head>
<body>

<div class="ui segment">
	欢迎使用管理平台
</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 
		} 
	};

	$(document).ready(function() { 
		$('.button').on('click', handler.activate)
		$('.dropdown').dropdown({
			fullTextSearch:true,
		});
	});

</script>
</body>
</html>