<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>

<html>
<head>
    <meta charset="UTF-8"> 
    <link href="${ctx}/resources/admin.css" rel="stylesheet" type="text/css" /> 
    <title>后台管理</title>  
</head>
<body>

<iframe id="leftmenu" src="${ctx}/views/admin/leftmenu.jsp"></iframe>
<iframe id="funwindow" name="funwindow" src="${ctx}/views/admin/main.jsp" onload="this.height=funwindow.document.body.scrollHeight" ></iframe>

</body>
</html>