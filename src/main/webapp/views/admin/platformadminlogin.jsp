<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/login.css" rel="stylesheet" type="text/css" />
    <title>后台管理</title>  
</head>
<body>

<div class="ui raised segment" id="signin">
	<h3 class="ui block header" id="signinheader">登录</h3>
	<div class="ui blue segment">
		<form class="ui form">
			<div class="field">
				<label>用户名</label>
                <div class="ui input">
                        <input type="text" id="username" placeholder="请输入你的用户名">
                </div>
            </div>
            <div class="field">
                <label>密码</label>
                <div class="ui input">
                    <input type="password" id="password" placeholder="请输入密码">
                </div>
            </div>
		</form>
		<br>
		<br>
		<div id="login" class="fluid ui red submit button">登录</div>
	</div>
</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
	var handler = { 
		activate: function() { 
			
			$(this)
				.addClass('active') 
				.siblings() 
				.removeClass('active'); 
			
			 var postUrl = '${ctx}/login.do';
			var username = $("#username").val();
             var password     = $("#password").val();
             var param = {};
             param['username']  = username;
             param['password']  = password;
			$.ajax({ 
                url: postUrl, 
                type: "POST", 
                data: param,
                success: function(data){ 
                    //alert('success'); 
                    location.href = '${ctx}/admin/main.page';
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
		} 
	};

	$(document).ready(function() { 
		$('.button').on('click', handler.activate)
		$('.dropdown').dropdown({
			fullTextSearch:true,
		});
	});
	
	//session过期的跳转出iframe
	if (window != top) 
	{top.location.href = location.href; }
	
</script>
</body>
</html>