<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>推荐详细信息 </title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  推荐详细信息
</h2>
<br><br>
<!--
<button class="ui primary button" id="add">
  创建
</button>
-->
<button class="ui primary button" id="backward">
   返回推荐列表
</button>
<!--
<button class="ui primary button" id="del">
  删除
</button>
-->
<br><br>

<div class="cont">
  	<div class="cell-div">
  		<select class="ui dropdown" name="search_status" id="search_status">
            <option value="">请选择面试状态</option>
            <option value="1">已发面试邀请</option>
            <option value="2">建议继续面试</option>
            <option value="3">拒绝</option>
            <option value="4">录用</option>
            <option value="4">奖金已发放</option>
        </select>
  	</div>
  	<div class="cell-div">
  		<button class="ui primary button" id="search">
  				搜索
		</button>
  	</div>
</div>

<br><br>
	<table id="grid"></table>
	<div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
            if($(this).attr('id') == 'backward') {
				var goUrl = '${ctx}/admin/recommend/manage.page';
                location.href = goUrl;
                return;
            }
		} 
	};

	$(document).ready(function() { 

		$("#grid").jqGrid({
			postData: {
				sstatus: function() { return $("#search_status").val(); },
			},
			mtype: 'POST',
			datatype : "json",
			url : '${ctx}/admin/interview/search.do?inid=' + ${recommend.id},
			width: 800, 
            height:450,
			rowNum : 10,
			colNames:['编号','更新时间', '面试类型', '状态'], 
			colModel:[ 
				{name:'id', index:'id', key: true, width:50}, 
				{name:'timeStr', index:'timeStr', width:200},
				{name:'itypeStr', index:'itypeStr', width:200},
                {name:'statusStr', index:'statusStr', width:200}
			],  
			pager: '#pager',  
			sortname: 'id',  
			viewrecords: true,  
			sortorder: "asc", 
			loadComplete : function(data) {
				//console.log(data)
			} 
		});

		$("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
		//$("#grid").jqGrid('filterToolbar',{autosearch:true});
		$('.button').on('click', handler.activate);

	});

</script>
</body>
</html>