<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>推荐管理</title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  推荐列表
</h2>
<br><br>
<!--
<button class="ui primary button" id="add">
  创建
</button>
-->
<button class="ui primary button" id="browser">
  查看推荐详细信息
</button>
<!--
<button class="ui primary button" id="del">
  删除
</button>
-->
<button class="ui primary button" id="permit">
  允许继续面试
</button>
<br><br>

<div class="cont">
	<div class="ui input cell-div">
		<input type="text" name="search_name" id="search_name" placeholder="职位名称">
  	</div>
  	<div class="cell-div">
  		<select class="ui dropdown" name="search_status" id="search_status">
            <option value="">请选择推荐状态</option>
            <option value="0">等待面试邀请</option>
            <option value="1">推荐中</option>
            <option value="2">推荐成功</option>
            <option value="3">推荐失败</option>
        </select>
  	</div>
  	<div class="cell-div">
  		<button class="ui primary button" id="search">
  				搜索
		</button>
  	</div>
</div>

<br><br>
	<table id="grid"></table>
	<div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
            if($(this).attr('id') == 'browser') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要查看的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/recommend/detail.page?rdid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'permit') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要操作的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/recommend/update.do?rdid=' + coe;
				$.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        	location.href = '${ctx}/admin/recommend/manage.page';
                    	} else {
                    		alert(data.msg);
                    		//location.href = '${ctx}/admin/error.page';
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
		} 
	};

	$(document).ready(function() { 

		$("#grid").jqGrid({
			postData: {
				sname: function() { return $("#search_name").val(); },
				sstatus: function() { return $("#search_status").val(); },
			},
			mtype: 'POST',
			datatype : "json",
			url : '${ctx}/admin/recommend/search.do',
			width: 800, 
            height:450,
			rowNum : 10,
			colNames:['编号','职位名称','公司', '候选人', '状态'], 
			colModel:[ 
				{name:'id', index:'id', key: true, width:50}, 
				{name:'jobName', index:'jobName', width:200},
				{name:'companyName', index:'companyName', width:200},
                {name:'personName', index:'personName', width:200},
                {name:'statusStr', index:'statusStr', width:100}  
			],  
			pager: '#pager',  
			sortname: 'id',  
			viewrecords: true,  
			sortorder: "asc", 
			loadComplete : function(data) {
				//console.log(data)
			} 
		});

		$("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
		//$("#grid").jqGrid('filterToolbar',{autosearch:true});
		$('.button').on('click', handler.activate);

	});

</script>
</body>
</html>