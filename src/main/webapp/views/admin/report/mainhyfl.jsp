<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>行业分类</title>  
</head>
<body>

<div class="ui segment">
    
    <h3 class="ui top attached header">
    行业分布
    </h3> 
    <div class="ui attached segment"> 
    <table class="ui celled table" id="indtable">
        <tr>
            <td class="td1"><div id="medical">4536(45.3%) 医疗</div></td>
            <td class="td2"><div id="drug">医药 5469(54.7%)</div></td>
        </tr>
    </table> 

    </div> 

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    var datasource = { 
        getdata: function() { 
            var getUrl = '${ctx}/admin/report/hyfl.do';
            var that = this;
            $.ajax({ 
                    url: getUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            that.appenddata(data);
                        } else {
                            var errorUrl = '${ctx}/admin/error.page';
                            location.href = errorUrl;
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
            });
        },

        appenddata:function(data) { 
            var medical = data.medical;
            var drug = data.drug;

            var total = medical + drug;
            if(total == 0) {
                medical = "0(0%) 医疗";
                drug = "医药 0(0%)";
            } else {
                medicalp = medical / total * 100;
                drugp = drug / total * 100;

                medicalp = Math.round(medicalp*100)/100;
                drugp = Math.round(drugp*100)/100;

                medicalp += "%";
                drugp += "%";

                medical = medical + "(" + medicalp + ")" + " 医疗";
                drug = "医药 " + drug + "(" + drugp + ")";
            }

            $('#medical').text(medical);
            $('#drug').text(drug);
        }
    };

    $(document).ready(function() { 
        datasource.getdata();
    });

</script>
</body>
</html>