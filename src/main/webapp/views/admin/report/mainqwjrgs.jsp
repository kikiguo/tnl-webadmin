<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqplot/jquery.jqplot.css" rel="stylesheet" type="text/css"/>
    <title>期望加入公司</title>  
</head>
<body>

<div class="ui segment">
    <div class="ui segment">
        <div class="ui form">
            <div class="field">
                <label>选择细分领域</label>
                <select class="ui dropdown" id="subdomain" name="subdomain">
                <c:forEach items="${domains}" var="domain">  
                    <option value="${domain.id}">${domain.name}</option>     
                </c:forEach>
                </select>
            </div>
        </div>
        <h3 class="ui top attached header">
            细分领域最受候选人青睐的十大公司
        </h3>
        <div class="ui attached segment">
            <div id="chartdiv1" class="charCanv"></div>
            <br>
            <button class="ui primary button" id="save1">
                保存为图片
            </button>
            &nbsp;
            <br>&nbsp;<br>
            <div id="chartdiv2" class="charCanv"></div>
            <br>
            <button class="ui primary button" id="save2">
                保存为图片
            </button>
        </div>
    </div>

    <div class="ui segment">
        <div class="ui form">
            <div class="field">
                <label>选择公司</label>
                <select class="ui dropdown" id="company" name="company">
                <c:forEach items="${companies}" var="company">  
                    <option value="${company.id}">${company.name}</option>     
                </c:forEach>
                </select>
            </div>
        </div>

        <h3 class="ui top attached header">
            最想加入公司的来源
        </h3>
        <table class="ui celled table">
            <tbody>
            <tr>
                <td></td>
                <td>
                    <div id="chartdiv30" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv31" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv32" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv33" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv34" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv35" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv36" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv37" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv38" class="charCanv2"></div>
                </td>
                <td>
                    <div id="chartdiv39" class="charCanv2"></div>
                </td>
            </tr>
            <tr>
                <td>临床教育</td>
                <td><div id="cell00"></div></td>
                <td><div id="cell01"></div></td>
                <td><div id="cell02"></div></td>
                <td><div id="cell03"></div></td>
                <td><div id="cell04"></div></td>
                <td><div id="cell05"></div></td>
                <td><div id="cell06"></div></td>
                <td><div id="cell07"></div></td>
                <td><div id="cell08"></div></td>
                <td><div id="cell09"></div></td>
            </tr>
            <tr>
                <td>注册/法规</td>
                <td><div id="cell10"></div></td>
                <td><div id="cell11"></div></td>
                <td><div id="cell12"></div></td>
                <td><div id="cell13"></div></td>
                <td><div id="cell14"></div></td>
                <td><div id="cell15"></div></td>
                <td><div id="cell16"></div></td>
                <td><div id="cell17"></div></td>
                <td><div id="cell18"></div></td>
                <td><div id="cell19"></div></td>
            </tr>
            <tr>
                <td>技术支持</td>
                <td><div id="cell20"></div></td>
                <td><div id="cell21"></div></td>
                <td><div id="cell22"></div></td>
                <td><div id="cell23"></div></td>
                <td><div id="cell24"></div></td>
                <td><div id="cell25"></div></td>
                <td><div id="cell26"></div></td>
                <td><div id="cell27"></div></td>
                <td><div id="cell28"></div></td>
                <td><div id="cell29"></div></td>
            </tr>
            <tr>
                <td>售后服务</td>
                <td><div id="cell30"></div></td>
                <td><div id="cell31"></div></td>
                <td><div id="cell32"></div></td>
                <td><div id="cell33"></div></td>
                <td><div id="cell34"></div></td>
                <td><div id="cell35"></div></td>
                <td><div id="cell36"></div></td>
                <td><div id="cell37"></div></td>
                <td><div id="cell38"></div></td>
                <td><div id="cell39"></div></td>
            </tr>
            <tr>
                <td>销售</td>
                <td><div id="cell40"></div></td>
                <td><div id="cell41"></div></td>
                <td><div id="cell42"></div></td>
                <td><div id="cell43"></div></td>
                <td><div id="cell44"></div></td>
                <td><div id="cell45"></div></td>
                <td><div id="cell46"></div></td>
                <td><div id="cell47"></div></td>
                <td><div id="cell48"></div></td>
                <td><div id="cell49"></div></td>
            </tr>
            <tr>
                <td>市场</td>
                <td><div id="cell50"></div></td>
                <td><div id="cell51"></div></td>
                <td><div id="cell52"></div></td>
                <td><div id="cell53"></div></td>
                <td><div id="cell54"></div></td>
                <td><div id="cell55"></div></td>
                <td><div id="cell56"></div></td>
                <td><div id="cell57"></div></td>
                <td><div id="cell58"></div></td>
                <td><div id="cell59"></div></td>
            </tr>
        </tbody>
        </table>

    </div> 
</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/jquery.jqplot.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pieRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.barRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.donutRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pointLabels.min.js" type="text/javascript" ></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.categoryAxisRenderer.min.js" type="text/javascript" ></script>

<script type="text/javascript">

    var plot1, plot2;

    var datasource = {
        getdata1: function() {
            var subdomain = $('#subdomain').val();
            if(!subdomain) {
                subdomain = 3;
            }

            var getUrl = '${ctx}/admin/report/qwjrgs1.do?subdomain=' + subdomain;
            var that = this;
            $.ajax({ 
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata1(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata1:function(data) {

            $.jqplot.config.enablePlugins = true;
            var colors = ["#EAA228", "#EAA228", "#EAA228", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5"];

            //var jsondata1 = {
            //    subdomainname:"IDD",
            //    data:[684, 599, 578, 496, 456, 358, 278, 253, 178, 154],
            //    companyticks:['公司A', '公司B', '公司C', '公司D', '公司E', '公司F', '公司G', '公司H', '公司I', '公司J']
            //};
            var jsondata1 = data.data;

            var s1 = jsondata1.data;
            var ticks = jsondata1.companyticks;
            var subdomainname = jsondata1.subdomainname;

            //var s2 = [399, 434, 344, 343, 488];
            if(plot1) { 
                plot1.destroy(); 
            }
            plot1 = $.jqplot('chartdiv1', [s1], {
                seriesColors: colors, 
                stackSeries: false,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    rendererOptions: {
                        varyBarColor: true
                    },
                    shadow: false,
                    showDataLabels: true,
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        showTicks: false,
                    }
                },
                highlighter: { show: false },
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: subdomainname + '细分领域最受候选人青睐的十大公司', 
            });

            if(plot2) { 
                plot2.destroy(); 
            }
            plot2 = $.jqplot('chartdiv2', [s1], {
                seriesColors: colors, 
                stackSeries: false,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    rendererOptions: {
                        varyBarColor: true
                    },
                    shadow: false,
                    pointLabels: {show: true, labels:['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'] }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        showTicks: false,
                    }
                },
                highlighter: { show: false },
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: subdomainname + '细分领域最受候选人青睐的十大公司', 
            });
        },

        getdata2: function() {
            var company = $('#company').val();
            if(!company) {
                company = 1;
            }

            var getUrl = '${ctx}/admin/report/qwjrgs2.do?company=' + company;
            var that = this;
            $.ajax({ 
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata2(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata2:function(data) { 

            $.jqplot.config.enablePlugins = true;

            //var jsondata2 = {
            //    companyname:"罗氏制药",
            //    positionticks:['临床教育','注册法规','技术支持','售后服务','销售','市场'],
            //    positiondata:[[84, 99, 78, 96, 56, 58, 78, 53, 78, 54],
            //        [68, 59, 57, 49, 45, 35, 27, 25, 17, 15],
            //        [95, 199, 38, 86, 96, 88, 98, 73, 88, 64],
            //        [62, 58, 55, 46, 41, 33, 25, 28, 15, 19],
            //        [44, 49, 28, 36, 96, 88, 98, 73, 88, 64],
            //        [24, 79, 55, 36, 41, 88, 25, 28, 15, 45]
            //    ]
            //};

            var jsondata2 = data.data;

            for(var i=0; i<10; i++) {
                for(var j=0; j<6; j++) {
                    var str = "cell"+j+i;
                    $('#'+str).text(jsondata2.positiondata[j][i]);
                }
                var divstr = "chartdiv3" + i;
                var datacompany1 = [jsondata2.positiondata[0][i]];
                var datacompany2 = [jsondata2.positiondata[1][i]];
                var datacompany3 = [jsondata2.positiondata[2][i]];
                var datacompany4 = [jsondata2.positiondata[3][i]];
                var datacompany5 = [jsondata2.positiondata[4][i]];
                var datacompany6 = [jsondata2.positiondata[5][i]];

                if(window['plot3'+i]) { 
                    window['plot3'+i].destroy(); 
                }

                window['plot3'+i] = $.jqplot(divstr, [datacompany1, datacompany2, datacompany3, datacompany4, datacompany5, datacompany6], {
                    stackSeries: true,
                    animate: !$.jqplot.use_excanvas,
                    seriesDefaults:{
                        renderer:$.jqplot.BarRenderer,
                        shadow: false,
                        pointLabels: {show: false}
                    },
                    axes: {
                        xaxis: {
                            showTicks: false,
                        },
                        yaxis: {
                            showTicks: false,
                        }
                    },
                    highlighter: { show: false },
                    grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                });
            }
        },
    };

    $(document).ready(function() {

        var $dropdown1 = $('#company');
        $dropdown1.dropdown({
            onChange: function(value) {
            }
        });

        var $dropdown2 = $('#subdomain');
        $dropdown2.dropdown({
            onChange: function(value) {
            }
        });

        var handler = {
            activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'save1') {
                $('#chartdiv1').jqplotSaveImage();
            }
            if($(this).attr('id') == 'save2') {
                $('#chartdiv2').jqplotSaveImage();
            }
        } 
    };

    $('.button').on('click', handler.activate);

  //$.jqplot.config.enablePlugins = true;
  //var colors = ["#EAA228", "#EAA228", "#EAA228", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5"];

  //var jsondata1 = {
  //  subdomainname:"IDD",
  //  data:[684, 599, 578, 496, 456, 358, 278, 253, 178, 154],
  //  companyticks:['公司A', '公司B', '公司C', '公司D', '公司E', '公司F', '公司G', '公司H', '公司I', '公司J']
  //};

  //var s1 = jsondata1.data;
  //var ticks = jsondata1.companyticks;
  //var subdomainname = jsondata1.subdomainname;

  //var s2 = [399, 434, 344, 343, 488];
  //var plot1 = $.jqplot('chartdiv1', [s1], {
  //  seriesColors: colors, 
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //      rendererOptions: {
  //      varyBarColor: true
  //    },
  //    shadow: false,
  //    showDataLabels: true,
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: ticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: { show: false },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域最受候选人青睐的十大公司', 
  //});

  //var plot2 = $.jqplot('chartdiv2', [s1], {
  //  seriesColors: colors, 
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    rendererOptions: {
  //      varyBarColor: true
  //    },
  //   shadow: false,
  //    pointLabels: {show: true, labels:['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'] }
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: ticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: { show: false },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域最受候选人青睐的十大公司', 
  //});

  //var jsondata2 = {
  //  companyname:"罗氏制药",
  //  positionticks:['临床教育','注册法规','技术支持','售后服务','销售','市场'],
  //  positiondata:[[84, 99, 78, 96, 56, 58, 78, 53, 78, 54],
  //    [68, 59, 57, 49, 45, 35, 27, 25, 17, 15],
  //    [95, 199, 38, 86, 96, 88, 98, 73, 88, 64],
  //    [62, 58, 55, 46, 41, 33, 25, 28, 15, 19],
  //    [44, 49, 28, 36, 96, 88, 98, 73, 88, 64],
  //    [24, 79, 55, 36, 41, 88, 25, 28, 15, 45]
  //  ]
  //};

  //for(var i=0; i<10; i++) {
  //  for(var j=0; j<6; j++) {
  //    var str = "cell"+j+i;
  //    $('#'+str).text(jsondata2.positiondata[j][i]);
  //  }
  //  var divstr = "chartdiv3" + i;
  //  var datacompany1 = [jsondata2.positiondata[0][i]];
  //  var datacompany2 = [jsondata2.positiondata[1][i]];
  //  var datacompany3 = [jsondata2.positiondata[2][i]];
  //  var datacompany4 = [jsondata2.positiondata[3][i]];
  //  var datacompany5 = [jsondata2.positiondata[4][i]];
  //  var datacompany6 = [jsondata2.positiondata[5][i]];

  //  var plot3 = $.jqplot(divstr, [datacompany1, datacompany2, datacompany3, datacompany4, datacompany5, datacompany6], {
  //    stackSeries: true,
  //    animate: !$.jqplot.use_excanvas,
  //    seriesDefaults:{
  //      renderer:$.jqplot.BarRenderer,
  //      shadow: false,
  //      pointLabels: {show: false}
  //    },
  //    axes: {
  //      xaxis: {
  //        showTicks: false,
  //      },
  //      yaxis: {
  //        showTicks: false,
  //      }
  //    },
  //    highlighter: { show: false },
  //    grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  });
  //}

});

</script>
</body>
</html>