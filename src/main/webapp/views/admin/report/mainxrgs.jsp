<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqplot/jquery.jqplot.css" rel="stylesheet" type="text/css"/>
    <title>现任公司</title>  
</head>
<body>

<div class="ui segment">
    <div class="ui segment">
        <div class="ui form">
            <div class="field">
                <label>选择细分领域</label>
                <select class="ui dropdown" id="subdomain" name="subdomain">
                <c:forEach items="${domains}" var="domain">  
                    <option value="${domain.id}">${domain.name}</option>     
                </c:forEach>
                </select>
            </div>
        </div>

        <h3 class="ui top attached header">
            细分领域前十大公司分布
        </h3>
        <div class="ui attached segment">
            <div id="chartdiv1" class="charCanv"></div>
            <br>
            <button class="ui primary button" id="save1">
                保存为图片
            </button>
            &nbsp;
            <br>&nbsp;<br>
            <div id="chartdiv2" class="charCanv"></div>
            <br>
            <button class="ui primary button" id="save2">
                保存为图片
            </button>
        </div>
  
    </div>

    <h3 class="ui top attached header">
        细分领域人才分布
    </h3>
    <div class="ui attached segment">
        <div id="chartdiv3" class="charCanv"></div>
        <br>
        <button class="ui primary button"  id="save3">
            保存为图片
        </button>
        &nbsp;
        <br>&nbsp;<br>
        <div id="chartdiv4" class="charCanv"></div>
        <br>
        <button class="ui primary button" id="save4">
            保存为图片
        </button>
    </div>

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/jquery.jqplot.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pieRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.barRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.donutRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pointLabels.min.js" type="text/javascript" ></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.categoryAxisRenderer.min.js" type="text/javascript" ></script>

<script type="text/javascript">

    var plot1, plot2, plot3, plot4;

    var datasource = {
        getdata1: function() {
            var subdomain = $('#subdomain').val();
            if(!subdomain) {
                subdomain = 3;
            }

            var getUrl = '${ctx}/admin/report/xrgs1.do?subdomain=' + subdomain;
            var that = this;
            $.ajax({ 
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata1(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata1:function(data) {

            $.jqplot.config.enablePlugins = true;

            var colors = ["#EAA228", "#EAA228", "#EAA228", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5"];

            //var jsondata1 = {
            //    subdomainname:"IVD",
            //    subdomainticks:['公司A', '公司B', '公司C', '公司D', '公司E', '公司F', '公司G', '公司H', '公司I', '公司J'],
            //    data:[684, 599, 578, 496, 456, 358, 278, 253, 178, 154]
            //};

            var jsondata1 = data.data;

            var s1 = jsondata1.data;
            var ticks = jsondata1.subdomainticks;
            var subdomainname = jsondata1.subdomainname;

            if(plot1) { 
                plot1.destroy(); 
            }
            plot1 = $.jqplot('chartdiv1', [s1], {
                seriesColors: colors,
                stackSeries: false,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    rendererOptions: {
                        varyBarColor: true
                    },
                    shadow: false,
                    showDataLabels: true,
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        showTicks: false,
                    }
                },
                highlighter: { show: false },
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: subdomainname + '细分领域前十大主要公司分布', 
            });

            if(plot2) { 
                plot2.destroy(); 
            }
            plot2 = $.jqplot('chartdiv2', [s1], { 
                seriesColors: colors, 
                stackSeries: false,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    rendererOptions: {
                        varyBarColor: true
                    },
                    shadow: false,
                    pointLabels: {show: true, labels:['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'] }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        showTicks: false,
                    }
                },
                highlighter: {show: false},
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: subdomainname + '细分领域前十大主要公司分布',
            });
        },

        getdata2: function() {

            var getUrl = '${ctx}/admin/report/xrgs2.do';
            var that = this;
            $.ajax({ 
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata2(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata2:function(data) {
            $.jqplot.config.enablePlugins = true;
            var colors = ["#EAA228", "#EAA228", "#EAA228", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5"];
            //var jsondata2 = [
            //    {subdomainticks:['普药', '特药', '新药'], data:[879, 1259, 459]}, 
            //    {subdomainticks:[], data:[]}
            //];

            var jsondata2 = data.data;

            var s3 =  jsondata2[0].data;
            var ticks2 = jsondata2[0].subdomainticks;

            if(plot3) { 
                plot3.destroy(); 
            }
            plot3 = $.jqplot('chartdiv3', [s3], { 
                seriesColors: colors,
                stackSeries: false,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    rendererOptions: {
                        barDirection: 'horizontal'
                    },
                    shadow: false,
                    showDataLabels: true,
                },
                axes: {
                    yaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks2
                    },
                    xaxis: {
                        showTicks: false,
                    }
                },
                highlighter: { show: false },
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: '各细分领域人才分布', 
            });

            var s3sum = 0;
            for(var i=0; i<s3.length; i++) {
                s3sum += s3[i];
            }

            var labels2 = [];
            for(var i=0; i<s3.length; i++) { 
                var tmp = s3[i]/s3sum*100;
                tmp = Math.round(tmp*100)/100;
                tmp = tmp + '%';
                labels2.push(tmp);
            }

            if(plot4) { 
                plot4.destroy(); 
            }
            plot4 = $.jqplot('chartdiv4', [s3], {
                seriesColors: colors, 
                stackSeries: false,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    rendererOptions: {
                        barDirection: 'horizontal'
                    },
                    shadow: false,
                    pointLabels: {show: true, labels:labels2}
                },
                axes: {
                    yaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks2
                    },
                    xaxis: {
                        showTicks: false,
                    }
                },
                highlighter: {show: false},
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: '各细分领域人才分布', 
            });
        },
    };

    $(document).ready(function() {
        var $dropdown2 = $('#subdomain');
        $dropdown2.dropdown({
            onChange: function(value) {
                datasource.getdata1();
            }
        });

        var handler = {
            activate: function() {
                $(this).addClass('active').siblings().removeClass('active'); 

                if($(this).attr('id') == 'save1') {
                    $('#chartdiv1').jqplotSaveImage();
                }
                if($(this).attr('id') == 'save2') {
                    $('#chartdiv2').jqplotSaveImage();
                }
                if($(this).attr('id') == 'save3') {
                    $('#chartdiv3').jqplotSaveImage();
                }
                if($(this).attr('id') == 'save4') {
                    $('#chartdiv4').jqplotSaveImage();
                }
            }
        };
        $('.button').on('click', handler.activate);

        datasource.getdata2();

  //$.jqplot.config.enablePlugins = true;

  //var colors = ["#EAA228", "#EAA228", "#EAA228", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5", "#4bb2c5"];

  //var jsondata1 = {
  //  subdomainname:"IVD",
  //  subdomainticks:['公司A', '公司B', '公司C', '公司D', '公司E', '公司F', '公司G', '公司H', '公司I', '公司J'],
  //  data:[684, 599, 578, 496, 456, 358, 278, 253, 178, 154]
  //};

  //var s1 = jsondata1.data;
  //var ticks = jsondata1.subdomainticks;
  //var subdomainname = jsondata1.subdomainname;
         
  //var plot1 = $.jqplot('chartdiv1', [s1], {
  //  seriesColors: colors, 
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    rendererOptions: {
  //      varyBarColor: true
  //    },
  //    shadow: false,
  //    showDataLabels: true,
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: ticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: { show: false },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域前十大主要公司分布', 
  //});

  //var plot2 = $.jqplot('chartdiv2', [s1], {
  //  seriesColors: colors, 
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    rendererOptions: {
  //      varyBarColor: true
  //    },
  //    shadow: false,
  //    pointLabels: {show: true, labels:['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'] }
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: ticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: {show: false},
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域前十大主要公司分布',
  //});

  //// plot3 and plot3 
  //var jsondata2 = [
  //  {subdomainticks:['普药', '特药', '新药'], data:[879, 1259, 459]}, 
  //  {subdomainticks:[], data:[]}
  //];

  //var s3 =  jsondata2[0].data;
  //var ticks2 = jsondata2[0].subdomainticks;

  //var plot3 = $.jqplot('chartdiv3', [s3], { 
  //  seriesColors: colors,
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    rendererOptions: {
  //      barDirection: 'horizontal'
  //    },
  //    shadow: false,
  //    showDataLabels: true,
  //  },
  //  axes: {
  //    yaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: ticks2
  //    },
  //    xaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: { show: false },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: '各细分领域人才分布', 
  //});

  //var s3sum = 0;
  //for(var i=0; i<s3.length; i++) {
  //  s3sum += s3[i];
  //}

  //var labels2 = [];
  //for(var i=0; i<s3.length; i++) { 
  //  var tmp = s3[i]/s3sum*100;
  //  tmp = Math.round(tmp*100)/100;
  //  tmp = tmp + '%';
  //  labels2.push(tmp);
  //}

  //var plot4 = $.jqplot('chartdiv4', [s3], {
  //  seriesColors: colors, 
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    rendererOptions: {
  //      barDirection: 'horizontal'
  //    },
  //    shadow: false,
  //    pointLabels: {show: true, labels:labels2}
  //  },
  //  axes: {
  //    yaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: ticks2
  //    },
  //    xaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: {show: false},
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: '各细分领域人才分布', 
  //});

});

</script>
</body>
</html>