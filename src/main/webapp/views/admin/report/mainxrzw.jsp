<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqplot/jquery.jqplot.css" rel="stylesheet" type="text/css"/>
    <title>现任职位</title>  
</head>
<body>

<div class="ui segment">
    <div class="ui segment">
        <div class="ui form">
            <div class="field">
                <label>选择细分领域</label>
                <select class="ui dropdown" id="subdomain" name="subdomain">
                <c:forEach items="${domains}" var="domain">  
                    <option value="${domain.id}">${domain.name}</option>     
                </c:forEach>
                </select>
            </div>
        </div>
        <h3 class="ui top attached header">
            细分领域人才分布情况
        </h3>
        <div class="ui attached segment">
            <div id="chartdiv1" class="charCanv"></div>
            <br>
            <button class="ui primary button" id="save1">
                保存为图片
            </button>
            &nbsp;
            <br>&nbsp;<br>
            <div id="chartdiv2" class="charCanv"></div>
            <br>
            <button class="ui primary button" id="save2">
            保存为图片
            </button>
        </div>
    </div>
</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/jquery.jqplot.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pieRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.barRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.donutRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pointLabels.min.js" type="text/javascript" ></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.categoryAxisRenderer.min.js" type="text/javascript" ></script>

<script type="text/javascript">

    var plot1, plot2;
    var datasource = {
        getdata1: function() {
            var subdomain = $('#subdomain').val();
            if(!subdomain) {
                subdomain = 3;
            }

            var getUrl = '${ctx}/admin/report/xrzw.do?subdomain=' + subdomain;
            var that = this;
            $.ajax({ 
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata1(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata1:function(data) {

            $.jqplot.config.enablePlugins = true;
            //var jsondata1 = { 
            //    subdomainname:"IVD", 
            //    data:[['研发',158], ['临床教育',289], ['注册/法规',298], ['技术支持',388], ['售后服务',456], ['生产', 597], //['市场',897], ['销售',1598]] 
            //}; 
            var jsondata1 = data.data;

            var s1 = jsondata1.data; 
            var subdomainname = jsondata1.subdomainname;

            var labels2 = [];
            for(var i=0; i<s1.length; i++) { 
                var tmp = s1[i][0] + "-" + s1[i][1]; 
                labels2.push(tmp); 
            }

            if(plot1) { 
                plot1.destroy(); 
            }

            plot1 = $.jqplot('chartdiv1', [s1], {
                stackSeries: false,
                animate: !$.jqplot.use_excanvas, 
                seriesDefaults:{ 
                    renderer:$.jqplot.PieRenderer,
                    rendererOptions: {
                        showDataLabels: true,
                        dataLabels:labels2
                    },
                    shadow: false,
                },
                highlighter: {show: false},
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: subdomainname + '细分领域人才职位分布情况', 
            });

            var labels3 = [];
            var sum = 0;
            for(var i=0; i<s1.length; i++) {
                sum += s1[i][1];
            }
            for(var i=0; i<s1.length; i++) {
                var per = s1[i][1]/sum*100;
                per = Math.round(per*100)/100;
                per = per + '%';
                var tmp = s1[i][0] + "-" + per;
                labels3.push(tmp);
            }

            if(plot2) { 
                plot2.destroy(); 
            }
            plot2 = $.jqplot('chartdiv2', [s1], {
                stackSeries: false,
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.PieRenderer,
                    rendererOptions: {
                        showDataLabels: true,
                        dataLabels:labels3
                    },
                    shadow: false,
                },
                highlighter: {show: false},
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: subdomainname + '细分领域人才职位分布情况',
            });

        },

        //appenddata2:function(data) {
        //},
    };

    $(document).ready(function() {

        var $dropdown2 = $('#subdomain'); 
        $dropdown2.dropdown({ 
            onChange: function(value) {
                datasource.getdata1();
            } 
        });

        var handler = { 
            activate: function() { 
                $(this).addClass('active').siblings().removeClass('active');  
                if($(this).attr('id') == 'save1') { 
                    $('#chartdiv1').jqplotSaveImage(); 
                } 
                if($(this).attr('id') == 'save2') { 
                    $('#chartdiv2').jqplotSaveImage(); 
                } 
            }  
        }; 
        $('.button').on('click', handler.activate);

  
  //var jsondata1 = {
  //  subdomainname:"IVD",
  //  data:[['研发',158], ['临床教育',289], ['注册/法规',298], ['技术支持',388], ['售后服务',456], ['生产', 597], ['市场',897], ['销售',1598]]
  //};

  //var s1 = jsondata1.data;
  //var subdomainname = jsondata1.subdomainname;

  //var labels2 = [];
  //for(var i=0; i<s1.length; i++) {
  //  var tmp = s1[i][0] + "-" + s1[i][1];
  //  labels2.push(tmp);
  //}

  //var plot1 = $.jqplot('chartdiv1', [s1], { 
  //  stackSeries: false, 
  //  animate: !$.jqplot.use_excanvas, 
  //  seriesDefaults:{ 
  //    renderer:$.jqplot.PieRenderer, 
  //    rendererOptions: { 
  //      showDataLabels: true, 
  //      dataLabels:labels2 
  //    },
  //    shadow: false,
  //  },
  //  highlighter: {show: false},
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域人才职位分布情况', 
  //});

  //var labels3 = [];
  //var sum = 0;
  //for(var i=0; i<s1.length; i++) { 
  //  sum += s1[i][1];
  //}
  //for(var i=0; i<s1.length; i++) { 
  //  var per = s1[i][1]/sum*100;
  //  per = Math.round(per*100)/100;
  //  per = per + '%';
  //  var tmp = s1[i][0] + "-" + per;
  //  labels3.push(tmp);
  //}

  //var plot2 = $.jqplot('chartdiv2', [s1], {
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.PieRenderer,
  //    rendererOptions: {
  //      showDataLabels: true,
  //      dataLabels:labels3
  //    },
  //    shadow: false,
  //  },
  //  highlighter: {show: false},
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域人才职位分布情况', 
  //});


    });

</script>
</body>
</html>