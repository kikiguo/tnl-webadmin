<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!DOCTYPE html>
<html data-embedded="">
<head>
  <meta charset="utf-8">
  <title>跳哪里邀请您加入中国医疗人才库</title>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
  <meta name="description" content= '为跳哪里邀请您加入中国医疗人才库提交数据。&amp;lt;p&amp;gt;近来医疗行业并购重组活跃，人才流动频繁，我们正在进行人才动向调查并提供职位帮助。希望通过...'>
  <meta property="og:title" content='跳哪里邀请您加入中国医疗人才库' >
<meta property="og:description" content='为跳哪里邀请您加入中国医疗人才库提交数据。&amp;lt;p&amp;gt;近来医疗行业并购重组活跃，人才流动频繁，我们正在进行人才动向调查并提供职位帮助。希望通过...'>
<meta property="og:image" content='https://cdn.jinshuju.net/assets/weixin_thumbnail/wx-img-1-e99378d457b9d77ca8e12f4e33db820e30357ae184aa3e24306eda1f9b268cff.png'/ >

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>

</head>
<body class="entry-container">
  <div class="entry-container-inner">
    


  


<form class="center with-shadow" data-form-token="bN77Hf" data-validate-url="/f/bN77Hf/validate_fields" id="new_entry" 
action="/f/bN77Hf" accept-charset="UTF-8" method="post">
<input type="hidden" name="utf8" value="&#x2713;" />

<input type="hidden" name="authenticity_token" value="YLaY5ZD1Cqmy/mfwqmrVqpSf0DzGXCIycqJQmkvOwlj/H6dNeyGRtKhaUFzB8ygupMgJP9hZyMH/2OXAJOr6cw==" />
      <div class="banner font-family-heiti">
          <div class="banner-text"> </div>
    </div>

  <div class="form-header container-fluid">
    <div class="row">
      <h1 class="form-title col-md-12 font-family-songti">
        跳哪里邀请您加入中国医疗人才库
      </h1>
      <div class="form-description col-md-12"><p>近来医疗行业并购重组活跃，人才流动频繁，我们正在进行人才动向调查并提供职位帮助。希望通过本次的问卷，让我们了解您目前的情况与需求，以更好的为你提供职业发展服务。</p></div>
    </div>
  </div>
  <div class="form-content container-fluid">
    <div class="row">
      
        <div class="fields clearfix">
              <div class="field field-check-box col-sm-12 required" data-api-code="field_12" data-type="CheckBox" data-label="您希望通过咨询顾问获得哪些帮助？" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_12">
            您希望通过咨询顾问获得哪些帮助？
</label>        
</div>
  <div class="field-content">

    <div class="choices font-family-inherit">
        <label onclick="" class="checkbox ">
          <input type="checkbox" value="fEcN" name="entry[field_12][]" />
          <div class="choice-description">
            职位推荐  
          </div>
        </label>
        <label onclick="" class="checkbox ">
          <input type="checkbox" value="Uyzl" name="entry[field_12][]" />
          <div class="choice-description">
            职涯规划  
          </div>
        </label>
        <label onclick="" class="checkbox ">
          <input type="checkbox" value="KX9n" name="entry[field_12][]" />
          <div class="choice-description">
            薪酬咨询  
          </div>
        </label>
        <label onclick="" class="checkbox ">
          <input type="checkbox" value="cBkS" name="entry[field_12][]" />
          <div class="choice-description">
            面试辅导  
          </div>
        </label>
        <div class="other-choice-area ">
          <label onclick="" class="checkbox  ">
            <input class="other_choice" data-field-key="field_12" type="checkbox" value="08E3" name="entry[field_12][]" />
            <div class="choice-description">
              其他  
            </div>
          </label>
            <span class="other-choice-input-container"><input class="other-choice-input gd-input-medium gd-input-thin fixed-width-control" data-field-key="field_12" type="text" value="" name="entry[field_12_other]" id="entry_field_12_other" /></span>
        </div>
</div>


  </div>
</div>


</div>              <div class="field field-text-field col-sm-12 required" data-api-code="field_1" data-type="TextField" data-label="您目前就职于哪家公司?" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_1">
            您目前就职于哪家公司?
</label>        
</div>
  <div class="field-content">

    <input type="text" name="entry[field_1]" id="entry_field_1" />

  </div>
</div>


</div>              <div class="field field-text-field col-sm-12 required" data-api-code="field_2" data-type="TextField" data-label="您目前的职位？（与名片上一致）" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_2">
            您目前的职位？（与名片上一致）
</label>        
</div>
  <div class="field-content">

    <input type="text" name="entry[field_2]" id="entry_field_2" />

  </div>
</div>


</div>              <div class="field field-text-field col-sm-12 required" data-api-code="field_3" data-type="TextField" data-label="您所在的地区是?" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_3">
            您所在的地区是?
</label>        
</div>
  <div class="field-content">

    <input type="text" name="entry[field_3]" id="entry_field_3" />

  </div>
</div>


</div>              <div class="field field-matrix-field col-sm-12 required" data-api-code="field_4" data-type="MatrixField" data-label="您目前的税前薪资情况？" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_4">
            您目前的税前薪资情况？
</label>        
</div>
  <div class="field-content">

    <div class="matrix"
     data-statement-api-codes="[&quot;zTUf&quot;]"
     data-dimension-api-codes="[&quot;5LIy&quot;, &quot;Y6i2&quot;, &quot;08yJ&quot;]"
>
  <table class="gd-table gd-table-bordered gd-table-hover">
    <thead class=" font-family-inherit">
      <tr>
        <th></th>
          <th>基本薪资</th>
          <th>奖金（2015年）</th>
          <th>年薪</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>薪资</td>
          <td>
            <input type="text" name="entry[field_4][zTUf][5LIy]" />
          </td>
          <td>
            <input type="text" name="entry[field_4][zTUf][Y6i2]" />
          </td>
          <td>
            <input type="text" name="entry[field_4][zTUf][08yJ]" />
          </td>
      </tr>
    </tbody>
  </table>
</div>

  </div>
</div>


</div>              <div class="field field-radio-button col-sm-12 required" data-api-code="field_17" data-type="RadioButton" data-label="您上次跳槽时的薪资涨幅" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_17">
            您上次跳槽时的薪资涨幅
</label>        
</div>
  <div class="field-content">

    <div class="choices font-family-inherit">
        <label onclick="" class="radio ">
          <input type="radio" value="3jz2" checked="checked" name="entry[field_17]" />
          <div class="choice-description">
            1%~10%
          </div>
        </label>
        <label onclick="" class="radio ">
          <input type="radio" value="aeOt" name="entry[field_17]" />
          <div class="choice-description">
            10%~20%
          </div>
        </label>
        <label onclick="" class="radio ">
          <input type="radio" value="1xEN" name="entry[field_17]" />
          <div class="choice-description">
            20%~30%
          </div>
        </label>
        <label onclick="" class="radio ">
          <input type="radio" value="9OTF" name="entry[field_17]" />
          <div class="choice-description">
            30%以上
          </div>
        </label>
</div>


  </div>
</div>


</div>              <div class="field field-radio-button col-sm-12 required" data-api-code="field_18" data-type="RadioButton" data-label="您期望的薪资涨幅" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_18">
            您期望的薪资涨幅
</label>        
</div>
  <div class="field-content">

    <div class="choices font-family-inherit">
        <label onclick="" class="radio ">
          <input type="radio" value="tfTJ" name="entry[field_18]" />
          <div class="choice-description">
            10%~15%
          </div>
        </label>
        <label onclick="" class="radio ">
          <input type="radio" value="sl2v" name="entry[field_18]" />
          <div class="choice-description">
            15%~20%
          </div>
        </label>
        <label onclick="" class="radio ">
          <input type="radio" value="R5cs" name="entry[field_18]" />
          <div class="choice-description">
            20%~25%
          </div>
        </label>
        <label onclick="" class="radio ">
          <input type="radio" value="cqWk" name="entry[field_18]" />
          <div class="choice-description">
            25%~30%
          </div>
        </label>
        <label onclick="" class="radio ">
          <input type="radio" value="npMH" name="entry[field_18]" />
          <div class="choice-description">
            30%以上
          </div>
        </label>
</div>


  </div>
</div>


</div>              <div class="field field-text-field col-sm-12 required" data-api-code="field_5" data-type="TextField" data-label="目前负责的产品/项目？" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_5">
            目前负责的产品/项目？
</label>        
</div>
  <div class="field-content">

    <input type="text" name="entry[field_5]" id="entry_field_5" />

  </div>
</div>


</div>              <div class="field field-text-field col-sm-12 required" data-api-code="field_13" data-type="TextField" data-label="您认为以您目前的行业背景，之后最适合去那家公司？" data-validations="[&quot;Presence&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_13">
            您认为以您目前的行业背景，之后最适合去那家公司？
</label>        
</div>
  <div class="field-content">

    <input type="text" name="entry[field_13]" id="entry_field_13" />

  </div>
</div>


</div>              <div class="field field-matrix-field col-sm-12" data-api-code="field_14" data-type="MatrixField" data-label="除此公司外，您认为其次适合去那些公司？（可选填多家公司）" data-validations="[]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_14">
            除此公司外，您认为其次适合去那些公司？（可选填多家公司）
</label>        
</div>
  <div class="field-content">

    <div class="matrix"
     data-statement-api-codes="[&quot;IM9V&quot;, &quot;E7X0&quot;, &quot;zCuc&quot;]"
     data-dimension-api-codes="[&quot;dHX1&quot;]"
>
  <table class="gd-table gd-table-bordered gd-table-hover">
    <thead class=" font-family-inherit">
      <tr>
        <th></th>
          <th>公司</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>一、</td>
          <td>
            <input type="text" name="entry[field_14][IM9V][dHX1]" />
          </td>
      </tr>
      <tr>
        <td>二、</td>
          <td>
            <input type="text" name="entry[field_14][E7X0][dHX1]" />
          </td>
      </tr>
      <tr>
        <td>其他</td>
          <td>
            <input type="text" name="entry[field_14][zCuc][dHX1]" />
          </td>
      </tr>
    </tbody>
  </table>
</div>

  </div>
</div>


</div>              <div class="field field-name-field col-sm-12" data-api-code="field_10" data-type="NameField" data-label="为了让我们的咨询顾问更方便地为您服务，请选填以下信息。姓名" data-validations="[]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_10">
            为了让我们的咨询顾问更方便地为您服务，请选填以下信息。姓名
</label>        
</div>
  <div class="field-content">

    <input class="input-with-icon" data-icon="gd-icon-name" type="text" value="" name="entry[field_10]" id="entry_field_10" />

  </div>
</div>


</div>              <div class="field field-mobile-field col-sm-12" data-api-code="field_15" data-type="MobileField" data-label="联系方式" data-validations="[&quot;Format&quot;]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_15">
            联系方式
</label>        
</div>
  <div class="field-content">

    <div data-role='verification_sender'>
  <input class="mobile-input input-with-icon" data-icon="gd-icon-mobile" type="tel" name="entry[field_15]" id="entry_field_15" />
    <input value="true" type="hidden" name="entry[field_15_skip_verification]" id="entry_field_15_skip_verification" />
</div>

  </div>
</div>


</div>              <div class="field field-text-field col-sm-12" data-api-code="field_16" data-type="TextField" data-label="感谢您的参与，您将得到我们下一季的薪资信息报告。你期望以何种您期望通过哪种方式得到反馈。如邮件，请附上您的邮箱地址。" data-validations="[]">
                

<div class="form-group" >
      <div class="field-label-container" onclick="">
        <label class="field-label font-family-inherit" for="entry_field_16">
            感谢您的参与，您将得到我们下一季的薪资信息报告。你期望以何种您期望通过哪种方式得到反馈。如邮件，请附上您的邮箱地址。
</label>        
</div>
  <div class="field-content">

    <input type="text" name="entry[field_16]" id="entry_field_16" />

  </div>
</div>


</div>        </div>
      


      <div class="field submit-field col-md-12 clearfix">

              <input type="submit" name="commit" value="提交" data-disable-with="提交中..." class="submit gd-btn gd-btn-primary-solid font-family-inherit with-shadow" />
       
      </div>
    </div>
  </div>
</form>
  

  </div>

  

</body>
</html>
