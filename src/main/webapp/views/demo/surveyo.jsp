<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!DOCTYPE html>
<html data-embedded="">
<head>
<meta charset="utf-8">
<title>跳哪里邀请您加入中国医疗人才库</title>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="description"
	content='为跳哪里邀请您加入中国医疗人才库提交数据。&amp;lt;p&amp;gt;近来医疗行业并购重组活跃，人才流动频繁，我们正在进行人才动向调查并提供职位帮助。希望通过...'>
<meta property="og:title" content='跳哪里邀请您加入中国医疗人才库'>
<meta property="og:description"
	content='为跳哪里邀请您加入中国医疗人才库提交数据。&amp;lt;p&amp;gt;近来医疗行业并购重组活跃，人才流动频繁，我们正在进行人才动向调查并提供职位帮助。希望通过...'>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed"
	href="https://cdn.jinshuju.net/favicon.png">

<link rel="shortcut icon" type="image/x-icon"
	href="https://cdn.jinshuju.net/assets/favicon-62fe2f27ea9d532a13fc76ed0e8b5e68bc2f61dde4a7935f54ff5dc3e3a727b2.ico" />
<link rel="stylesheet" media="screen"
	href="https://cdn.jinshuju.net/assets/published-1ea706899af85ede9b6a960e804eaa417b591d81b076479ca0c7cacad1b0b64e.css"
	debug="false" />

<script>
  (function() {
    var js;
    if (typeof JSON !== 'undefined' && 'querySelector' in document && 'addEventListener' in window) {
      js = 'https://cdn.jinshuju.net/assets/jquery/jquery-5c9323e78c3665389a20ff4638ac5b3f367c175c527045243e53a97cff9b81d8.js';
    } else {
      js = 'https://cdn.jinshuju.net/assets/jquery/jquery-1.11.3-4569ddbe1675e82642ee7b8cc084d90093a4fafb51635c382b4b64a0f4c77228.js';
    }
    document.write('<script src="' + js + '"><\/script>');
  }());
</script>

<script
	src="https://cdn.jinshuju.net/assets/published_forms/application-1ed94f0826244dee96a5108cd9efb1ea7d75c317fecf3ddd53ef27cce2fee806.js"
	debug="false"></script>

<meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token"
	content="e+hjFVIXIQT77mDIxSoSKIt2USKDjj0eTz0nJ+UOB5LkQVy9ucO6GeFKV2Sus++suyGIIZ2L1+3CR5J9iio/uQ==" />
</head>
<body class="entry-container mobile-device phone-device">
	<div class="entry-container-inner">



		<style type="text/css">
.entry-container {
	background-color: #ffffff;
	background-image:
		url(https://cdn.jinshuju.net/assets/wallpaper/textures/texture_wood-ea2c51f6d97bb14fc4af68317d3e0b59a91f4f724f0ee831360493f3ef07a20a.png?imageView2/2/w/1920);
	background-position: top center;
	background-attachment: fixed;
	background-repeat: repeat;
}

@media screen and (min-width: 769px) and (max-width: 1440px) {
	.entry-container {
		background-color: #ffffff;
		background-image:
			url(https://cdn.jinshuju.net/assets/wallpaper/textures/texture_wood-ea2c51f6d97bb14fc4af68317d3e0b59a91f4f724f0ee831360493f3ef07a20a.png?imageView2/2/w/1440);
		background-position: top center;
		background-attachment: fixed;
		background-repeat: repeat;
	}
}

@media screen and (min-width: 481px) and (max-width: 768px) {
	.entry-container {
		background-color: #ffffff;
		background-image:
			url(https://cdn.jinshuju.net/assets/wallpaper/textures/texture_wood-ea2c51f6d97bb14fc4af68317d3e0b59a91f4f724f0ee831360493f3ef07a20a.png?imageView2/2/w/768);
		background-position: top center;
		background-attachment: fixed;
		background-repeat: repeat;
	}
}

@media screen and (max-width: 480px) {
	.entry-container {
		background-color: #ffffff;
		background-image:
			url(https://cdn.jinshuju.net/assets/wallpaper/textures/texture_wood-ea2c51f6d97bb14fc4af68317d3e0b59a91f4f724f0ee831360493f3ef07a20a.png?imageView2/2/w/480);
		background-position: top center;
		background-attachment: fixed;
		background-repeat: repeat;
	}
}
</style>

		<style type="text/css">
.entry-container>.entry-container-inner .center {
	max-width: 700px;
}

.entry-container>.entry-container-inner>form {
	background-color: rgba(255, 255, 255, 0.98);
	max-width: 700px;
	border-width: 0;
	border-color: rgb(211, 163, 77);
}

.entry-container>.entry-container-inner>form>.banner {
	background-color: #2CC0C5;
	font-size: 1.167em;
	font-weight: normal;
	color: #FFF;
	text-align: left;
}

.entry-container>.entry-container-inner>form>.form-header .form-title {
	font-size: 1.333em;
	font-weight: bold;
	color: rgb(0, 0, 0);
	text-align: left;
}

.entry-container>.entry-container-inner>form .fields .field {
	padding-top: 15px;
	padding-bottom: 15px;
}

.entry-container>.entry-container-inner>form .fields .field .field-label
	{
	font-size: 1.000em;
	font-weight: normal;
	color: rgb(0, 0, 0);
}

.entry-container>.entry-container-inner>form .fields .field .choices label,
	.entry-container>.entry-container-inner>form .fields .field .likert thead th,
	.entry-container>.entry-container-inner>form .fields .field .matrix thead th,
	.entry-container>.entry-container-inner>form .fields .field .goods-items .name
	{
	font-size: 1.000em;
	font-weight: normal;
	color: rgb(0, 0, 0);
}

.entry-container>.entry-container-inner>form .submit-field {
	text-align: left;
}

.entry-container>.entry-container-inner>form .submit-field .gd-btn {
	background-color: #2CC0C5;
	padding: 0 30px;
	font-size: 1.167em;
	font-weight: normal;
	color: #FFF;
	border-width: 1px;
	border-color: transparent;
}
</style>


		<form id="new_entry" class="center with-shadow">


			<div class="banner font-family-heiti">
				<div class="banner-text"></div>
			</div>

			<div class="form-header container-fluid">
				<div class="row">
					<h1 class="form-title col-md-12 font-family-songti">
						跳哪里邀请您加入中国医疗人才库</h1>
					<div class="form-description col-md-12">
						<p>近来医疗行业并购重组活跃，人才流动频繁，我们正在进行人才动向调查并提供职位帮助。希望通过本次的问卷，让我们了解您目前的情况与需求，以更好的为你提供职业发展服务。</p>
					</div>
				</div>
			</div>
			<div class="form-content container-fluid">
				<div class="row">

					<div class="fields clearfix">
						<div class="field field-check-box col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_12"> 您希望通过咨询顾问获得哪些帮助？ </label>
								</div>
								<div class="field-content">

									<div class="choices font-family-inherit">
										<label onclick="" class="checkbox "> <input
											type="checkbox" value="recommend" name="service"
											id="recommend" />
											<div class="choice-description">职位推荐</div>
										</label> <label onclick="" class="checkbox "> <input
											type="checkbox" value="career" name="service" id="career" />
											<div class="choice-description">职涯规划</div>
										</label> <label onclick="" class="checkbox "> <input
											type="checkbox" value="salary" name="service" id="salary" />
											<div class="choice-description">薪酬咨询</div>
										</label> <label onclick="" class="checkbox "> <input
											type="checkbox" value="interview" name="service"
											id="interview" />
											<div class="choice-description">面试辅导</div>
										</label>
										<div class="other-choice-area ">
											<label onclick="" class="checkbox  "> <input
												class="other_choice" data-field-key="field_12"
												type="checkbox" value="other" name="service" />
												<div class="choice-description">其他</div>
											</label> <span class="other-choice-input-container"> <input
												class="other-choice-input gd-input-medium gd-input-thin fixed-width-control"
												data-field-key="field_12" type="text" value=""
												name="otherservice" id="otherservices" /></span>
										</div>
									</div>


								</div>
							</div>


						</div>
						<div class="field field-text-field col-sm-12 required">

							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_1"> 您目前就职于哪家公司? </label>
								</div>
								<div class="field-content">

									<input type="text" name="companyName" id="companyName" />

								</div>
							</div>


						</div>
						<div class="field field-text-field col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_2"> 您目前的职位？（与名片上一致） </label>
								</div>
								<div class="field-content">

									<input type="text" name="roleName" id="roleName" />

								</div>
							</div>


						</div>
						<div class="field field-text-field col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_3"> 您所在的地区是? </label>
								</div>
								<div class="field-content">

									<input type="text" name="location" id="location" />

								</div>
							</div>


						</div>
						<div class="field field-matrix-field col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_4"> 您目前的税前薪资情况？ </label>
								</div>
								<div class="field-content">

									<div class="matrix"
										data-statement-api-codes="[&quot;zTUf&quot;]"
										data-dimension-api-codes="[&quot;5LIy&quot;, &quot;Y6i2&quot;, &quot;08yJ&quot;]">
										<table class="gd-table gd-table-bordered gd-table-hover">
											<thead class=" font-family-inherit">
												<tr>
													<th></th>
													<th>基本薪资</th>
													<th>奖金（2015年）</th>
													<th>年薪</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>薪资</td>
													<td><input type="text" name="baseSlry" /></td>
													<td><input type="text" name="award" /></td>
													<td><input type="text" name="totalSlry" /></td>
												</tr>
											</tbody>
										</table>
									</div>

								</div>
							</div>


						</div>
						<div class="field field-radio-button col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_17"> 您上次跳槽时的薪资涨幅 </label>
								</div>
								<div class="field-content">

									<div class="choices font-family-inherit">
										<label onclick="" class="radio "> <input type="radio"
											value="3jz2" checked="checked" name="lastPercent" />
											<div class="choice-description">1%~10%</div>
										</label> <label onclick="" class="radio "> <input type="radio"
											value="aeOt" name="lastPercent" />
											<div class="choice-description">10%~20%</div>
										</label> <label onclick="" class="radio "> <input type="radio"
											value="1xEN" name="lastPercent" />
											<div class="choice-description">20%~30%</div>
										</label> <label onclick="" class="radio "> <input type="radio"
											value="9OTF" name="lastPercent" />
											<div class="choice-description">30%以上</div>
										</label>
									</div>


								</div>
							</div>


						</div>
						<div class="field field-radio-button col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_18"> 您期望的薪资涨幅 </label>
								</div>
								<div class="field-content">

									<div class="choices font-family-inherit">
										<label onclick="" class="radio "> <input type="radio"
											value="tfTJ" checked="checked" name="expectPercent" />
											<div class="choice-description">10%~15%</div>
										</label> <label onclick="" class="radio "> <input type="radio"
											value="sl2v" name="expectPercent" />
											<div class="choice-description">15%~20%</div>
										</label> <label onclick="" class="radio "> <input type="radio"
											value="R5cs" name="expectPercent" />
											<div class="choice-description">20%~25%</div>
										</label> <label onclick="" class="radio "> <input type="radio"
											value="cqWk" name="expectPercent" />
											<div class="choice-description">25%~30%</div>
										</label> <label onclick="" class="radio "> <input type="radio"
											value="npMH" name="expectPercent" />
											<div class="choice-description">30%以上</div>
										</label>
									</div>


								</div>
							</div>


						</div>
						<div class="field field-text-field col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_5"> 目前负责的产品/项目？ </label>
								</div>
								<div class="field-content">

									<input type="text" name="projectName" id="projectName" />

								</div>
							</div>


						</div>
						<div class="field field-text-field col-sm-12 required">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_13"> 您认为以您目前的行业背景，之后最适合去那家公司？ </label>
								</div>
								<div class="field-content">

									<input type="text" name="expectCompany" id="expectCompany" />

								</div>
							</div>


						</div>
						<div class="field field-matrix-field col-sm-12">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_14"> 除此公司外，您认为其次适合去那些公司？（可选填多家公司） </label>
								</div>
								<div class="field-content">

									<div class="matrix"
										data-statement-api-codes="[&quot;IM9V&quot;, &quot;E7X0&quot;, &quot;zCuc&quot;]"
										data-dimension-api-codes="[&quot;dHX1&quot;]">
										<table class="gd-table gd-table-bordered gd-table-hover">
											<thead class=" font-family-inherit">
												<tr>
													<th></th>
													<th>公司</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>一、</td>
													<td><input type="text" name="expectCompany1" /></td>
												</tr>
												<tr>
													<td>二、</td>
													<td><input type="text" name="expectCompany2" /></td>
												</tr>
												<tr>
													<td>其他</td>
													<td><input type="text" name="expectCompany3" /></td>
												</tr>
											</tbody>
										</table>
									</div>

								</div>
							</div>


						</div>
						<div class="field field-name-field col-sm-12">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_10"> 为了让我们的咨询顾问更方便地为您服务，请选填以下信息。姓名 </label>
								</div>
								<div class="field-content">

									<input class="input-with-icon" data-icon="gd-icon-name"
										type="text" value="" name="name" id="name" />

								</div>
							</div>


						</div>
						<div class="field field-mobile-field col-sm-12">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_15"> 联系方式 </label>
								</div>
								<div class="field-content">

									<div data-role='verification_sender'>
										<input class="mobile-input input-with-icon"
											data-icon="gd-icon-mobile" type="tel" name="phone" id="phone" />

									</div>

								</div>
							</div>


						</div>
						<div class="field field-text-field col-sm-12">


							<div class="form-group">
								<div class="field-label-container" onclick="">
									<label class="field-label font-family-inherit"
										for="entry_field_16">
										感谢您的参与，您将得到我们下一季的薪资信息报告。你期望以何种您期望通过哪种方式得到反馈。如邮件，请附上您的邮箱地址。 </label>
								</div>
								<div class="field-content">

									<input type="text" name="email" id="email" />

								</div>
							</div>


						</div>
					</div>

					<div class="field submit-field col-md-12 clearfix">


						<div  id="commit" name="commit" 
							onclick="submitForm()" 
							class="gd-btn" >
							提交
						</div>
					</div>
				</div>
			</div>
		</form>
		<script>
      //<![CDATA[
      function serializeObject(form) {
        var o = {};
        $.each(form.serializeArray(), function(index) {
          if (o[this['name']]) {
            o[this['name']] = o[this['name']] + "," + this['value'];
          } else {
            o[this['name']] = this['value'];
          }
        })
        return o;
      }
      function submitForm() {

        var param = serializeObject($("#new_entry"));

        $.ajax({
          url : "${ctx}/demo/survey/add",
          type : "POST",
          data : param,
          success : function(data, status) {
            if (data.errorcode > 0) {
              location.href = '${ctx}/demo/survey/view?user=' + data.msg;
            } else {
              alert("提交失败"+ data.msg);

            }
          },
          error : function() {
            alert("服务出错，请稍后尝试");

          }
        });

      }

      //]]>
    </script>
		

		<div id="form_page_error_messages_modal"
			class="modal warning form-error-messages-modal  mobile-dialog"
			tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
			data-keyboard="false">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span>&times;</span>
						</button>
						<h4 class="modal-title">错误提醒</h4>
					</div>
					<div class="modal-body clearfix">
						<div class="error-explanation">
							<h5>提交未成功，当前页面填写有错误！</h5>
						</div>
					</div>
				</div>
			</div>
		</div>


		<footer class='published text-center'>
			<p>
				<span>Powered by </span> <span> MedTrend</span>
			</p>
		</footer>

	</div>


	<div id="form_thumbnail_default" class="gd-hide">
		<img
			src="https://cdn.jinshuju.net/assets/weixin_thumbnail/wx-img-1-e99378d457b9d77ca8e12f4e33db820e30357ae184aa3e24306eda1f9b268cff.png"
			alt="Wx img 1" />
	</div>
	<div class="gd-hide" id="form_share_info"
		data-form-name="跳哪里邀请您加入中国医疗人才库"
		data-form-url="https://jinshuju.net/f/bN77Hf" data-hide-logo=false>
		近来医疗行业并购重组活跃，人才流动频繁，我们正在进行人才动向调查并提供职位帮助。希望通过本次的问卷，让我们了解您目前的情况与需求，以更好的为你提供职业发展服务。
	</div>




</body>
</html>
