<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
    <title>职位信息</title>  
</head>
<body>
<div class="ui segment" id="n-page">
    &nbsp;<br>
    &nbsp;<br>
    <h3 class="ui attached header" id="list-title">
        职位信息
    </h3>
    &nbsp;<br>
    &nbsp;<br>
    职位：${job.name }&nbsp;<br>&nbsp;<br>
    职位编码：${job.jobCode }&nbsp;<br>&nbsp;<br>
    公司：${job.companyName }&nbsp;<br>&nbsp;<br>
    工作地点：${job.city.name }&nbsp;<br>&nbsp;<br>
    负责区域：${job.location}<br>&nbsp;<br>
    下属人数：${job.employeeNum}<br>&nbsp;<br> 
    主要职责：${job.majorDuty}<br>&nbsp;<br>
    职位描述：${job.description}<br>&nbsp;<br>
    有效期：${job.expiredDateStr }&nbsp;<br>&nbsp;<br>
    推荐奖金：${job.reward}元&nbsp;<br>&nbsp;<br>
    期望公司来源1：
<c:forEach items="${companies}" var="company">  
    <c:choose>
        <c:when test="${job.expectcompanyid == company.id }">
            ${company.name}
        </c:when> 
    </c:choose>  
</c:forEach>
<br>&nbsp;<br>
期望公司来源2：
<c:forEach items="${companies}" var="company">  
    <c:choose>
        <c:when test="${job.expectcompanyid2 == company.id }">
            ${company.name}
        </c:when> 
    </c:choose>  
</c:forEach>
<br>&nbsp;<br>
期望公司来源3：
<c:forEach items="${companies}" var="company">  
    <c:choose>
        <c:when test="${job.expectcompanyid3 == company.id }">
            ${company.name}
        </c:when> 
    </c:choose>  
</c:forEach>
<br>&nbsp;<br>

    <input type="hidden" name="id" id="id" value="${job.id}" />

    <div class="ui segment" id="btn-next2">
        <div class="fluid ui  inverted blue button" id="liucheng">了解“推荐流程”</div>

    </div>
    


</div>



<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">


    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

           // if($(this).attr('id') == 'next') {
           //      var jobId = $("#id").val();
            //     location.href =  '${ctx}/wechat/candidate/recommend/toRecommend.do?jobId='+jobId;
              
            //}
            if($(this).attr('id') == 'assistant') {
                 var jobId = $("#id").val();
                 location.href =  '${ctx}/wechat/candidate/assistant/toRecommend.do?jobId='+jobId;
              
            }
            if($(this).attr('id') == 'liucheng') {
                location.href = '${ctx}/wechat/candidate/recommend/help.page';
            }
        } 
    };

    $(document).ready(function() { 
       
            $('.button').on('click', handler.activate);
           
           

    });

</script>
</body>
</html>