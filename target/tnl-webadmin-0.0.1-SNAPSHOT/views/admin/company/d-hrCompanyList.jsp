<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<%@ taglib uri="http://www.tnl.com/system" prefix="system"%> 
<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" />
<script type="text/javascript" src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js"></script>
    <script type="text/javascript" src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js"></script>

</head>

<body>
	
	<div >
		<fieldset >
			<div 
				style="min-width: 820px; margin: 0 0px 0 0;">
				
				<div >
					<a href="${ctx}/admin/company/toEdit.page"  id="createCompany"> Create</a>
					<a href="#" id="editCompany"> Edit</a>
					<a href="#" id="removeCompany"> Remove </a>
				</div>
			</div>
				<div style="height: 35px;">
				
			</div>
				<div class="ct_table_border">
					<table id='compList'></table>
					<div id="pager"></div>
				</div>
			</fieldset>
		</div>
	
	
<script type="text/javascript">
$(document).ready(function() { 
			initialListGrid();
			jQuery("#editCompany").click(function(){
				editCompany(null);
			})
			jQuery("#removeCompany").click(function(){
				deleteCompany(null);
			})
		});
		
		//jqgrid表格随浏览器的变化而变化
		window.onload = window.onresize = function() {
			var screenAvailwidth = document.documentElement.clientWidth;
			var defaultGridWidth = screenAvailwidth - 46;
			$("#compList").setGridWidth(defaultGridWidth);
		};
		
		
		function initialListGrid() {
			jQuery("#compList").jqGrid({
				url : '${ctx}/admin/hrcompany/getAllCompany.do',
				datatype : "json",
				colNames : [ 'id',
						'公司名称',
						'联系人',
						'电话号码',
						'地址',
						'认证状态',
						'Email',
						'组织代码',
						'机构代码'],
				colModel : [ {
					name : 'id',
					index : 'id',
					hidden : true,
					align : "left"
				}, {
					name : 'name',
					index : 'name',
					align : "left"
				}, {
					name : 'contact',
					index : 'contact',
					align : "left"
				},{
					name : "phone",
					index : "phone"
				},{
					name : 'address',
					index : 'address',
					align : "left"
				}, {
					name : 'approved',
					index : 'approved',
					align : "left"
				},{
					name : 'email',
					index : 'email',
					align : "left"
				},{
					name : 'orgnizationCode',
					index : 'orgnizationCode',
					align : "left"
				},{
					name : 'licenseCode',
					index : 'licenseCode',
					align : "left"
				}],
				jsonReader : {
					root : "data",
					repeatitems : false
				},
				rowNum : 10,
				height : 300,
				scrollOffset:0,
				pager : '#pager',
				sortname : 'id',
				viewrecords : true,
				multiselect : false,
				sortorder : "ASC",


			});

			jQuery("#compList").jqGrid('navGrid', '#pager', {
				edit : false,
				add : false,
				del : false
			});
			

		}
		
		function editCompany(cl) {
			 var grid = $("#compList");
			var id = grid.jqGrid("getGridParam", "selrow");
				if (!id)
				{
					alert("没有选中");
					return;
				}
			
       	 var rowdata = jQuery("#compList").jqGrid('getRowData', id);

			var empId = rowdata['id'];
			var url ="${ctx}/admin/hrcompany/toEdit.page?id=" + empId;
			 location.href = url;
		}
		
		function deleteCompany(cl) {
			 var grid = $("#compList");
			var id = grid.jqGrid("getGridParam", "selrow");
				if (!id)
				{
					alert("没有选中");
					return;
				}
			
      	 var rowdata = jQuery("#compList").jqGrid('getRowData', id);

			var empId = rowdata['id'];
			var datas={};
			datas['id'] = empId;
			
			//TODO 弹出确认删除吗对话框
			$.ajax({
				url : "${ctx}/admin/hrcompany/delete.do",
				type : 'POST',
				async : false,
				data : datas,
				dataType : 'json',
				success : function(data) {
					
					alert("删除成功");
				},
				error : function() {
					 alert("服务出错，请稍后尝试"); 
				}
			});
		}
	</script>
</body>
</html>