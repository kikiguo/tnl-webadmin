<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
    <title>猎头公司信息</title>  
</head>
<body>
<div class="ui segment" id="n-page">

        <div class="ui segment" id="n-content">
            <form class="ui form">
                <div class="field">
                    <label>公司名称</label>
                    <select class="ui search dropdown" name="companyid" id="companyid">
                    <c:choose>
                        <c:when test="${(company.id == 0) and (company.name != '')}">
                            <option value="">${company.name}</option>
                        </c:when> 
                        <c:otherwise>
                            <option value="">请输入公司名称</option>
                        </c:otherwise>
                    </c:choose>
                    <c:forEach items="${companies}" var="comp">
                        <c:choose>
                            <c:when test="${comp.id == company.id }">
                                <option value="${comp.id}" selected="selected">${comp.name}</option>
                            </c:when> 
                            <c:otherwise> 
                                <option value="${comp.id}">${comp.name}</option>
                            </c:otherwise> 
                        </c:choose>  
                    </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <label>联系人</label>
                    <div class="ui input">
                        <input type="text" name="companycontact" id="companycontact" value="${company.contact}" placeholder="请输入公司联系人">
                    </div>
                </div>
                <div class="field">
                    <label>联系电话</label>
                    <div class="ui input">
                        <input type="text" name="companyphone" id="companyphone" value="${company.phone}" placeholder="请输入公司联系电话">
                    </div>
                </div>
                <div class="field">
                    <label>公司地址</label>
                    <div class="ui input">
                        <input type="text" name="companyaddress" id="companyaddress" value="${company.address}" placeholder="请输入公司地址">
                    </div>
                </div>
                <div class="field">
                    <label>组织机构代码</label>
                    <div class="ui input">
                        <input type="text" name="orgcode" id="orgcode" value="${company.orgnizationCode}" placeholder="请输入组织机构代码">
                    </div>
                </div>
                <div class="field">
            <label>您可以在这里上传公司营业执照</label>
            <div class="ui input">
                <input type="file" name="bizcodephoto" id="bizcodephoto" placeholder="请选择要上传的公司营业执照">
            </div>
        </div>                  
                <input type="hidden" name="hunterid" id="hunterid" value="${hunter.id}"/>
            </form>        
        </div>
        <div class="ui segment" id="btn-next2">
            <div class="fluid ui button" id="next">下一步</div>
        </div>

</div>


<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this)
                .addClass('active') 
                .siblings() 
                .removeClass('active'); 

            if($(this).attr('id') == 'next') {

                var companyid = $("#companyid").val();
                var hunterid  = $("#hunterid").val();
                var companycontact = $("#companycontact").val();
                var companyphone   = $("#companyphone").val();
                var companyname    = "";
                var companyaddress = $("#companyaddress").val();
                var orgcode  = $("#orgcode").val();
                var bizcode  = $("#bizcode").val();
                var companyname2 = "${hunter.companyName}";

                if(companyid == "") {
                    //alert($("#companyId").dropdown('get search')[0]);
                    if(!$("#companyid").dropdown('get search')[0] || $("#companyid").dropdown('get search')[0]=="" ) {
                        if(companyname2!=$("#companyid").dropdown('get text')[0]) {
                            alert("您必须输入公司名称");
                            return false;
                        } 
                        companyname = companyname2;
                        companyid = 0;
                        
                    } else {
                        companyname = $("#companyid").dropdown('get search')[0];
                        companyid   = 0;
                    }
                }

                if(!companycontact) {
                    alert("您必须输入公司联系人");
                    return false;
                }
                if(!companyphone) {
                    alert("您必须输入公司电话");
                    return false;
                }
                if(!companyaddress) {
                    alert("您必须输入公司地址");
                    return false;
                }
                if(!orgcode) {
                    alert("您必须输入公司组织代码");
                    return false;
                }
                if(!bizcode ) {
                    alert("您必须输入营业执照编码");
                    return false;
                }

                var postUrl = '${ctx}/admin/hunterCompany/create.do';
                var param = {};
                param['companycontact']  = companycontact;
                param['companyid']   = companyid;
                param['companyname'] = companyname;
                param['companyphone'] = companyphone;
                param['companyaddress'] = companyaddress;
                param['hunterid']     = hunterid;
                param['orgcode']     = orgcode;
                param['bizcode']     = bizcode;

                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data: param,
                    success: function(response){ 
                    	if(response.errorcode=="200"){
                            location.href = '${ctx}/admin/hunterCompany/manage.page?hunterId='+hunterid+"&companyId="+companyid;
                        }else{
                        	alert(response.msg); 
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                
            }
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
			fullTextSearch:true,
		});
    });

</script>
</body>
</html>