<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" />
<script type="text/javascript" src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js"></script>
    <script type="text/javascript" src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js"></script>

</head>

<body>
	
	<div >
		<fieldset >
			<div 
				style="min-width: 820px; margin: 0 0px 0 0;">
				
				<div >
					<a href="${ctx}/admin/employee/toEdit.page"  id="createEmployee"> Create</a>
					<a href="#" id="editEmployee"> Edit</a>
					<a href="#" id="removeEmployee"> Remove </a>
				</div>
			</div>
				<div style="height: 35px;">
				
			</div>
				<div class="ct_table_border">
					<table id='empList'></table>
					<div id="pager"></div>
				</div>
			</fieldset>
		</div>
	
	
<script type="text/javascript">
$(document).ready(function() { 
			initialListGrid();
			jQuery("#editEmployee").click(function(){
				editEmployee(null);
			})
			jQuery("#removeEmployee").click(function(){
				deleteEmployee(null);
			})
		});
		//jqgrid表格随浏览器的变化而变化
		window.onload = window.onresize = function() {
			var screenAvailwidth = document.documentElement.clientWidth;
			var defaultGridWidth = screenAvailwidth - 46;
			$("#empList").setGridWidth(defaultGridWidth);
		};
		//初始化用户列表信息
		function initialListGrid() {
			jQuery("#empList").jqGrid({
				url : '${ctx}/admin/employee/getAllEmployee.do',
				datatype : "json",
				colNames : [ 'id',
						'姓名',
						'年龄',
						'现任公司',
						'职位',
						'工作年限',
						'工作状态',
						'城市',
						'联系方式'],
				colModel : [ {
					name : 'id',
					index : 'id',
					hidden : true,
					align : "left"
				}, {
					name : 'name',
					index : 'name',
					align : "left"
				}, {
					name : 'age',
					index : 'age',
					align : "left"
				},{
					name : "companyName",
					index : "companyName"
				},{
					name : 'roleId',
					index : 'roleId',
					align : "left"
				}, {
					name : 'workage',
					index : 'workage',
					align : "left"
				},{
					name : 'inservice',
					index : 'inservice',
					align : "left"
				},{
					name : 'cityId',
					index : 'cityId',
					align : "left"
				},{
					name : 'mobile',
					index : 'mobile',
					align : "left"
				}],
				jsonReader : {
					root : "data",
					repeatitems : false
				},
				rowNum : 10,
				height : 300,
				scrollOffset:0,
				pager : '#pager',
				sortname : 'id',
				viewrecords : true,
				multiselect : false,
				sortorder : "ASC",

				gridComplete : userListGridComplete

			});

			jQuery("#empList").jqGrid('navGrid', '#pager', {
				edit : false,
				add : false,
				del : false
			});
			
			$("#empList").jqGrid('filterToolbar',
                    {stringResult:true,searchOnEnter:true,defaultSearch:"cn"});

		}


		//userListGridComplete
		function userListGridComplete() {
			//$('#name').combobox('reload', '${ctx}/busisys/getAllBusiSysNames.do')
		var ids = jQuery("#empList").jqGrid('getDataIDs');

		/*for ( var i = 0; i < ids.length; i++) {
				var cl = ids[i];
				var rowdata = jQuery("#empList").jqGrid('getRowData', cl);
				var id = rowdata["id"];
				var status = rowdata["states"];
				var canClose = true;

				var _edit = "<input style='font-size: 12px;' type='button' value=' "Edit")}' "
						+ (canClose == false ? " disabled='disabled' " : '')
						+ " onclick=\"javascript:editSys('" + id + "');\"/>";

				var _del = "<input style='font-size: 12px;' type='button' value=' "Delete")}' "
						+ (canClose == false ? " disabled='disabled' " : '')
						+ " onclick=\"javascript:delSys('" + id + "');\"/>";

				
				jQuery("#empList").jqGrid('setRowData', cl, {
					act : _edit + _del
				});
			}*/
		}
	

	
		
		function editEmployee(cl) {
			 var grid = $("#empList");
			var id = grid.jqGrid("getGridParam", "selrow");
				if (!id)
				{
					alert("没有选中");
					return;
				}
			
       	 var rowdata = jQuery("#empList").jqGrid('getRowData', id);

			var empId = rowdata['id'];
			var url ="${ctx}/admin/employee/toEdit.page?id=" + empId;
			 location.href = url;
		}
		
		function deleteEmployee(cl) {
			 var grid = $("#empList");
			var id = grid.jqGrid("getGridParam", "selrow");
				if (!id)
				{
					alert("没有选中");
					return;
				}
			
      	 var rowdata = jQuery("#empList").jqGrid('getRowData', id);

			var empId = rowdata['id'];
			//var url ="${ctx}/admin/employee/delete.do?id=" + empId;
			var datas={};
			datas['id'] = empId;
			
			//TODO 弹出确认删除吗对话框
			$.ajax({
				url : "${ctx}/admin/employee/delete.do",
				type : 'POST',
				async : false,
				data : datas,
				dataType : 'json',
				success : function(data) {
					
					alert("删除成功");
				},
				error : function() {
					 alert("服务出错，请稍后尝试"); 
				}
			});
		}
	</script>
</body>
</html>