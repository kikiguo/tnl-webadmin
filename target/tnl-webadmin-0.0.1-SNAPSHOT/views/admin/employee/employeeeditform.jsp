<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>求职人编辑</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
      求职人编辑
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    返回求职人管理
    </button>
    <br><br>
    <div class="ui segment" id="n-content">
        <form class="ui form">
                <div class="field">
                    <label>求职人所在的行业</label>

                    <div class="ui center aligned segment">
                        <div class="ui buttons">
                            <c:choose>
                                <c:when test="${employee.domainId == 1 }">
                                    <div class="ui button" id="drug">医药</div>
                                    <div class="or"></div>
                                    <div class="ui button active" id="medical">医疗</div>
                                </c:when>
                                <c:when test="${employee.domainId == 2 }">
                                    <div class="ui button active" id="drug">医药</div>
                                    <div class="or"></div>
                                    <div class="ui button" id="medical">医疗</div>
                                </c:when>
                                <c:otherwise>
                                    <div class="ui button" id="drug">医药</div>
                                    <div class="or"></div>
                                    <div class="ui button" id="medical">医疗</div>
                                </c:otherwise>
                            </c:choose>

                        </div>
                    </div>
                </div>
                <input type="hidden" name="empId" id="empId" value="${employee.empid}" />

                <div class="field">
                    <label>姓名</label>
                    <div class="ui input">
                        <input type="text" id="name" name="name" value="${employee.name}"
                            placeholder="候选人的姓名">
                    </div>
                </div>
                <div class="field">
                    <label>年龄</label>
                    <div class="ui input">
                        <input type="text" id="age" name="age" value="${employee.age}"
                            placeholder="候选人的年龄">
                    </div>
                </div>
                <div class="field">
                    <label>工作年限</label>
                    <div class="ui input">
                        <input id="workAge" type="text" name="duration"
                            value="${employee.workage}" placeholder="现任工作年限">
                    </div>
                </div>
                <div class="field">
                    <label>联系方式</label>
                    <div class="ui input">
                        <input type="text" id="phone" name="phone"
                            value="${employee.phone}" placeholder="候选人的手机号码">
                    </div>
                </div>
                <div class="field">
                    <label>现任公司</label> 
                    <select name="companyId" id="companyId" class="ui search dropdown">
                        <c:choose>
                            <c:when
                                test="${(employee.companyId == 0) and (employee.companyName != '')}">
                                <option value="">${employee.companyName}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="">现任公司名称</option>
                            </c:otherwise>
                        </c:choose>
                        <c:forEach items="${companies}" var="company">
                            <c:choose>
                                <c:when test="${employee.companyId == company.id }">
                                    <option value="${company.id}" selected="selected">${company.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${company.id}">${company.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <label>现任职位</label> <select name="roleId" id="roleId"
                        class="ui search dropdown">
                        <option value="">现任职位名称</option>
                        <c:forEach items="${roles}" var="role">
                            <c:choose>
                                <c:when test="${employee.roleId == role.id }">
                                    <option value="${role.id}" selected="selected">${role.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${role.id}">${role.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <label>所在城市</label> <select name="cityId" id="cityId"
                        class="ui search dropdown">
                        <option value="">所在城市名称</option>
                        <c:forEach items="${cities}" var="city">
                            <c:choose>
                                <c:when test="${employee.cityId == city.id }">
                                    <option value="${city.id}" selected="selected">${city.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${city.id}">${city.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <label>管理区域</label>
                    <div class="ui input">
                        <input type="text" id="bestdomain" name="bestdomain"
                            value="${employee.bestdomain}" placeholder="请输入管理区域名称">
                    </div>
                </div>

                <div class="field">
                    <label>期望加入公司(至少填入一个公司名)</label> <select name="expectCompany1"
                        id="expectCompany1" class="ui search dropdown">
                        <option value="">第一期望加入公司</option>
                        <c:forEach items="${companies}" var="company">
                            <c:choose>
                                <c:when test="${employee.expectCompany1 == company.id }">
                                    <option value="${company.id}" selected="selected">${company.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${company.id}">${company.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <select name="expectCompany2" id="expectCompany2"
                        class="ui search dropdown">
                        <option value="">第二期望加入公司</option>
                        <c:forEach items="${companies}" var="company">
                            <c:choose>
                                <c:when test="${employee.expectCompany2 == company.id }">
                                    <option value="${company.id}" selected="selected">${company.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${company.id}">${company.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <select name="expectCompany3" id="expectCompany3"
                        class="ui search dropdown">
                        <option value="">第三期望加入公司</option>
                        <c:forEach items="${companies}" var="company">
                            <c:choose>
                                <c:when test="${employee.expectCompany3 == company.id }">
                                    <option value="${company.id}" selected="selected">${company.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${company.id}">${company.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <label>期望担任职位</label> <select name="expectJob" id="expectJob"
                        class="ui search dropdown">
                        <option value="">期望担任职位名称</option>
                        <c:forEach items="${roles}" var="role">
                            <c:choose>
                                <c:when test="${employee.expectJob == role.id }">
                                    <option value="${role.id}" selected="selected">${role.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${role.id}">${role.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <label>何时联系候选人？</label> <select id="intentTime" name="intentTime"
                        class="ui selection dropdown">
                        <c:choose>
                            <c:when test="${employee.intentTime == 0 }">
                                <option value="0" selected="selected">现在</option>
                                <option value="1">一月后</option>
                                <option value="6">半年后</option>
                                <option value="12">一年后</option>
                            </c:when>
                            <c:when test="${employee.intentTime == 1 }">
                                <option value="0">现在</option>
                                <option value="1" selected="selected">一月后</option>
                                <option value="6">半年后</option>
                                <option value="12">一年后</option>
                            </c:when>
                            <c:when test="${employee.intentTime == 6 }">
                                <option value="0">现在</option>
                                <option value="1">一月后</option>
                                <option value="6" selected="selected">半年后</option>
                                <option value="12">一年后</option>
                            </c:when>
                            <c:otherwise>
                                <option value="0">现在</option>
                                <option value="1">一月后</option>
                                <option value="6">半年后</option>
                                <option value="12" selected="selected">一年后</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>

                <div class="field">
                    <label>工作状态</label> <select id="inService"
                        class="ui selection dropdown">
                        <option value="0">离职</option>
                        <option value="1">在职</option>
                    </select>
                </div>
        </form>

        <div class="ui segment" id="cont-next">
            <div class="ui primary button" id="next">提交</div>
        </div>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'next') {
                var expectCompany1 = $("#expectCompany1").val();
                var expectCompany2 = $("#expectCompany2").val();
                var expectCompany3 = $("#expectCompany3").val();
                var expectJob = $("#expectJob").val();
                var intentTime = $("#intentTime").val();
                var phone = $("#phone").val();
                var companyId = $("#companyId").val();
                var roleId = $("#roleId").val();
                var name = $("#name").val();
                var age = $("#age").val();
                var workAge = $("#workAge").val();
                var inService = $("#inService").val();
                var empId = $("#empId").val();
                var domainId = 2;
                var cityid = $("#cityId").val();
                if($('#medical').hasClass('active')) {
                    domainId =1;
                }
                var bestdomain  = $("#bestdomain").val();
                
                if(!expectCompany1) {
                    alert("您必须至少输入第一个公司名称");
                    return false;
                }
                if(!expectCompany2) {
                    expectCompany2 = 0;
                }
                if(!expectCompany3) {
                    expectCompany3 = 0;
                }
              
                if(!phone) {
                    alert("您必须输入您的联系方式");
                    return false;
                }
                if(!roleId) {
                    alert("您必须选择一个职位名称");
                    return false;
                }
                if(!cityid) {
                    alert("您必须选择一个城市");
                    return false;
                }
                if(bestdomain == "") {
                    alert("您必须输入你的管理区域");
                    return false;
                }
               
            
                var param = {};
                param['name'] = name;        
                param['phone'] = phone;
                param['age'] = age;
                param['workage'] = workAge;
                param['inservice'] = inService;
                param['companyId'] = companyId;
                param['roleId'] = roleId;
                param['empid'] = empId; 
                param['domainId'] = domainId;
                param['bestdomain'] = bestdomain;
                param['expectCompany1'] = expectCompany1;
                param['expectCompany2'] = expectCompany2;
                param['expectCompany3'] = expectCompany3;
                param['expectJob']  = expectJob;
                param['intentTime'] = intentTime;
                param['cityId'] = cityid;
                if (empId && empId>0)
                  {
                  var postUrl = '${ctx}/admin/employee/update.do'
                  }
                else
                  { var postUrl = '${ctx}/admin/employee/create.do'}
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0")
                            {alert("保存成功");}
                        else{
                          alert(data.msg)
                        }
                        //location.href = '${ctx}/admin/employee/manage.page';
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });

            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/employee/manage.page';
                location.href = goUrl;
                return;
            }
           
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
            fullTextSearch:true,
        });
    });

</script>
</body>
<html>