<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>求职人管理</title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  求职人管理
</h2>
<br><br>
<button class="ui primary button" id="detail">
  详细信息
</button>
<button class="ui primary button" id="create">
  创建
</button>
<c:if test="${system:checkalias(sessionScope.UserId,'editEmployee')}">
<button class="ui primary button" id="edit">
  编辑
</button>
</c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'deleteEmployee')}">
<button class="ui primary button" id="del">
  删除
</button>
</c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'normaliseCompany')}">
<button class="ui primary button" id="normalise">
  公司名归一化
</button>
</c:if>
<br><br>

<div class="cont">
	<div class="ui input cell-div">
		<input type="text" name="search_name" id="search_name" placeholder="姓名">
  	</div>
    <div class="ui input cell-div">
         <input type="text" name="search_phone" id="search_phone" placeholder="联系电话">
    </div>
  	<div class="cell-div2">
  		<select class="ui dropdown" name="search_company" id="search_company">
            <option value="">请选择所在公司</option>
            <option value="0">公司名未归一化的</option>
            <c:forEach items="${companies}" var="company">  
              <option value="${company.id}">${company.name}</option>
            </c:forEach>
        </select>
  	</div>
  	<div class="cell-div">
  		<button class="ui primary button" id="search">
  				搜索
		</button>
  	</div>
</div>

<br><br>
	<table id="grid"></table>
	<div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
            if($(this).attr('id') == 'detail') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要查看的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['empid'];
				var goUrl = '${ctx}/admin/employee/detail.page?empid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'normalise') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要编辑的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var companyid = rowdata['companyId'];
                if(!companyid) {
                    alert("该候选人所在公司名称不需要归一化处理");
                    return;
                }
                var coe = rowdata['empid'];
                var goUrl = '${ctx}/admin/employee/normalise.page?empid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'edit') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要编辑的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var companyid = rowdata['companyId'];
                if(companyid) {
                    alert("该候选人所在公司名称需要归一化处理");
                    return;
                }
                var coe = rowdata['empid'];
                var goUrl = '${ctx}/admin/employee/form.page?empid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'create') {
              
              var goUrl = '${ctx}/admin/employee/toCreate.page';
              location.href = goUrl;
              return;
          }
            if($(this).attr('id') == 'del') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要删除的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['empid'];
				var goUrl = '${ctx}/admin/employee/delete.do?empid=' + coe;
				$.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        location.href = '${ctx}/admin/employee/manage.page';
                    	} else {
                    		alert(data.msg);
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
		} 
	};

	$(document).ready(function() { 

		$("#grid").jqGrid({
			postData: {
				sname: function() { return $("#search_name").val(); },
				scompanyid: function() { return $("#search_company").val(); },
                sphone: function() { return $("#search_phone").val(); }
			},
			mtype: 'POST',
			datatype : 'json',
			url : '${ctx}/admin/employee/search.do',
			width: 980, 
            height:420,
			rowNum : 10,
			colNames:['编号','归一化', '姓名','现任公司','职位','工作年限','工作状态', '城市', '联系方式'], 
			colModel:[ 
				{name:'empid', index:'empid', key: true, width:50},
                {name:'companyId', index:'companyId', width:50, formatter:companyFmatter}, 
				{name:'name', index:'name', width:100},
				{name:'companyName', index:'companyName', width:150},
                {name:'roleId', index:'roleId', width:100, formatter:roleFmatter},
                {name:'workage', index:'workage', width:80},
                {name:'intentTime', index:'intentTime', width:80, formatter:intentFmatter},
                {name:'cityId', index:'cityId', width:100, formatter:cityFmatter},
                {name:'phone', index:'phone', width:100}  
			],  
			pager: '#pager',  
			sortname: 'empid',  
			viewrecords: true,  
			sortorder: 'asc', 
			loadComplete : function(data) {
				//console.log(data)
			} 
		});

		$("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
		//$("#grid").jqGrid('filterToolbar',{autosearch:true});
		$('.button').on('click', handler.activate);

	});

    function companyFmatter (cellvalue, options, rowObject) {
        var companydata = "";
        
        if(cellvalue == 0) {
            companydata = "<img src='${ctx}/resources/images/normalise.png' alt='需要归一化公司名' />";
        }

        return companydata; 
    }

    function roleFmatter (cellvalue, options, rowObject) {
        var roledata = {};
        <c:forEach items="${roles}" var="role">
            roledata['s${role.id}'] = "${role.name}";
        </c:forEach>

        var keyss = 's'+cellvalue;
        return roledata[keyss]; 
    }

    function intentFmatter (cellvalue, options, rowObject) {
        var intStr = {};
        intStr['s0'] = "现在";
        intStr['s1'] = "一月后";
        intStr['s6'] = "半年后";
        intStr['s12'] = "一年后";

        var keyss = 's'+cellvalue;

        return intStr[keyss]; 
    }

    function cityFmatter (cellvalue, options, rowObject) {
        var citydata = {};
        <c:forEach items="${cities}" var="city">
            citydata['s${city.id}'] = "${city.name}";
        </c:forEach>

        var keyss = 's'+cellvalue;
        return citydata[keyss]; 
    }

</script>
</body>
</html>