<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
    <title>HR信息</title>  
</head>
<body>
<div class="ui segment" id="n-page">

        <div class="ui segment" id="n-content">
            <form class="ui form">
                <div class="field">
                    <label>姓名</label>
                    <div class="ui input">
                        <input type="text" name="name" id="name" placeholder="请输入姓名" value="${employer.name}">
                    </div>
                </div>
                <div class="field">
                    <label>手机</label>
                    <div class="ui input">
                        <input type="text" name="phone" id="phone" placeholder="请输入您的手机号码" value="${employer.phone}">
                    </div>
                </div>
                <div class="field">
                    <label>座机</label>
                    <div class="ui input">
                        <input type="text" name="telephone" id="telephone" placeholder="请输入您的座机号码" value="${employer.telephone}">
                    </div>
                </div>
                <div class="field">
                    <label>邮箱</label>
                    <div class="ui input">
                        <input type="text" name="email" id="email" placeholder="请输入您的邮箱" value="${employer.email}">
                    </div>
                </div>
                <input type="hidden" name="hrid" id="hrid" value="${employer.id}"/>
                  <input type="hidden" name="openid" id="openid" value="${employer.openid}"/>
            </form>        
        </div>
        <div class="ui segment" id="btn-next2">
            <div class="fluid ui button" id="next">下一步</div>
        </div>
</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this)
                .addClass('active') 
                .siblings() 
                .removeClass('active'); 

            if($(this).attr('id') == 'next') {
                var name = $("#name").val();
                var telephone = $("#telephone").val();
                var phone = $("#phone").val();
                var email = $("#email").val();
                var hrid  = $("#hrid").val();
				var openId = $("#openid").val();
                if(!name) {
                    alert("您必须输入您的名字");
                    return false;
                }
                if(!telephone) {
                    alert("您必须输入座机号码");
                    return false;
                }
                if(!phone) {
                    alert("您必须输入手机号码");
                    return false;
                }
                if(!email) {
                    alert("您必须输入电子邮箱");
                    return false;
                }

                var postUrl = '${ctx}/wechat/employer/create.do';
                var param = {};
                param['name']  = name;
                param['telephone'] = telephone;
                param['email']  = email;
                param['phone']  = phone;
              //  if (hrid)
               //// { param['id']     = hrid;}
               // if (openId)
              //  	{
              //  	  param['openid'] = openid;
              //  	}

                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data: param,
                    success: function(data){ 
                    	if(data.errorcode=="200"){
                    		var employer = data.data;
                            location.href = '${ctx}/wechat/company/toCreate.page?employerId='+employer.id;
                        }else{
                        	alert(data.msg);
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
    });

</script>
</body>
</html>