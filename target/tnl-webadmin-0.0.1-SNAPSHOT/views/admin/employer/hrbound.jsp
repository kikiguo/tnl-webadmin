<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>HR绑定</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
    HR绑定
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    HR管理
    </button>
    <br><br>
    <input type="hidden" name="hrid" id="hrid" value="${hr.id}"/>  
    <div class="ui segment" id="n-content">

        选择对应的公司<br><br>
        <form class="ui form">
            <div class="field">
                <label>选择公司名</label>
                <div class="ui search" id="existcompanyname" name="existcompanyname">
                    <div class="ui input">
                        <input class="prompt" type="text" placeholder="请输入公司名称" value="${company.name}">
                    </div>
                    <div class="results"></div>
                </div>    
            </div>
            <div class="ui segment" id="cont-next">
                <div class="ui primary button" id="subsitute">绑定</div>
            </div>
        </form>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'subsitute') {
                var hrid = $("#hrid").val();
                var existcompanyname = $('#existcompanyname').search('get value');
                
                if(!existcompanyname) {
                    alert("必须输入公司名称");
                    return false;
                }
            
                var param = {};     
                param['name'] = existcompanyname;
  
                var postUrl = '${ctx}/admin/hr/approve.do?op=subsitute&hrid=' + hrid;
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                            location.href = '${ctx}/admin/hr/manage.page';
                        } else {
                            alert(data.msg);
                        }
                        return;
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }


            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/hr/manage.page';
                location.href = goUrl;
                return;
            }
           
        } 
    };

    var companydata = [];
    
    <c:forEach items="${companies}" var="company">
        var tmp${company.id} = {};
        tmp${company.id}.title = "${company.name}";
        companydata.push(tmp${company.id});
    </c:forEach>

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
            fullTextSearch:true,
        });
        $('.ui.search').search({
            source: companydata,
            searchFullText: true
        });
    });

</script>
</body>
<html>