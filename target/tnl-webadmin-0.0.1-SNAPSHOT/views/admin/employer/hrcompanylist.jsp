<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>HR公司管理</title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  HR公司管理
</h2>
<br><br>
<button class="ui primary button" id="detail">
  详细信息
</button>
<c:if test="${system:checkalias(sessionScope.UserId,'reviewCompany')}">
<button class="ui primary button" id="check">
  认证
</button>
</c:if>
<button class="ui primary button" id="del">
  删除
</button>
<br><br>

<div class="cont">
    <div class="ui input cell-div">
        <input type="text" name="search_name" id="search_name" placeholder="公司名称">
      </div>
    <div class="ui input cell-div">
         <input type="text" name="search_phone" id="search_phone" placeholder="联系电话">
    </div>
      <div class="cell-div">
          <button class="ui primary button" id="search">
              搜索
        </button>
      </div>
</div>

<br><br>
    <table id="grid"></table>
    <div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
            if($(this).attr('id') == 'detail') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要查看的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/hrcompany/detail.page?companyid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'check') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要认证的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/hrcompany/form.page?companyid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要删除的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/hrcompany/delete.do?companyid=' + coe;
                $.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            alert(data.msg);
                        location.href = '${ctx}/admin/hrcompany/manage.page';
                        } else {
                            alert(data.msg);
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
        } 
    };

    $(document).ready(function() { 

        $("#grid").jqGrid({
            postData: {
                sname: function() { return $("#search_name").val(); },
                sphone: function() { return $("#search_phone").val(); }
            },
            mtype: 'POST',
            datatype : 'json',
            url : '${ctx}/admin/hrcompany/search.do',
            width: 980, 
            height:420,
            rowNum : 10,
            colNames:['编号','公司名', '联系人','联系电话','组织机构代码','细分领域'], 
            colModel:[ 
                {name:'id', index:'id', key: true, width:50},
                {name:'name', index:'name', width:100},
                {name:'contact', index:'contact', width:150},
                {name:'phone', index:'phone', width:100},
                {name:'orgnizationCode', index:'orgnizationCode', width:80},
                {name:'domain', index:'domain', width:80, formatter:domainFmatter}
            ],  
            pager: '#pager',  
            sortname: 'id',  
            viewrecords: true,  
            sortorder: 'asc', 
            loadComplete : function(data) {
                //console.log(data)
            } 
        });

        $("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
        $('.button').on('click', handler.activate);

    });

    function approveFmatter (cellvalue, options, rowObject) {
        if(cellvalue == 0) {
            return "未认证";
        } else {
            return "";
        }
    }

    function domainFmatter (cellvalue, options, rowObject) {
        var domaindata = {};
        <c:forEach items="${domains}" var="domain">
            domaindata['s${domain.id}'] = "${domain.name}";
        </c:forEach>

        var keyss = 's'+cellvalue;
        return domaindata[keyss]; 
    }


</script>
</body>
</html>