<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
    <title>错误页</title>  
</head>
<body>
<div class="ui segment" id="n-page">
&nbsp;<br>
操作中出现错误，请和管理员联系

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() { 
    });

</script>
</body>
</html>