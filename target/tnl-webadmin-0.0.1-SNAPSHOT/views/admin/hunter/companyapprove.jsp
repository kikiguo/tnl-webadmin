<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>猎头公司认证</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
    猎头公司认证
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    返回猎头管理
    </button>
    <br><br>
    <input type="hidden" name="hunterid" id="hunterid" value="${hunter.id}"/>  
    <div class="ui segment" id="n-content">
        1. 认证该猎头公司<br><br>
        <form class="ui form">
            <div class="field">
                <label>猎头公司名称</label>
                <div class="ui input">
                    <input type="text" name="newcompanyname" id="newcompanyname" value="${company.name}" placeholder="请输入公司名称" >
                </div>
            </div>
            <div class="field">
                <label>公司地址</label>
                <div class="ui input">
                    <input type="text" name="companyaddress" id="companyaddress" value="${company.address}" placeholder="请输入公司地址" >
                </div>
            </div>
            <div class="field">
                <label>联系人</label>
                <div class="ui input">
                    <input type="text" name="companycontact" id="companycontact" value="${company.contact}" placeholder="请输入公司联系人">
                </div>
            </div>
            <div class="field">
                <label>联系电话</label>
                <div class="ui input">
                    <input type="text" name="companyphone" id="companyphone" value="${company.phone}" placeholder="请输入公司联系电话">
                </div>
            </div>
            <div class="field">
                <label>组织机构代码</label>
                <div class="ui input">
                    <input type="text" name="orgcode" id="orgcode"  value="${company.orgnizationCode}" placeholder="请输入组织机构代码">
                </div>
            </div>
            <div class="field">
                <label>营业执照编码</label>
                <div class="ui input">
                    <input type="text" name="bizcode" id="bizcode"  value="${company.licenseCode}" placeholder="请输入营业执照编码">
                </div>
            </div>

            <div class="ui segment" id="cont-next">
                <div class="ui primary button" id="approvecompany">修改并认证</div>
            </div>
        </form>
        <div class="ui horizontal divider">
        或者
        </div>
        2. 使用现有猎头公司<br><br>
        <form class="ui form">
            <div class="field">
                <label>选择公司名</label>
                <div class="ui search" id="existcompanyname" name="existcompanyname">
                    <div class="ui input">
                        <input class="prompt" type="text" placeholder="请输入公司名称" value="${company.name}">
                    </div>
                    <div class="results"></div>
                </div>    
            </div>
            <div class="ui segment" id="cont-next">
                <div class="ui primary button" id="subsitute">替换</div>
            </div>
        </form>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'subsitute') {
                var hunterid = $("#hunterid").val();
                var existcompanyname = $('#existcompanyname').search('get value');
                
                if(!existcompanyname) {
                    alert("必须输入公司名称");
                    return false;
                }
            
                var param = {};     
                param['name'] = existcompanyname;
  
                var postUrl = '${ctx}/admin/hunter/approve.do?op=subsitute&hunterid=' + hunterid;
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                            location.href = '${ctx}/admin/hunter/manage.page';
                        } else {
                            alert(data.msg);
                        }
                        return;
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }

            if($(this).attr('id') == 'approvecompany') {

                var hunterid = $("#hunterid").val();
                //var newcompanyname = $('#newcompanyname').search('get value');
                var newcompanyname = $('#newcompanyname').val();

                var companyaddress = $("#companyaddress").val();
                var companycontact = $("#companycontact").val();

                var companyphone = $("#companyphone").val();
                var companyemail = $("#companyemail").val();
                var orgcode = $("#orgcode").val();
                var bizcode = $("#bizcode").val();
                
                if(!newcompanyname) {
                    alert("您必须输入公司名称");
                    return false;
                }
                if(!companycontact) {
                    alert("您必须输入联系人");
                    return false;
                }
                if(!companyphone) {
                    alert("您必须输入公司联系电话");
                    return false;
                }
                if(companyemail == "") {
                    alert("您必须输入公司邮箱");
                    return false;
                }
                if(!orgcode) {
                    alert("您必须输入公司组织机构代码");
                    return false;
                }
                if(bizcode == "") {
                    alert("您必须输入公司营业执照编码");
                    return false;
                }
               
                var param = {};
                param['name'] = newcompanyname;        
                param['orgnizationCode'] = orgcode;
                param['licenseCode'] = bizcode;
                param['contact'] = companycontact;
                param['phone'] = companyphone;
                param['email'] = companyemail;
                param['address'] = companyaddress;
  
                var postUrl = '${ctx}/admin/hunter/approve.do?op=create&hunterid=' + hunterid;
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data){ 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                            location.href = '${ctx}/admin/hunter/manage.page';
                        } else {
                            alert(data.msg);
                            return;
                        } 
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });

            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/hunter/manage.page';
                location.href = goUrl;
                return;
            }
           
        } 
    };

    var companydata = [];
    
    <c:forEach items="${companies}" var="company">
        var tmp${company.id} = {};
        tmp${company.id}.title = "${company.name}";
        companydata.push(tmp${company.id});
    </c:forEach>

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
            fullTextSearch:true,
        });
        $('.ui.search').search({
            source: companydata,
            searchFullText: true
        });
    });

</script>
</body>
<html>