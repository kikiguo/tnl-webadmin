<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
    <title>猎头信息</title>  
</head>
<body>
<div class="ui segment" id="n-page">

        <div class="ui segment" id="n-content">
            <form class="ui form">
                <div class="field">
                    <label>姓名</label>
                    <div class="ui input">
                        <input type="text" name="name" id="name" placeholder="请输入姓名" value="${hunter.name}">
                    </div>
                </div>
                <div class="field">
                    <label>年龄</label>
                    <div class="ui input">
                        <input type="text" name="age" id="age" placeholder="请输入年龄" value="${hunter.age}">
                    </div>
                </div>
                <div class="field">
                    <label>擅长领域</label>
                    <select class="ui search dropdown" name="domainid" id="domainid">
                    <option value="">请输入擅长领域名称</option>
                    <c:forEach items="${domains}" var="dm">
                        <c:choose>
                            <c:when test="${dm.id == hunter.domainid }">
                                <option value="${dm.id}" selected="selected">${dm.name}</option>
                            </c:when> 
                            <c:otherwise> 
                                <option value="${dm.id}">${dm.name}</option>
                            </c:otherwise> 
                        </c:choose>  
                    </c:forEach>
                    </select>
                </div>
                <div class="field">
                    <label>从业时间</label>
                    <div class="ui input">
                        <input type="text" name="workage" id="workage" placeholder="请输入从业时间" value="${hunter.workage}">
                    </div>
                </div>
                <div class="field">
                    <label>手机</label>
                    <div class="ui input">
                        <input type="text" name="mobile" id="mobile" placeholder="请输入您的手机号码" value="${hunter.mobile}">
                    </div>
                </div>
                <div class="field">
                    <label>座机</label>
                    <div class="ui input">
                        <input type="text" name="phone" id="phone" placeholder="请输入您的座机号码" value="${hunter.phone}">
                    </div>
                </div>
                <div class="field">
                    <label>邮箱</label>
                    <div class="ui input">
                        <input type="text" name="email" id="email" placeholder="请输入您的邮箱" value="${hunter.email}">
                    </div>
                </div>
                <input type="hidden" name="hunterid" id="hunterid" value="${hunter.id}"/>
            </form>        
        </div>
        <div class="ui segment" id="btn-next2">
            <div class="fluid ui button" id="next">下一步</div>
        </div>
</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this)
                .addClass('active') 
                .siblings() 
                .removeClass('active'); 

            if($(this).attr('id') == 'next') {
                var name = $("#name").val();
                var age  = $("#age").val();
                var domainid = $("#domainid").val();
                var workage  = $("#workage").val();
                var mobile  = $("#mobile").val();
                var phone = $("#phone").val();
                var email  = $("#email").val();
                var hunterid  = $("#hunterid").val();

                if(!name) {
                    alert("您必须输入您的名字");
                    return false;
                }
                if(!age) {
                    alert("您必须输入年龄");
                    return false;
                }
                if(!domainid) {
                    alert("您必须选择一个擅长领域");
                    return false;
                }
                if(!workage) {
                    alert("您必须输入从业时间");
                    return false;
                }
                if(!mobile) {
                    alert("您必须输入手机号码");
                    return false;
                }
                if(!email) {
                    alert("您必须输入电子邮箱");
                    return false;
                }

                var postUrl = '${ctx}/wechat/headhunter/toCreate_1.do';
                var param = {};
                param['name']  = name;
                param['age']   = age;
                param['domainid'] = domainid;
                param['workage']  = workage;
                param['mobile'] = mobile;
                param['email']  = email;
                param['phone']  = phone;
                param['id']     = hunterid;

                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data: param,
                    success: function(){ 
                        location.href = '${ctx}/wechat/headhunter/toCreate_21.do';
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
			fullTextSearch:true,
		});
    });

</script>
</body>
</html>