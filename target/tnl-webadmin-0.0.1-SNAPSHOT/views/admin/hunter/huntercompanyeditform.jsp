<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>猎头公司编辑</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
      猎头公司编辑
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    返回猎头公司管理
    </button>
    <br><br>
    <div class="ui segment" id="n-content">
        <form class="ui form">
            <div class="field">
                <label>公司名称</label>
                <div class="ui input">
                    <input type="text" name="companyname" id="companyname" value="${company.name}">
                </div>
            </div>
            <div class="field">
                <label>联系人</label>
                <div class="ui input">
                    <input type="text" name="companycontact" id="companycontact" value="${company.contact}" placeholder="请输入公司联系人">
                </div>
            </div>
            <div class="field">
                <label>联系电话</label>
                <div class="ui input">
                    <input type="text" name="companyphone" id="companyphone" value="${company.phone}" placeholder="请输入公司联系电话">
                </div>
            </div>
            <div class="field">
                <label>公司地址</label>
                <div class="ui input">
                    <input type="text" name="companyaddress" id="companyaddress" value="${company.address}" placeholder="请输入公司地址">
                </div>
            </div>
            <div class="field">
                <label>组织机构代码</label>
                <div class="ui input">
                    <input type="text" name="orgcode" id="orgcode" value="${company.orgnizationCode}" placeholder="请输入组织机构代码">
                </div>
            </div>
            <div class="field">
                <label>营业执照编码</label>
                <div class="ui input">
                    <input type="text" name="bizcode" id="bizcode" value="${company.licenseCode}" placeholder="请输入营业执照编码">
                </div>
            </div>                      
            <input type="hidden" name="huntercompanyid" id="huntercompanyid" value="${company.id}"/>  
        </form>

        <div class="ui segment" id="cont-next">
            <div class="ui primary button" id="next">提交</div>
        </div>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'next') {
                var huntercompanyid = $("#huntercompanyid").val();
                var companycontact = $("#companycontact").val();
                var companyphone   = $("#companyphone").val();
                var companyname    = $("#companyname").val();
                var companyaddress = $("#companyaddress").val();
                var orgcode  = $("#orgcode").val();
                var bizcode  = $("#bizcode").val();

                if(!companyname) {
                    alert("您必须输入公司名称");
                    return false;
                }

                if(!companycontact) {
                    alert("您必须输入公司联系人");
                    return false;
                }
                if(!companyphone) {
                    alert("您必须输入公司电话");
                    return false;
                }
                if(!companyaddress) {
                    alert("您必须输入公司地址");
                    return false;
                }
                if(!orgcode) {
                    alert("您必须输入公司组织代码");
                    return false;
                }
                if(!bizcode ) {
                    alert("您必须输入营业执照编码");
                    return false;
                }
               
                var param = {};
                param['contact'] = companycontact;
                param['name']    = companyname;
                param['phone']   = companyphone;
                param['address'] = companyaddress;
                //param['hunterid']       = hunterid;
                param['orgnizationCode'] = orgcode;
                param['licenseCode']     = bizcode;
                param['id']              = huntercompanyid;

                var postUrl = '${ctx}/admin/huntercompany/update.do'
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data) { 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                        }
                        location.href = '${ctx}/admin/huntercompany/manage.page';
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });

            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/huntercompany/manage.page';
                location.href = goUrl;
                return;
            }
           
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
    });

</script>
</body>
<html>