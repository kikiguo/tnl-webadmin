<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>猎头详细信息</title>  
</head>
<body>

<div class="ui segment">

	<h2 class="ui sub header">
  	猎头详细信息
	</h2>

	<br>
	<button class="ui primary button" id="backward">
    返回猎头管理
	</button>
	<button class="ui primary button" id="edit">
  	编辑
	</button>
	<button class="ui primary button" id="del">
  	删除
	</button>
<c:if test="${hunter.binded == 0}">
	<button class="ui primary button" id="check">
  	绑定审核
	</button>
</c:if>	
<c:if test="${company.approved == 0}">
    <button class="ui primary button" id="approve">
    公司认证
    </button>
</c:if> 

	<br><br><br>

    <b>猎头信息：</b>&nbsp;&nbsp;&nbsp;&nbsp;
    <c:if test="${hunter.binded == 0}">
        <font color='red'>等待绑定审核</font>
    </c:if>

    <br><br>

    姓名：${hunter.name}&nbsp;<br>&nbsp;<br>
    年龄：${hunter.age}&nbsp;<br>&nbsp;<br>
    擅长领域：
    <c:forEach items="${domains}" var="dm">
        <c:choose>
            <c:when test="${dm.id == hunter.majordomain1 }">
                ${dm.name}
            </c:when> 
            <c:when test="${dm.id == hunter.majordomain2 }">
                / ${dm.name}
            </c:when> 
            <c:when test="${dm.id == hunter.majordomain3 }">
                / ${dm.name}
            </c:when> 
        </c:choose>  
    </c:forEach>
    &nbsp;<br>&nbsp;<br>
    从业时间：${hunter.workage}年&nbsp;<br>&nbsp;<br>
    手机：${hunter.phone}&nbsp;<br>&nbsp;<br>
    座机：${hunter.telephone}&nbsp;<br>&nbsp;<br>
    邮箱：${hunter.email}&nbsp;<br>&nbsp;<br>

	<br><br>
    <b>猎头公司信息：</b>&nbsp;&nbsp;&nbsp;&nbsp;
    <c:if test="${company.approved == 0}">
        <font color='red'>等待公司信息认证</font>
    </c:if>
	<br><br>

    公司名称：${company.name}&nbsp;<br>&nbsp;<br>
    联系人：${company.contact}&nbsp;<br>&nbsp;<br>
    联系电话：${company.phone}&nbsp;<br>&nbsp;<br>
    公司地址：${company.address}&nbsp;<br>&nbsp;<br>
    组织机构代码：${company.orgnizationCode}&nbsp;<br>&nbsp;<br>
    公司营业执照编码：${company.licenseCode}&nbsp;<br>&nbsp;<br> 

</div>

<input type="hidden" name="approved" id="approved" value="${company.approved}" />

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script type="text/javascript">
	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'backward') {
				var goUrl = '${ctx}/admin/hunter/manage.page';
                location.href = goUrl;
                return;
            }
			if($(this).attr('id') == 'check') {
                var approved = $('#approved').val();
                if(approved != 1) {
                    alert("公司需要先认证");
                    return;
                }
                var goUrl = '${ctx}/admin/hunter/check.do?hunterid=${hunter.id}';
                $.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            alert(data.msg);
                        location.href = '${ctx}/admin/hunter/manage.page';
                        } else {
                            alert(data.msg);
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
            }
            if($(this).attr('id') == 'approve') {
                var goUrl = '${ctx}/admin/hunter/approve.page?companyid=${company.id}&hunterid=${hunter.id}';
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'edit') {
                var goUrl = '${ctx}/admin/hunter/form.page?hunterid=${hunter.id}';
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
            	var goUrl = '${ctx}/admin/hunter/delete.do?hunterid=${hunter.id}';
				$.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        location.href = '${ctx}/admin/hunter/manage.page';
                    	} else {
                    		alert(data.msg);
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
		} 
	};

	$(document).ready(function() { 
		$('.button').on('click', handler.activate)
	});

</script>
</body>
</html>