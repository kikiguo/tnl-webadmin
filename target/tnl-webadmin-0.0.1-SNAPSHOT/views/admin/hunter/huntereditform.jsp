<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" /> 
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <title>猎头编辑</title>  
</head>

<body>
<div class="ui segment" id="content">

    <h2 class="ui sub header">
      猎头编辑
    </h2>
    <br>
    <button class="ui primary button" id="backward">
    返回猎头管理
    </button>
    <br><br>
    <div class="ui segment" id="n-content">
        <form class="ui form">
            <div class="field">
                <label>姓名</label>
                <div class="ui input">
                    <input type="text" name="name" id="name" placeholder="请输入姓名" value="${hunter.name}">
                </div>
            </div>
            <div class="field">
                <label>现任猎头公司</label> 
                <select name="companyId" id="companyId" class="ui selection dropdown">
                    <c:choose>
                        <c:when test="${(hunter.companyId == 0) and (hunter.companyName != '')}">
                            <option value="">${hunter.companyName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="">现任猎头公司名称</option>
                        </c:otherwise>
                    </c:choose>
                    <c:forEach items="${companies}" var="company">
                        <c:choose>
                            <c:when test="${hunter.companyId == company.id }">
                                <option value="${company.id}" selected="selected">${company.name}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${company.id}">${company.name}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <div class="field">
                <label>年龄</label>
                <div class="ui input">
                    <input type="text" name="age" id="age" placeholder="请输入年龄" value="${hunter.age}">
                </div>
            </div>
            <div class="field">
                <label>擅长领域1</label>
                <select class="ui selection dropdown" name="majordomain1" id="majordomain1">
                <option value="">请输入擅长领域名称</option>
                <c:forEach items="${domains}" var="dm">
                    <c:choose>
                        <c:when test="${dm.id == hunter.majordomain1 }">
                            <option value="${dm.id}" selected="selected">${dm.name}</option>
                        </c:when> 
                        <c:otherwise> 
                            <option value="${dm.id}">${dm.name}</option>
                        </c:otherwise> 
                    </c:choose>  
                </c:forEach>
                </select>
            </div>
            <div class="field">
                <label>擅长领域2</label>
                <select class="ui selection dropdown" name="majordomain2" id="majordomain2">
                <option value="">请输入擅长领域名称</option>
                <c:forEach items="${domains}" var="dm">
                    <c:choose>
                        <c:when test="${dm.id == hunter.majordomain2 }">
                            <option value="${dm.id}" selected="selected">${dm.name}</option>
                        </c:when> 
                        <c:otherwise> 
                            <option value="${dm.id}">${dm.name}</option>
                        </c:otherwise> 
                    </c:choose>  
                </c:forEach>
                </select>
            </div>
            <div class="field">
                <label>擅长领域3</label>
                <select class="ui selection dropdown" name="majordomain3" id="majordomain3">
                <option value="">请输入擅长领域名称</option>
                <c:forEach items="${domains}" var="dm">
                    <c:choose>
                        <c:when test="${dm.id == hunter.majordomain3 }">
                            <option value="${dm.id}" selected="selected">${dm.name}</option>
                        </c:when> 
                        <c:otherwise> 
                            <option value="${dm.id}">${dm.name}</option>
                        </c:otherwise> 
                    </c:choose>  
                </c:forEach>
                </select>
            </div>
            <div class="field">
                <label>从业时间</label>
                <div class="ui input">
                    <input type="text" name="workage" id="workage" placeholder="请输入从业时间" value="${hunter.workage}">
                </div>
            </div>
            <div class="field">
                <label>手机</label>
                <div class="ui input">
                    <input type="text" name="phone" id="phone" placeholder="请输入您的手机号码" value="${hunter.phone}">
                </div>
            </div>
            <div class="field">
                <label>座机</label>
                <div class="ui input">
                    <input type="text" name="telephone" id="telephone" placeholder="请输入您的座机号码" value="${hunter.telephone}">
                </div>
            </div>
            <div class="field">
                <label>邮箱</label>
                <div class="ui input">
                    <input type="text" name="email" id="email" placeholder="请输入您的邮箱" value="${hunter.email}">
                </div>
            </div>
            <input type="hidden" name="hunterid" id="hunterid" value="${hunter.id}"/>   
        </form>

        <div class="ui segment" id="cont-next">
            <div class="ui primary button" id="next">提交</div>
        </div>
    </div>
</div>

    <script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
    <script src="${ctx}/resources/semanticui/semantic.js"
        type="text/javascript"></script>

    <script type="text/javascript">
    var handler = { 
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            if($(this).attr('id') == 'next') {
                var name = $("#name").val();
                var age  = $("#age").val();
                var majordomain1 = $("#majordomain1").val();
                var workage  = $("#workage").val();
                var telephone  = $("#telephone").val();
                var phone = $("#phone").val();
                var email  = $("#email").val();
                var hunterid  = $("#hunterid").val();
                var companyid = $("#companyId").val();

                if(!name) {
                    alert("您必须输入您的名字");
                    return false;
                }
                if(!age) {
                    alert("您必须输入年龄");
                    return false;
                }
                if(!majordomain1) {
                    alert("您必须选择一个擅长领域");
                    return false;
                }
                if(!workage) {
                    alert("您必须输入从业时间");
                    return false;
                }
                if(!phone) {
                    alert("您必须输入手机号码");
                    return false;
                }
                if(!telephone) {
                    alert("您必须输入座机号码");
                    return false;
                }
                if(!email) {
                    alert("您必须输入电子邮箱");
                    return false;
                }
                if(!companyid) {
                    alert("您必须选择公司");
                    return false;
                }
               
                var param = {};
                param['name']  = name;
                param['age']   = age;
                param['majordomain1'] = majordomain1;
                param['majordomain2'] = majordomain2;
                param['majordomain3'] = majordomain3;
                param['workage']  = workage;
                param['telephone'] = telephone;
                param['email']  = email;
                param['phone']  = phone ;
                param['id'] = hunterid;
                param['companyId'] = companyid;

                var postUrl = '${ctx}/admin/hunter/update.do'
                $.ajax({ 
                    url: postUrl, 
                    type: "POST", 
                    data : param,
                    dataType : 'json',
                    success: function(data) { 
                        if (data.errorcode=="0") {
                            alert("保存成功");
                        }
                        location.href = '${ctx}/admin/hunter/manage.page';
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });

            }

            if($(this).attr('id') == 'backward') {
                var goUrl = '${ctx}/admin/hunter/manage.page';
                location.href = goUrl;
                return;
            }
           
        } 
    };

    $(document).ready(function() { 
        $('.button').on('click', handler.activate);
        $('.dropdown').dropdown({
            fullTextSearch:true,
        });
    });

</script>
</body>
<html>