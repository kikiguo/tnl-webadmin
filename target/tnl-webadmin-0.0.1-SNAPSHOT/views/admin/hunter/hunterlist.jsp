<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqueryjqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <title>猎头管理</title>  
</head>
<body>

<div class="ui segment">

<h2 class="ui sub header">
  猎头管理
</h2>
<br><br>
<button class="ui primary button" id="detail">
  详细信息
</button>
<c:if test="${system:checkalias(sessionScope.UserId,'editHunter')}">
<button class="ui primary button" id="edit">
  编辑
</button></c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'deleteHunter')}">
<button class="ui primary button" id="del">
  删除
</button>
</c:if>
<c:if test="${system:checkalias(sessionScope.UserId,'reviewHunter')}">
<button class="ui primary button" id="check">
  审核绑定
</button></c:if>
<br><br>

<div class="cont">
	<div class="ui input cell-div">
		<input type="text" name="search_name" id="search_name" placeholder="姓名">
  	</div>
    <div class="ui input cell-div">
         <input type="text" name="search_phone" id="search_phone" placeholder="联系电话">
    </div>
  	<div class="cell-div2">
  		<select class="ui dropdown" name="search_company" id="search_company">
            <option value="">请选择所在公司</option>
            <c:forEach items="${companies}" var="company">  
              <option value="${company.id}">${company.name}</option>
            </c:forEach>
        </select>
  	</div>
  	<div class="cell-div">
  		<button class="ui primary button" id="search">
  				搜索
		</button>
  	</div>
</div>

<br><br>
	<table id="grid"></table>
	<div id="pager"></div>
</div>

<br>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<!-- 
<script src="${ctx}/resources/jqueryjqgrid/js/jquery-1.11.0.min.js"></script>
 -->
<script src="${ctx}/resources/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqueryjqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">

	var handler = { 
		activate: function() { 
			$(this).addClass('active').siblings().removeClass('active'); 

			if($(this).attr('id') == 'search') {
                $("#grid").trigger("reloadGrid");
                return;
            }
            if($(this).attr('id') == 'detail') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要查看的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/hunter/detail.page?hunterid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'check') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要审核的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var binded = rowdata['binded'];
                if(binded != "未绑定") {
                    alert("该猎头已经绑定了");
                    return;
                }
                //var approved = rowdata['approved'];
                //if(approved != 1) {
                //    alert("该猎头的公司尚未认证");
                //    return;
                //}
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/hunter/approve.page?hunterid=' + coe;
                //$.ajax({ 
                //    url: goUrl, 
                //    type: "GET", 
                //    success: function(data, status){ 
                //        if(data.errorcode == 0) {
                //            alert(data.msg);
                //       location.href = '${ctx}/admin/hunter/manage.page';
                //        } else {
                //            alert(data.msg);
                //        }
                //    }, 
                //    error: function(){ 
                //        alert("服务出错，请稍后尝试"); 
                //   } 
                //});
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'edit') {
                var grid = $("#grid");
                var id = grid.jqGrid("getGridParam", "selrow");
                if (!id) {
                    alert("请选中要编辑的行");
                    return;
                }
                var rowdata = grid.jqGrid('getRowData', id);
                var coe = rowdata['id'];
                var goUrl = '${ctx}/admin/hunter/form.page?hunterid=' + coe;
                location.href = goUrl;
                return;
            }
            if($(this).attr('id') == 'del') {
            	var grid = $("#grid");
            	var id = grid.jqGrid("getGridParam", "selrow");
				if (!id) {
					alert("请选中要删除的行");
					return;
				}
				var rowdata = grid.jqGrid('getRowData', id);
				var coe = rowdata['id'];
				var goUrl = '${ctx}/admin/hunter/delete.do?hunterid=' + coe;
				$.ajax({ 
                    url: goUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                    	if(data.errorcode == 0) {
                    		alert(data.msg);
                        location.href = '${ctx}/admin/hunter/manage.page';
                    	} else {
                    		alert(data.msg);
                    	}
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
                });
                return;
            }
		} 
	};

	$(document).ready(function() { 

		$("#grid").jqGrid({
			postData: {
				sname: function() { return $("#search_name").val(); },
				scompanyid: function() { return $("#search_company").val(); },
                sphone: function() { return $("#search_phone").val(); }
			},
			mtype: 'POST',
			datatype : 'json',
			url : '${ctx}/admin/hunter/search.do',
			width: 980, 
            height:420,
			rowNum : 10,
			colNames:['编号','绑定', '姓名','电话','擅长领域','认证状态'], 
			colModel:[ 
				{name:'id', index:'id', key: true, width:50},
                {name:'binded', index:'binded', width:50, formatter:bindFmatter}, 
				{name:'name', index:'name', width:100},
				
                {name:'phone', index:'phone', width:100},
                {name:'majordomain1', index:'majordomain1', width:80, formatter:domainFmatter},
                {name:'state', index:'state', width:100, formatter:approveFmatter}
			],  
			pager: '#pager',  
			sortname: 'id',  
			viewrecords: true,  
			sortorder: 'asc', 
			loadComplete : function(data) {
				//console.log(data)
			} 
		});

		$("#grid").jqGrid('navGrid','#pager',{edit:false,add:false,del:false, search:false});
		//$("#grid").jqGrid('filterToolbar',{autosearch:true});
		$('.button').on('click', handler.activate);

	});

    function bindFmatter (cellvalue, options, rowObject) {
        
        if(cellvalue == 1) {
            return "绑定";
        }

        return "未绑定"; 
    }

    function approveFmatter (cellvalue, options, rowObject) {
        if(cellvalue == "approved") {
            return "已认证";
        } else {
            return "未认证";
        }
    }

    function domainFmatter (cellvalue, options, rowObject) {
        var domaindata = {};
        <c:forEach items="${domains}" var="domain">
            domaindata['s${domain.id}'] = "${domain.name}";
        </c:forEach>

        var keyss = 's'+cellvalue;
        return domaindata[keyss]; 
    }


</script>
</body>
</html>