<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqplot/jquery.jqplot.css" rel="stylesheet" type="text/css"/>
    <title>何时联系</title>  
</head>
<body>

<div class="ui segment">

  <div class="ui segment">
    <div class="ui form">
      <div class="field">
        <label>选择细分领域</label>
        <select class="ui dropdown" id="subdomain" name="subdomain">
            <c:forEach items="${domains}" var="domain">  
                <option value="${domain.id}">${domain.name}</option>     
            </c:forEach>
        </select>
      </div>
    </div>
    <h3 class="ui top attached header">
    细分领域人才考虑流动时间段
    </h3> 
    <div class="ui attached segment"> 
      &nbsp; 
      <br>&nbsp;<br> 
      <div id="chartdiv1" class="charCanv"></div> 
      <br> 
      <button class="ui primary button" id="save1"> 
      保存为图片 
      </button> 
    </div> 
  </div> 

  <div class="ui segment">
    <div class="ui form">
      <div class="field">
        <label>选择公司</label>
        <select class="ui dropdown" id="company" name="company">
            <c:forEach items="${companies}" var="company">  
                <option value="${company.id}">${company.name}</option>     
            </c:forEach>
        </select>
      </div>
    </div>
    <h3 class="ui top attached header">
    人才考虑机会时间段与期待岗位 
    </h3> 
    <div class="ui attached segment"> 
      &nbsp; 
      <br>&nbsp;<br> 
      <div id="chartdiv2" class="charCanv"></div> 
      <br> 
      <button class="ui primary button" id="save2"> 
      保存为图片 
      </button> 
    </div> 
  </div> 

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/jquery.jqplot.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pieRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.barRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.donutRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pointLabels.min.js" type="text/javascript" ></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.categoryAxisRenderer.min.js" type="text/javascript" ></script>

<script type="text/javascript">
    var plot1, plot2;

    var datasource = {
        getdata1: function() {
            var subdomain = $('#subdomain').val();
            if(!subdomain) {
                subdomain = 3;
            }

            var getUrl = '${ctx}/admin/report/hslx1.do?subdomain=' + subdomain;
            var that = this;
            $.ajax({ 
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata1(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata1:function(data) {

            $.jqplot.config.enablePlugins = true; 
            var monthticks = ['现在', '1个月', '6个月', '一年以后'];

            ///////chart 1
            //var jsondata1 = { 
            //    subdomain:"ICU", 
            //    monthdata:[399, 484, 344, 343]
            //};
            var jsondata1 = data.data;

            var monthdata = jsondata1.monthdata;

            if(plot1) { 
                plot1.destroy(); 
            }
            plot1 = $.jqplot('chartdiv1', [monthdata], { 
                animate: !$.jqplot.use_excanvas, 
                seriesDefaults:{ 
                    renderer:$.jqplot.BarRenderer, 
                    shadow: false, 
                    pointLabels: {show: true, location:'s'} 
                }, 
                axes: { 
                    xaxis: { 
                        renderer: $.jqplot.CategoryAxisRenderer, 
                        ticks: monthticks 
                    }, 
                    yaxis: { 
                        showTicks: false, 
                    } 
                }, 
                highlighter: { show: false }, 
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false}, 
                title: jsondata1.subdomain + '细分领域人才考虑流动时间段',  
            });
        },

        getdata2: function() {
            var company = $('#company').val();
            if(!company) {
                company = 1;
            }

            var getUrl = '${ctx}/admin/report/hslx2.do?company=' + company;
            var that = this;
            $.ajax({
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata2(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata2:function(data) { 
            $.jqplot.config.enablePlugins = true; 
            var monthticks = ['现在', '1个月', '6个月', '一年以后'];

            //var jsondata2 = { 
            //    companyname:"罗氏诊断", 
            //    positiondata:[  
            //        [451, 398, 365, 265],  
            //        [160, 350, 101, 85],  
            //        [589, 489, 479, 450],  
            //        [398, 366, 284, 95] 
            //    ] 
            //}; 
            var jsondata2 = data.data;

            var positions  = ['技术支持','售后服务','市场','销售']; 
            var positiondata1 = jsondata2.positiondata[0]; 
            var positiondata2 = jsondata2.positiondata[1]; 
            var positiondata3 = jsondata2.positiondata[2]; 
            var positiondata4 = jsondata2.positiondata[3];

            if(plot2) { 
                plot2.destroy(); 
            }
            plot2 = $.jqplot('chartdiv2', [positiondata1, positiondata2, positiondata3, positiondata4], { 
                stackSeries: true, 
                animate: !$.jqplot.use_excanvas, 
                seriesDefaults: { 
                    renderer:$.jqplot.BarRenderer, 
                    shadow: false, 
                }, 
                axes: { 
                    xaxis: { 
                        renderer: $.jqplot.CategoryAxisRenderer, 
                        ticks: monthticks 
                    }, 
                    yaxis: { 
                        showTicks: false, 
                    }
                }, 
                series:[ 
                    {label:positions[0]}, 
                    {label:positions[1]}, 
                    {label:positions[2]}, 
                    {label:positions[3]} 
                ], 
                highlighter: {show: false}, 
                legend: {show:true, location: 'e',placement: 'outside'}, 
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false}, 
                title: '青睐' + jsondata2.companyname + '的人才考虑机会时间段与期待岗位',  
            });
        },

    };

    $(document).ready(function() {
        //datasource.getdata1(); 

        //$('#subdomain').change(function(){
        //    datasource.getdata1();
        //});

        var $dropdown1 = $('#company'); 
        $dropdown1.dropdown({ 
            onChange: function(value) { 
                datasource.getdata2();
            }
        });

        var $dropdown2 = $('#subdomain'); 
        $dropdown2.dropdown({ 
            onChange: function(value) { 
                datasource.getdata1();
            } 
        }); 

        var handler = {  
            activate: function() {  
                $(this).addClass('active').siblings().removeClass('active');  
                if($(this).attr('id') == 'save1') { 
                    $('#chartdiv1').jqplotSaveImage();  
                } 
                if($(this).attr('id') == 'save2') { 
                    $('#chartdiv2').jqplotSaveImage();  
                } 
            } 
        }; 
        $('.button').on('click', handler.activate);

  //$.jqplot.config.enablePlugins = true;

  //var monthticks = ['现在', '1个月', '6个月', '一年以后'];

  ///////chart 1
  //var jsondata1 = {
  //  subdomain:"ICU",
  //  monthdata:[399, 484, 344, 343]
  //};
  //var monthdata = jsondata1.monthdata;

  //var plot1 = $.jqplot('chartdiv1', [monthdata], {
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    shadow: false,
  //    pointLabels: {show: true, location:'s'}
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: monthticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: { show: false },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: jsondata1.subdomain + '细分领域人才考虑流动时间段', 
  //});

  ////////chart 2
  //var jsondata2 = {
  //  companyname:"罗氏诊断",
  //  positiondata:[ 
  //    [451, 398, 365, 265], 
  //    [160, 350, 101, 85], 
  //    [589, 489, 479, 450], 
  //    [398, 366, 284, 95]
  //  ]
  //};

  //var positions  = ['技术支持','售后服务','市场','销售'];

  //var positiondata1 = jsondata2.positiondata[0];  
  //var positiondata2 = jsondata2.positiondata[1];
  //var positiondata3 = jsondata2.positiondata[2];
  //var positiondata4 = jsondata2.positiondata[3];

  //var plot2 = $.jqplot('chartdiv2', [positiondata1, positiondata2, positiondata3, positiondata4], {
  //  stackSeries: true,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults: {
  //    renderer:$.jqplot.BarRenderer,
  //    shadow: false,
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: monthticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  series:[
  //    {label:positions[0]},
  //    {label:positions[1]},
  //   {label:positions[2]},
  //    {label:positions[3]}
  //  ],
  //  highlighter: {show: false},
  //  legend: {show:true, location: 'e',placement: 'outside'},
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: '青睐' + jsondata2.companyname + '的人才考虑机会时间段与期待岗位', 
  //});


    });

</script>
</body>
</html>