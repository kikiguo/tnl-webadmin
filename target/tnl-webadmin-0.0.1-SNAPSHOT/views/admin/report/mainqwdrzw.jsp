<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqplot/jquery.jqplot.css" rel="stylesheet" type="text/css"/>
    <title>期望担任职位</title>  
</head>
<body>

<div class="ui segment"> 
    <div class="ui segment">
        <div class="ui form">
            <div class="field">
                <label>选择公司</label>
                <select class="ui dropdown" id="company" name="company">
                <c:forEach items="${companies}" var="company">  
                    <option value="${company.id}">${company.name}</option>     
                </c:forEach>
                </select>
            </div>
        </div>
        <h3 class="ui top attached header">
        最感兴趣的5个职位 
        </h3> 
        <div class="ui attached segment"> 
            &nbsp; 
            <br>&nbsp;<br> 
            <div id="chartdiv1" class="charCanv"></div> 
            <br> 
            <button class="ui primary button" id="save1"> 
                保存为图片 
            </button> 
        </div> 
    </div> 

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/jquery.jqplot.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pieRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.barRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.donutRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pointLabels.min.js" type="text/javascript" ></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.categoryAxisRenderer.min.js" type="text/javascript" ></script>

<script type="text/javascript">

    var plot1;

    var datasource = {
        getdata1: function() {
            var company = $('#company').val();
            if(!company) {
                company = 1;
            }

            var getUrl = '${ctx}/admin/report/qwdrzw.do?company=' + company;
            var that = this;
            $.ajax({ 
                url: getUrl, 
                type: "GET", 
                success: function(data, status){ 
                    if(data.errorcode == 0) {
                        that.appenddata1(data);
                    } else {
                        var errorUrl = '${ctx}/admin/error.page';
                        location.href = errorUrl;
                    }
                }, 
                error: function(){ 
                    alert("服务出错，请稍后尝试"); 
                } 
            });
        },

        appenddata1:function(data) {

            $.jqplot.config.enablePlugins = true; 
            //var monthticks = ['现在', '1个月', '6个月', '一年以后'];

            ///////chart 1 
            //var jsondata1 = { 
            //    companyname:"罗氏制药",
            //    positiondata:[399, 484, 344, 343, 200],
            //    positionticks:['销售','售后服务','技术支持','市场','临床'] <-这里应该是职位名
            //};
            var jsondata1 = data.data;

            var positiondata = jsondata1.positiondata;
            var positionticks = jsondata1.positionticks;
            var companyname = jsondata1.companyname;

            if(plot1) { 
                plot1.destroy(); 
            }
            plot1 = $.jqplot('chartdiv1', [positiondata], {
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    shadow: false,
                    pointLabels: {show: true, location:'s'}
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: positionticks
                    },
                    yaxis: {
                        showTicks: false,
                    }
                },
                highlighter: { show: false },
                grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
                title: '对' + companyname + '最感兴趣的5个职位', 
            });

        },
    };

    $(document).ready(function() { 
        var $dropdown1 = $('#company'); 
        $dropdown1.dropdown({ 
            onChange: function(value) { 
                datasource.getdata1();
            } 
        }); 

        var handler = { 
            activate: function() {  
                $(this).addClass('active').siblings().removeClass('active'); 
                if($(this).attr('id') == 'save1') { 
                    $('#chartdiv1').jqplotSaveImage(); 
                } 
            } 
        }; 
        $('.button').on('click', handler.activate);

  //$.jqplot.config.enablePlugins = true;

  //var monthticks = ['现在', '3个月', '6个月', '一年以后'];

  ///////chart 1
  //var jsondata1 = {
  //  companyname:"罗氏制药",
  //  positiondata:[399, 484, 344, 343, 200],
  //  positionticks:['销售','售后服务','技术支持','市场','临床']
  //};

  //var positiondata = jsondata1.positiondata;
  //var positionticks = jsondata1.positionticks;
  //var companyname = jsondata1.companyname;

  //var plot1 = $.jqplot('chartdiv1', [positiondata], {
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    shadow: false,
  //    pointLabels: {show: true, location:'s'}
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: positionticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  highlighter: { show: false },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: '对' + companyname + '最感兴趣的5个职位', 
  //});

    });

</script>
</body>
</html>