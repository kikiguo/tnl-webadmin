<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">  
    <link href="${ctx}/resources/semanticui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/main.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/jqplot/jquery.jqplot.css" rel="stylesheet" type="text/css"/>
    <title>所在城市</title>  
</head>
<body>

<div class="ui segment">
    <div class="ui segment">
        <div class="ui form">
            <div class="field">
                <label>选择细分领域</label>
                <select class="ui dropdown" id="subdomain" name="subdomain">
                <c:forEach items="${domains}" var="domain">  
                    <option value="${domain.id}">${domain.name}</option>     
                </c:forEach>
                </select>
            </div>
        </div>
    </div>

    <h3 class="ui top attached header">
        细分领域人才分布情况
    </h3>
    <div class="ui attached segment">
    <!--
        <div id="chartdiv1" class="charCanv"></div>
        <br>
        <button class="ui primary button" id="save1">
            保存为图片
        </button>
        &nbsp;
        <br>&nbsp;<br>
        <div id="chartdiv2" class="charCanv"></div>
        <br>
        <button class="ui primary button" id="save2">
            保存为图片
        </button>

        &nbsp;
        <br>&nbsp;<br> -->
        <div id="chartdiv3" class="charCanv"></div>
        <br>
        <button class="ui primary button" id="save3">
            保存为图片
        </button>

        &nbsp;
        <br>&nbsp;<br>
        <div id="chartdiv4" class="charCanv"></div>
        <br>
        <button class="ui primary button" id="save4">
            保存为图片
        </button>

        &nbsp;
        <br>&nbsp;<br>
        <div id="chartdiv5" class="charCanv"></div>
        <br>
        <button class="ui primary button" id="save5">
            保存为图片
        </button>

        &nbsp;
        <br>&nbsp;<br>
        <div id="chartdiv6" class="charCanv"></div>
        <br>
        <button class="ui primary button" id="save6">
            保存为图片
        </button>
    </div>

</div>

<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
<script src="${ctx}/resources/semanticui/semantic.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/jquery.jqplot.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pieRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.barRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.donutRenderer.min.js" type="text/javascript"></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.pointLabels.min.js" type="text/javascript" ></script>
<script src="${ctx}/resources/jqplot/plugins/jqplot.categoryAxisRenderer.min.js" type="text/javascript" ></script>

<script type="text/javascript">

var plot1, plot2, plot3, plot4, plot5, plot6;

var datasource = {
    getdata1: function() {
        var subdomain = $('#subdomain').val();
        if(!subdomain) {
            subdomain = 3;
        }

        var getUrl = '${ctx}/admin/report/szcs.do?subdomain=' + subdomain;
        var that = this;
        $.ajax({ 
            url: getUrl, 
            type: "GET", 
            success: function(data, status){ 
                if(data.errorcode == 0) {
                    that.appenddata1(data);
                } else {
                    var errorUrl = '${ctx}/admin/error.page';
                    location.href = errorUrl;
                }
            }, 
            error: function(){ 
                alert("服务出错，请稍后尝试"); 
            } 
        });
    },

    appenddata1:function(data) {
        $.jqplot.config.enablePlugins = true;

        /*
        var jsondata1 = {
            subdomainname:"IDV",
            data:[['研发',158], ['临床教育',289], ['注册/法规',298], ['技术支持',388], ['售后服务',456], ['生产', 597], ['市场',897], ['销售',1598]]
        };

        var s1 = jsondata1.data;
        var subdomainname = jsondata1.subdomainname;

        var labels2 = [];
        for(var i=0; i<s1.length; i++) { 
            var tmp = s1[i][0] + "-" + s1[i][1];
            labels2.push(tmp);
        }

        var plot1 = $.jqplot('chartdiv1', [s1], {
            stackSeries: false,
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.DonutRenderer,
                rendererOptions: {
                    showDataLabels: true,
                    dataLabels:labels2
                },
                shadow: false,
            },
            highlighter: {show: false},
            grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
            title: subdomainname + '细分领域人才职位分布情况', 
        });

        var labels3 = [];
        var sum = 0;
        for(var i=0; i<s1.length; i++) {
            sum += s1[i][1];
        }
        for(var i=0; i<s1.length; i++) { 
            var per = s1[i][1]/sum*100;
            per = Math.round(per*100)/100;
            per = per + '%';
            var tmp = s1[i][0] + "-" + per;
            labels3.push(tmp);
        }

        var plot2 = $.jqplot('chartdiv2', [s1], {
            stackSeries: false,
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.DonutRenderer,
                rendererOptions: {
                    showDataLabels: true,
                    dataLabels:labels3
                },
                shadow: false,
            },
            highlighter: { show: false },
            grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
            title: subdomainname + '细分领域人才职位分布情况', 
        });

        */
        //var jsondata2 = {
        //    subdomainname:"IVD",
        //    companynames:['公司A', '公司B', '公司C', '公司D', '公司E', '公司F'],
        //    cityticks:['成都', '深圳', '广州', '上海', '北京'],
        //    citydata:[[146, 265, 365, 398, 451],
        //        [56, 85, 101, 350, 160],
        //        [465, 450, 478, 489, 549],
        //        [101, 95, 284, 366, 398],
        //        [101, 95, 284, 366, 398],
        //        [215, 328, 412, 589, 679]
        //    ]
        //};
        var jsondata2 = data.data2;

        var data1 = jsondata2.citydata[0];
        var data2 = jsondata2.citydata[1];
        var data3 = jsondata2.citydata[2];
        var data4 = jsondata2.citydata[3];
        var data5 = jsondata2.citydata[4];
        var data6 = jsondata2.citydata[5];

        var sum = [];
        for(var i=0; i<data1.length; i++) {
            var tmp = 0;
            tmp += data1[i]; 
            tmp += data2[i]; 
            tmp += data3[i]; 
            tmp += data4[i]; 
            tmp += data5[i]; 
            tmp += data6[i]; 
            sum.push(tmp);
        }

        var data1p = [];
        var data2p = [];
        var data3p = [];
        var data4p = [];
        var data5p = [];
        var data6p = [];

        var labeldata1 = [];
        var labeldata2 = [];
        var labeldata3 = [];
        var labeldata4 = [];
        var labeldata5 = [];
        var labeldata6 = [];

        for(var i=0; i<data1.length; i++) {
            var tmp1 = data1[i]/sum[i]*100;
            data1p.push(tmp1);
            tmp1 = Math.round(tmp1*100)/100;
            tmp1 += "%";
            labeldata1.push(tmp1);
            var tmp2 = data2[i]/sum[i]*100;
            data2p.push(tmp2);
            tmp2 = Math.round(tmp2*100)/100;
            tmp2 += "%";
            labeldata2.push(tmp2);
            var tmp3 = data3[i]/sum[i]*100;
            data3p.push(tmp3);
            tmp3 = Math.round(tmp3*100)/100;
            tmp3 += "%";
            labeldata3.push(tmp3);
            var tmp4 = data4[i]/sum[i]*100;
            data4p.push(tmp4);
            tmp4 = Math.round(tmp4*100)/100;
            tmp4 += "%";
            labeldata4.push(tmp4);
            var tmp5 = data5[i]/sum[i]*100;
            data5p.push(tmp5);
            tmp5 = Math.round(tmp5*100)/100;
            tmp5 += "%";
            labeldata5.push(tmp5);
            var tmp6 = data6[i]/sum[i]*100;
            data6p.push(tmp6);
            tmp6 = Math.round(tmp6*100)/100;
            tmp6 += "%";
            labeldata6.push(tmp6);
        }

        var cityticks = jsondata2.cityticks;
        var company = jsondata2.companynames;
        subdomainname = jsondata2.subdomainname;

        if(plot3) { 
            plot3.destroy(); 
        }
        plot3 = $.jqplot('chartdiv3', [data1p, data2p, data3p, data4p, data5p, data6p], {
            stackSeries: true,
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                rendererOptions: {
                    barDirection: 'horizontal',
                },
                shadow: false,
            },
            axes: {
                yaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: cityticks
                },
                xaxis: {
                    showTicks: false,
                }
            },
            series:[
                {label:company[0], pointLabels: {show:true, labels:data1}},
                {label:company[1], pointLabels: {show:true, labels:data2}},
                {label:company[2], pointLabels: {show:true, labels:data3}},
                {label:company[3], pointLabels: {show:true, labels:data4}},
                {label:company[4], pointLabels: {show:true, labels:data5}},
                {label:company[5], pointLabels: {show:true, labels:data6}}
            ],
            highlighter: { show: false },
            legend: { show:true, location: 'e',placement: 'outside' },
            grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
            title: subdomainname + '细分领域人才前五大城市分布', 
        });

        if(plot4) { 
            plot4.destroy(); 
        }
        plot4 = $.jqplot('chartdiv4', [data1p, data2p, data3p, data4p, data5p, data6p], {
            stackSeries: true,
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                rendererOptions: {
                    barDirection: 'horizontal',
                },
                shadow: false,
            },
            axes: {
                yaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: cityticks
                },
                xaxis: {
                    showTicks: false,
                }
            },
            series:[
                {label:company[0], pointLabels: {show:true, labels:labeldata1}},
                {label:company[1], pointLabels: {show:true, labels:labeldata2}},
                {label:company[2], pointLabels: {show:true, labels:labeldata3}},
                {label:company[3], pointLabels: {show:true, labels:labeldata4}},
                {label:company[4], pointLabels: {show:true, labels:labeldata5}},
                {label:company[5], pointLabels: {show:true, labels:labeldata6}}
            ],
            highlighter: { show: false },
            legend: { show:true, location: 'e',placement: 'outside' },
            grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
            title: subdomainname + '细分领域人才前五大城市分布', 
        });

        //var jsondata3 = {
        //    subdomainname:"IDV",
        //    cityticks:['成都', '深圳', '广州', '上海', '北京'],
        //    positions:['技术支持','售后服务','市场','销售'],
        //    positiondata:[[451, 398, 365, 265, 146],
        //        [160, 350, 101, 85, 56],
        //        [589, 489, 479, 450, 465],
        //        [398, 366, 284, 95, 101]
        //    ]
        //};
        var jsondata3 = data.data3;

        var d3data1 = jsondata3.positiondata[0];
        var d3data2 = jsondata3.positiondata[1];
        var d3data3 = jsondata3.positiondata[2];
        var d3data4 = jsondata3.positiondata[3];

        var positions = jsondata3.positions;
        subdomainname = jsondata3.subdomainname;

        if(plot5) { 
            plot5.destroy(); 
        }
        plot5 = $.jqplot('chartdiv5', [d3data1, d3data2, d3data3, d3data4], {
            stackSeries: true,
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                shadow: false,
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: cityticks
                },
                yaxis: {
                    showTicks: false,
                }
            },
            series:[
                {label:positions[0]},
                {label:positions[1]},
                {label:positions[2]},
                {label:positions[3]}
            ],
            highlighter: { show: false },
            legend: { show:true, location: 'e',placement: 'outside' },
            grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
            title: subdomainname + '细分领域', 
        });

        //var jsondata4 = {
        //    subdomainname:"IDV",
        //    cityticks:['成都', '深圳', '广州', '上海', '北京'],
        //    positions:['技术支持','售后服务','市场','销售'],
        //    positiondata:[[7.4, 6.53, 5.99, 4.35, 2.4],
        //        [2.63, 5.74, 1.66, 1.4, 0.92],
        //        [9.67, 8.03, 7.86, 7.39, 7.63],
        //        [6.53, 6.01, 4.66, 1.56, 1.66]
        //    ],
        //};
        var jsondata4 = data.data4;

        cityticks = jsondata4.cityticks;
        subdomainname = jsondata4.subdomainname;

        var d4data1 = jsondata4.positiondata[0];
        var d4data2 = jsondata4.positiondata[1];
        var d4data3 = jsondata4.positiondata[2];
        var d4data4 = jsondata4.positiondata[3];

        var d4datalabel1 = [];
        var d4datalabel2 = [];
        var d4datalabel3 = [];
        var d4datalabel4 = [];

        for(var i=0; i<d4data1.length; i++) {
            var tmp1 = d4data1[i] + "%";
            d4datalabel1.push(tmp1);
            var tmp2 = d4data2[i] + "%";
            d4datalabel2.push(tmp2);
            var tmp3 = d4data3[i] + "%";
            d4datalabel3.push(tmp3);
            var tmp4 = d4data4[i] + "%";
            d4datalabel4.push(tmp4);
        }

        if(plot6) { 
            plot6.destroy(); 
        }
        plot6 = $.jqplot('chartdiv6', [d4data1, d4data2, d4data3, d4data4], {
            stackSeries: true,
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                shadow: false,
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: cityticks
                },
                yaxis: {
                    showTicks: false,
                }
            },
            series:[
                {label:positions[0], pointLabels: {show:true, labels:d4datalabel1}},
                {label:positions[1], pointLabels: {show:true, labels:d4datalabel2}},
                {label:positions[2], pointLabels: {show:true, labels:d4datalabel3}},
                {label:positions[3], pointLabels: {show:true, labels:d4datalabel4}}
            ],
            highlighter: { show: false },
            legend: { show:true, location: 'e',placement: 'outside' },
            grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
            title: subdomainname + '细分领域', 
        });

    },
};

$(document).ready(function() {
    var $dropdown2 = $('#subdomain');
    $dropdown2.dropdown({
        onChange: function(value) {
        
        }
    });

    var handler = {
        activate: function() { 
            $(this).addClass('active').siblings().removeClass('active'); 

            //if($(this).attr('id') == 'save1') { 
            //    $('#chartdiv1').jqplotSaveImage(); 
            //}
            //if($(this).attr('id') == 'save2') { 
            //    $('#chartdiv2').jqplotSaveImage(); 
            //}
            if($(this).attr('id') == 'save3') {
                $('#chartdiv3').jqplotSaveImage();
            }
            if($(this).attr('id') == 'save4') { 
                $('#chartdiv4').jqplotSaveImage();
            }
            if($(this).attr('id') == 'save5') {
                $('#chartdiv5').jqplotSaveImage();
            }
            if($(this).attr('id') == 'save6') {
                $('#chartdiv6').jqplotSaveImage();
            }
        } 
    };

    $('.button').on('click', handler.activate);
  
    //$.jqplot.config.enablePlugins = true;

    //var jsondata1 = {
    //subdomainname:"IDV",
    //data:[['研发',158], ['临床教育',289], ['注册/法规',298], ['技术支持',388], ['售后服务',456], ['生产', 597], ['市场',897], ['销售',1598]]
  //};

  //var s1 = jsondata1.data;
  //var subdomainname = jsondata1.subdomainname;

  //var labels2 = [];
  //for(var i=0; i<s1.length; i++) { 
  //  var tmp = s1[i][0] + "-" + s1[i][1];
  //  labels2.push(tmp);
  //}

  //var plot1 = $.jqplot('chartdiv1', [s1], {
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.DonutRenderer,
  //    rendererOptions: {
  //      showDataLabels: true,
  //      dataLabels:labels2
  //    },
  //    shadow: false,
  //  },
  //  highlighter: {show: false},
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域人才职位分布情况', 
  //});

  //var labels3 = [];
  //var sum = 0;
  //for(var i=0; i<s1.length; i++) {
  //  sum += s1[i][1];
  //}
  //for(var i=0; i<s1.length; i++) { 
  //  var per = s1[i][1]/sum*100;
  //  per = Math.round(per*100)/100;
  //  per = per + '%';
  //  var tmp = s1[i][0] + "-" + per;
  //  labels3.push(tmp);
  //}

  //var plot2 = $.jqplot('chartdiv2', [s1], {
  //  stackSeries: false,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.DonutRenderer,
  //    rendererOptions: {
  //      showDataLabels: true,
  //      dataLabels:labels3
  //    },
  //    shadow: false,
  //  },
  //  highlighter: { show: false },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域人才职位分布情况', 
  //});

  //var jsondata2 = {
  //  subdomainname:"IVD",
  //  companynames:['公司A', '公司B', '公司C', '公司D', '公司E', '公司F'],
  //  cityticks:['成都', '深圳', '广州', '上海', '北京'],
  //  citydata:[[146, 265, 365, 398, 451],
  //    [56, 85, 101, 350, 160],
  //    [465, 450, 478, 489, 549],
  //    [101, 95, 284, 366, 398],
  //    [101, 95, 284, 366, 398],
  //    [215, 328, 412, 589, 679]
  //  ]
  //};

  //var data1 = jsondata2.citydata[0];
  //var data2 = jsondata2.citydata[1];
  //var data3 = jsondata2.citydata[2];
  //var data4 = jsondata2.citydata[3];
  //var data5 = jsondata2.citydata[4];
  //var data6 = jsondata2.citydata[5];

  //var sum = [];
  //for(var i=0; i<data1.length; i++) {
  //  var tmp = 0;
  //  tmp += data1[i]; 
  //  tmp += data2[i]; 
  //  tmp += data3[i]; 
  //  tmp += data4[i]; 
  //  tmp += data5[i]; 
  //  tmp += data6[i]; 
  //  sum.push(tmp);
  //}

  //var data1p = [];
  //var data2p = [];
  //var data3p = [];
  //var data4p = [];
  //var data5p = [];
  //var data6p = [];

  //var labeldata1 = [];
  //var labeldata2 = [];
  //var labeldata3 = [];
  //var labeldata4 = [];
  //var labeldata5 = [];
  //var labeldata6 = [];

  //for(var i=0; i<data1.length; i++) {
  //  var tmp1 = data1[i]/sum[i]*100;
  //  data1p.push(tmp1);
  //  tmp1 = Math.round(tmp1*100)/100;
  //  tmp1 += "%";
  //  labeldata1.push(tmp1);
  //  var tmp2 = data2[i]/sum[i]*100;
  //  data2p.push(tmp2);
  //  tmp2 = Math.round(tmp2*100)/100;
  //  tmp2 += "%";
  //  labeldata2.push(tmp2);
  //  var tmp3 = data3[i]/sum[i]*100;
  //  data3p.push(tmp3);
  //  tmp3 = Math.round(tmp3*100)/100;
  //  tmp3 += "%";
  //  labeldata3.push(tmp3);
  //  var tmp4 = data4[i]/sum[i]*100;
  //  data4p.push(tmp4);
  //  tmp4 = Math.round(tmp4*100)/100;
  //  tmp4 += "%";
  //  labeldata4.push(tmp4);
  //  var tmp5 = data5[i]/sum[i]*100;
  //  data5p.push(tmp5);
  //  tmp5 = Math.round(tmp5*100)/100;
  //  tmp5 += "%";
  //  labeldata5.push(tmp5);
  //  var tmp6 = data6[i]/sum[i]*100;
  //  data6p.push(tmp6);
  //  tmp6 = Math.round(tmp6*100)/100;
  //  tmp6 += "%";
  //  labeldata6.push(tmp6);
  //}

  //var cityticks = jsondata2.cityticks;
  //var company = jsondata2.companynames;
  //subdomainname = jsondata2.subdomainname;

  //var plot3 = $.jqplot('chartdiv3', [data1p, data2p, data3p, data4p, data5p, data6p], {
  //  stackSeries: true,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    rendererOptions: {
  //      barDirection: 'horizontal',
  //    },
  //    shadow: false,
  //  },
  //  axes: {
  //    yaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: cityticks
  //    },
  //    xaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  series:[
  //    {label:company[0], pointLabels: {show:true, labels:data1}},
  //    {label:company[1], pointLabels: {show:true, labels:data2}},
  //    {label:company[2], pointLabels: {show:true, labels:data3}},
  //    {label:company[3], pointLabels: {show:true, labels:data4}},
  //    {label:company[4], pointLabels: {show:true, labels:data5}},
  //    {label:company[5], pointLabels: {show:true, labels:data6}}
  //  ],
  //  highlighter: { show: false },
  //  legend: { show:true, location: 'e',placement: 'outside' },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域人才前五大城市分布', 
  //});

  //var plot4 = $.jqplot('chartdiv4', [data1p, data2p, data3p, data4p, data5p, data6p], {
  //  stackSeries: true,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    rendererOptions: {
  //      barDirection: 'horizontal',
  //    },
  //    shadow: false,
  //  },
  //  axes: {
  //    yaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: cityticks
  //    },
  //    xaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  series:[
  //    {label:company[0], pointLabels: {show:true, labels:labeldata1}},
  //    {label:company[1], pointLabels: {show:true, labels:labeldata2}},
  //    {label:company[2], pointLabels: {show:true, labels:labeldata3}},
  //    {label:company[3], pointLabels: {show:true, labels:labeldata4}},
  //    {label:company[4], pointLabels: {show:true, labels:labeldata5}},
  //    {label:company[5], pointLabels: {show:true, labels:labeldata6}}
  //  ],
  //  highlighter: { show: false },
  //  legend: { show:true, location: 'e',placement: 'outside' },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域人才前五大城市分布', 
  //});

  //var jsondata3 = {
  //  subdomainname:"IDV",
  //  cityticks:['成都', '深圳', '广州', '上海', '北京'],
  //  positions:['技术支持','售后服务','市场','销售'],
  //  positiondata:[[451, 398, 365, 265, 146],
  //    [160, 350, 101, 85, 56],
  //    [589, 489, 479, 450, 465],
  //    [398, 366, 284, 95, 101]]
  //};

  //var d3data1 = jsondata3.positiondata[0];
  //var d3data2 = jsondata3.positiondata[1];
  //var d3data3 = jsondata3.positiondata[2];
  //var d3data4 = jsondata3.positiondata[3];

  //var positions = jsondata3.positions;
  //subdomainname = jsondata3.subdomainname;

  //var plot5 = $.jqplot('chartdiv5', [d3data1, d3data2, d3data3, d3data4], {
  //  stackSeries: true,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    shadow: false,
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: cityticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  series:[
  //    {label:positions[0]},
  //    {label:positions[1]},
  //    {label:positions[2]},
  //    {label:positions[3]}
  //  ],
  //  highlighter: { show: false },
  //  legend: { show:true, location: 'e',placement: 'outside' },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域', 
  //});

  //var jsondata4 = {
  //  subdomainname:"IDV",
  //  cityticks:['成都', '深圳', '广州', '上海', '北京'],
  //  positions:['技术支持','售后服务','市场','销售'],
  //  positiondata:[[7.4, 6.53, 5.99, 4.35, 2.4],
  //    [2.63, 5.74, 1.66, 1.4, 0.92],
  //    [9.67, 8.03, 7.86, 7.39, 7.63],
  //    [6.53, 6.01, 4.66, 1.56, 1.66]
  //  ],
  //};

  //cityticks = jsondata4.cityticks;
  //subdomainname = jsondata4.subdomainname;

  //var d4data1 = jsondata4.positiondata[0];
  //var d4data2 = jsondata4.positiondata[1];
  //var d4data3 = jsondata4.positiondata[2];
  //var d4data4 = jsondata4.positiondata[3];

  //var d4datalabel1 = [];
  //var d4datalabel2 = [];
  //var d4datalabel3 = [];
  //var d4datalabel4 = [];

  //for(var i=0; i<d4data1.length; i++) {
  //  var tmp1 = d4data1[i] + "%";
  //  d4datalabel1.push(tmp1);
  //  var tmp2 = d4data2[i] + "%";
  //  d4datalabel2.push(tmp2);
  //  var tmp3 = d4data3[i] + "%";
  //  d4datalabel3.push(tmp3);
  //  var tmp4 = d4data4[i] + "%";
  //  d4datalabel4.push(tmp4);
  //}

  //var plot6 = $.jqplot('chartdiv6', [d4data1, d4data2, d4data3, d4data4], {
  //  stackSeries: true,
  //  animate: !$.jqplot.use_excanvas,
  //  seriesDefaults:{
  //    renderer:$.jqplot.BarRenderer,
  //    shadow: false,
  //  },
  //  axes: {
  //    xaxis: {
  //      renderer: $.jqplot.CategoryAxisRenderer,
  //      ticks: cityticks
  //    },
  //    yaxis: {
  //      showTicks: false,
  //    }
  //  },
  //  series:[
  //    {label:positions[0], pointLabels: {show:true, labels:d4datalabel1}},
  //    {label:positions[1], pointLabels: {show:true, labels:d4datalabel2}},
  //    {label:positions[2], pointLabels: {show:true, labels:d4datalabel3}},
  //    {label:positions[3], pointLabels: {show:true, labels:d4datalabel4}}
  //  ],
  //  highlighter: { show: false },
  //  legend: { show:true, location: 'e',placement: 'outside' },
  //  grid: {drawGridLines: false, gridLineColor: '#FFFFFF', background:'#FFFFFF', borderColor:'#FFFFFF', shadow: false},
  //  title: subdomainname + '细分领域', 
  //});


});
</script>
</body>
</html>