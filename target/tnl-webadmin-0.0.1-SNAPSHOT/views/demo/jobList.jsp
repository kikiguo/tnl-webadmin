<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/taglibs.jsp"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="${ctx}/resources/semantic/dist/semantic.min.css"
	rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/semantic/dist/components/icon.min.css"
	rel="stylesheet" type="text/css" />
<link href="${ctx}/resources/apps.css" rel="stylesheet" type="text/css" />
<title>职位列表</title>

</head>
<body>
	<style type="text/css">

html, body, div, span, h1, h2, h3, h4, p, ul, li {
	margin: 0;
	padding: 0;
	border: 0px;
	vertical-align: baseline;
}

body {
	font-family: "Microsoft YaHei", "微软雅黑";
}

ol, ul {
	list-style: none;
}

blockquote, q {
	quotes: none;
}

blockquote:before, blockquote:after, q:before, q:after {
	content: '';
	content: none;
}

.top-view {
	height: 230px;
	background: url("${ctx}/resources/images/img/background.jpg") no-repeat;
	background-size: 100%;
	background-position: center;
	color: #fff;
	text-align: center;
}

.top-view .logo {
	padding-top: 40px;
}

.top-view .logo img {
	width: 70px;
}

.top-view .title {
	font-size: 18px;
	margin-top: 3px;
}

.top-view .list_content {
	margin-top: 5px;
}

.top-view .web_url {
	margin-top: 10px;
}

.top-view .menu {
	width: 28px;
	float: right;
	padding-right: 9px;
	margin-top: -8px;
}

.top-view .menu img {
	width: 100%;
	height: 100%;
}

.content {
	font-size: 14px;
}

.list {
	padding: 0 15px;
	margin-top: 11px;
}

.list .list_title {
	font-size: 16px;
	color: #005bac;
}

.list .list_title .redhead {
	color: #c00000;
	padding-right: 5px;
}

.list .list_content {
	line-height: 32px;
}

.list .list_content .content_title {
	padding-right: 40px;
}

.list .list_foot {
	font-size: 12px;
	margin-top: 10px;
}

.list .list_foot .bottom_icon {
	display: inline-block;
	width: 20px;
}

.list .list_foot .bottom_icon img {
	width: 100%;
	height: 100%;
}
</style>
	<!--  <header class="search refer">
	
   <img 
        class="banner-img" src="${ctx}/resources/images/banar.jpg"  style="width: 100%; height: auto;">
 
	<div class="keywords refer">
		<div class="circle refer"></div>
		<p class="company-name">西门子诊断</p>
		<p class="company-info">
			<span class="industry">医疗</span> <span class="employee"></span>
		</p>
		<p class="company-site">http://www.MedTrend.com</p>

		
	</div> 
	</header> -->
	<!--  <img 
        class="ui image" src="${ctx}/resources/images/title.png"  style="width: 100%; height: auto;">
        -->
	<div class="top-view">
		<div class="logo">
			<img src="${ctx}/resources/images/img/logo.png" alt="">
		</div>
		<h2 class="title">西门子诊断</h2>
		<p class="list_content content">医疗</p>
		<p class="web_url content">http://www.MedTrend.com</p>
		<div class="menu">
			<img src="${ctx}/resources/images/img/menu.png" alt="">
		</div>
	</div>
	<div class="ui segment" id="n-page">
		
			<c:choose>
				<c:when test="${jobs == null or fn:length(jobs)==0 }">  没有相关职位信息    </c:when>
				<c:otherwise>
					<div class="ui middle aligned divided list" id="row-data">

						<c:forEach items="${jobs}" var="jd">
							<div class="list">
								<h2 class="list_title">
									<span class="redhead">[热招]</span>${jd.name}（奖金 ${jd.reward} ）
								</h2>
								<p class="list_content content">${jd.companyName }</p>
								<p class="list_content content">
									<span class="content_title">描述：</span>${jd.majorDuty}
								</p>
								<div class="list_foot">
									<div class="bottom_icon">
										<img src="${ctx}/resources/images/img/location.png" alt="">
									</div>
									<span class="content_title">${jd.city.name}</span>
									<div class="bottom_icon">
										<img src="${ctx}/resources/images/img/update.png" alt="">
									</div>
									<span class="content_title"> ${jd.expiredDateStr}</span>
									<div class="bottom_icon">
										<img src="${ctx}/resources/images/img/price.png" alt="">
									</div>
									<span class="content_title">${jd.annualPayment}</span>
								</div>
							</div>
							<!--  <div class="item position" data-position-id="${jd.id}">
							
						<div class="content">
							<a class="header">${jd.name}&nbsp;&nbsp;( ${jd.reward} ) </a>
							<div class="meta">${jd.companyName }</div>
							<div class="description">描述：${jd.majorDuty}&nbsp;&nbsp;
								--备注${jd.comment}</div>
							<div class="extra">
								<div class="ui label">
									<i class="marker icon"></i>${jd.city.name}</div>
								<div class="ui label">
									<i class="wait icon"></i> ${jd.expiredDateStr}
								</div>
								<div class="ui label">
									<i class="dollar icon"></i>${jd.annualPayment}</div>
							</div>
						</div>
				</div> -->
						</c:forEach>
					</div>
				</c:otherwise>
			</c:choose>
		
	</div>

	<input type="hidden" name="lastposition" id="lastposition"
		value="${fn:length(jobs)}" />
	<input type="hidden" name="hasmoredata" id="hasmoredata" value="yes" />


	<script src="${ctx}/resources/semanticui/jquery-2.1.4.min.js"></script>
	<script src="${ctx}/resources/semanticui/semantic.js"
		type="text/javascript"></script>
	<script type="text/javascript">

wx.config({
    debug: false, 
    appId: "${config.appid}",  
    timestamp: ${config.currenttimestamp},  
    nonceStr: "${config.nonceStr}", 
    signature: "${config.signatureStr}", 
    jsApiList: ['checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'showOptionMenu']  
});

    var datasource = {
        moredata: function() {
            var getUrl = '${ctx}/demo/job/more';
            var offset  = $('#lastposition').val();
            var hasmore = $('#hasmoredata').val();

            getUrl += "?offset=" + offset;

            if(hasmore != "yes"){
                alert("已经没有更多数据了");
                return false;
            }

            var that = this;

            $.ajax({ 
                    url: getUrl, 
                    type: "GET", 
                    success: function(data, status){ 
                        if(data.errorcode == 0) {
                            that.appenddata(data);
                        } else {
                            var errorUrl = '${ctx}/wechat/candidate/error.page';
                            location.href = errorUrl;
                        }
                    }, 
                    error: function(){ 
                        alert("服务出错，请稍后尝试"); 
                    } 
            });
        },

        appenddata:function(data) {
            var offset = parseInt($('#lastposition').val());
            var divstring = "";
            var jobs = data.jobs;

            var count = jobs.length;
            offset += parseInt(count);
            $('#lastposition').val(offset);
            if(count <10) {
                $('#hasmoredata').val('no');
            }
            
            for (var i in jobs) {
                var jd = jobs[i];
                divstring +='<div class="item position" data-position-id="${jd.id}">'
                  divstring +='      <img class="ui avatar image"'
                  divstring +='          src="${ctx}/resources/images/img1.jpg">'
                  divstring +='      <div class="content">'
                  divstring +='          <a class="header">${jd.name}&nbsp;&nbsp;( ${jd.reward} ) </a>'
                  divstring +='          <div class="meta">jd.companyName</div>'
                  divstring +='          <div class="description">主要职责： ${jd.majorDuty}<br>备注${jd.comment}</div>'
                  divstring +='          <div class="extra">'
                  divstring +='              <div class="ui label"><i class="marker icon"></i> ${jd.cityId}</div>'
                  divstring +='              <div class="ui label">'
                  divstring +='                <i class="wait icon"></i>  ${jd.createDate}'
                  divstring +='              </div>'
                  divstring +='              <div class="ui label"><i class="dollar icon"></i>${jd.annualPayment}</div>'
                  divstring +='          </div>'
                  divstring +='      </div>'
                  divstring +='  </div>';

            }
            var newdata = $(divstring);
            $('#row-data').append(newdata);
        }
    };

    $(document).ready(function() { 
        
            $(window).scroll(function() { 
                if($(window).scrollTop() == $(document).height() - $(window).height()) {
                    datasource.moredata();
                } 
            });
            $('.position').on('click', function() {
                var positionid = $(this).attr('data-position-id');
                var getUrl = '${ctx}/demo/job/view.page?jobId='+positionid;
                location.href = getUrl;
            });

           
           
        });
    });

</script>
</body>
</html>